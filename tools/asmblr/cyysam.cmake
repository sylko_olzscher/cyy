# top level files
set (asmblr)

set (asmblr_cpp

    tools/asmblr/src/main.cpp
    tools/asmblr/src/driver.cpp
    tools/asmblr/src/linker.cpp
    tools/asmblr/src/reader.cpp
    tools/asmblr/src/writer.cpp
    tools/asmblr/src/symbols.cpp
    tools/asmblr/src/intrinsics/label.cpp
    tools/asmblr/src/intrinsics/factory/label_factory.cpp
    tools/asmblr/src/io/io_callback.cpp
)

set (asmblr_h

    tools/asmblr/src/driver.h
    tools/asmblr/src/linker.h
    tools/asmblr/src/reader.h
    tools/asmblr/src/writer.h
    tools/asmblr/src/section_types.h
    tools/asmblr/src/symbols.h
    tools/asmblr/src/intrinsics/label.h
    tools/asmblr/src/intrinsics/traits/type_traits_label.hpp
    tools/asmblr/src/intrinsics/factory/label_factory.h
    tools/asmblr/src/io/io_callback.h

)

set (asmblr_parser

    tools/asmblr/src/parser/section.h
    tools/asmblr/src/parser/section.hpp
    tools/asmblr/src/parser/section.cpp
    tools/asmblr/src/parser/data.h
    tools/asmblr/src/parser/data.hpp
    tools/asmblr/src/parser/data.cpp
    tools/asmblr/src/parser/text.h
    tools/asmblr/src/parser/text.hpp
    tools/asmblr/src/parser/text.cpp

)

set (asmblr_examples

    tools/asmblr/examples/factorial.cysam
    tools/asmblr/examples/p1.cysam

)

source_group("parser" FILES ${asmblr_parser})
source_group(examples FILES ${asmblr_examples})

# define the asmblr lib
set (asmblr
  ${asmblr_cpp}
  ${asmblr_h}
  ${asmblr_parser}
  ${asmblr_examples}
)