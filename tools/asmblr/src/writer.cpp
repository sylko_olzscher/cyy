/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */


#include "writer.h"
#include "driver.h"
#include "symbols.h"
#include "io/io_callback.h"
#include <fstream>
#include <iomanip>
#include <cyy/io/binary_io.h>
#include <cyy/io/write_binary.hpp>
#include <cyy/io.h>
#include <cyy/io/native_io.h>
#include <cyy/io/stream_io.h>
#include <cyy/io/cpp_io.h>
#include <cyy/io/format/time_format.h>
#include <cyy/version_info.h>
#include <cyy/intrinsics/version.h>
#include <cyy/intrinsics/type_traits.h>
#include <cyy/value_cast.hpp>


namespace cyy
{
	writer::writer(driver const& d)
	: driver_(d)
	{}
	
	void writer::dump()
	{
		//	
		//	Write code sequence
		//
		std::size_t count = 0;
		for (auto const& obj : driver_.symbols_->code_)
		{
			std::cout
				<< "["
				<< count
				<< "]\t"
				<< cyy::to_literal(obj, &io::asmbl_io_callback)
				<< std::endl;
			count++;
		}
	}
	
	bool writer::out(boost::filesystem::path const& fname)
	{
		std::fstream fout(fname.string(), std::ios::out | std::ios::trunc | std::ios::binary);
		if (fout.is_open())
		{
			//	
			//	first write the CY signature and the version
			//
			const std::string signature("CY");
			fout.write(signature.c_str(), signature.size());
			
			const version ver(CYY_VERSION_MAJOR, CYY_VERSION_MINOR);
			io::write_binary(fout, ver.full());

			//
			//	alignment to 8 bytes
			//
			fout.put(0xff);
			fout.put(0xff);

			//	
			//	write some statistical information
			//	* size of data section
			//	* op count
			//	* time of generation
			//

			//	
			//	Write code sequence
			//
			for (auto const& obj : driver_.symbols_->code_)
			{
				serialize_native(fout, obj, std::bind(&writer::custom_callback, this, std::placeholders::_1, std::placeholders::_2));
			}
			
			return true;
		}
		
		std::cerr
		<< "***error: could not open "
		<< fname
		<< std::endl 
		;
		
		return false;
	}

	bool writer::custom_callback(std::ostream&, object const&)
	{
		return false;
	}

	bool writer::cpp(boost::filesystem::path const& fname, boost::filesystem::path const& source)
	{
		std::fstream fout(fname.string(), std::ios::out | std::ios::trunc);
		if (fout.is_open())
		{

			fout
				<< "//\tgenerated at " << std::chrono::system_clock::now() << " (UTC)" << std::endl
				<< "//\tfrom " << source.filename() << std::endl
				<< "#include <cyy/intrinsic/factory.h>" << std::endl
				<< "vector_t prg;" << std::endl
				<< "prg" << std::endl
				;

			std::size_t counter{ 0 };
			for (auto const& obj : driver_.symbols_->code_)
			{

				if (counter == (driver_.symbols_->code_.size() - driver_.symbols_->data_.size() - driver_.symbols_->bss_.size()))
				{
					//	.BSS section
					fout << "// .BSS" << std::endl;
				}
				if (counter == (driver_.symbols_->code_.size() - driver_.symbols_->data_.size()))
				{
					//	.DATA section
					fout << "// .DATA" << std::endl;
				}

				//CYY_NULL = 1,	//!<	null, void
				//bool serialize_cpp(std::ostream& os, object const& obj, io::custom_callback cb);
				fout << "\t<< ";
				if (!serialize_cpp(fout, obj, &io::asmbl_io_callback))
				{
					//	unknown type:
					fout
						<< "//\t ***error"
						;
				}
				fout << std::endl;

				//	update counter
				counter++;
			}

			fout
				<< "\t;"
				<< std::endl;
			;

			return true;
		}
		return false;
	}


}	//	cyy
