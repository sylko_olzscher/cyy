/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CYY_ASMBL_LABEL_H
#define CYY_ASMBL_LABEL_H

#include <string>

namespace cyy 
{
	
	class label_patch
	{
	public:
		typedef std::string value_type;
		
	public:
		typedef std::string subtype;

	public:
		label_patch();
		label_patch(std::string const&);
		const std::string& get_name() const;
		
	private:
		const std::string name_;
	};

	class data_patch
	{
	public:
		typedef std::string value_type;

	public:
		typedef std::string subtype;

	public:
		data_patch();
		data_patch(std::string const&);
		const std::string& get_name() const;

	private:
		const std::string name_;
	};

	class bss_patch
	{
	public:
		typedef std::string value_type;

	public:
		typedef std::string subtype;

	public:
		bss_patch();
		bss_patch(std::string const&);
		const std::string& get_name() const;

	private:
		const std::string name_;
	};
}

#endif	//	CYY_ASMBL_LABEL_H
