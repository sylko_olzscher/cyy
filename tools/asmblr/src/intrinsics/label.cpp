/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/

#include "label.h"

namespace cyy 
{
	
	label_patch::label_patch()
	: name_()
	{}
	
	label_patch::label_patch(std::string const& name)
	: name_(name)
	{}

	const std::string& label_patch::get_name() const
	{
		return name_;
	}


	data_patch::data_patch()
		: name_()
	{}

	data_patch::data_patch(std::string const& name)
		: name_(name)
	{}

	const std::string& data_patch::get_name() const
	{
		return name_;
	}


	bss_patch::bss_patch()
		: name_()
	{}

	bss_patch::bss_patch(std::string const& name)
		: name_(name)
	{}

	const std::string& bss_patch::get_name() const
	{
		return name_;
	}
}