/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/


#ifndef CYY_ASMBL_FACTORY_LABEL_H
#define CYY_ASMBL_FACTORY_LABEL_H

#include "../label.h"
#include <cyy/object.h>

namespace cyy 
{
	
	object factory(label_patch const& v);
	object label_patch_factory(std::string const& v);
	object label_patch_factory();

	object factory(data_patch const& v);
	object data_patch_factory(std::string const& v);
	object data_patch_factory();

	object factory(bss_patch const& v);
	object bss_patch_factory(std::string const& v);
	object bss_patch_factory();

}

#endif	//	CYY_ASMBL_FACTORY_LABEL_H
