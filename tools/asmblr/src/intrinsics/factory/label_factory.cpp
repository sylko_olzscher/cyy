/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/


#include "label_factory.h"
#include "../traits/type_traits_label.hpp"
#include <cyy/detail/make_value.hpp>

namespace cyy 
{
	
	object factory(label_patch const& v)
	{
		return object(core::make_value(v));
	}
	
	object label_patch_factory(std::string const& v)
	{
		return object(core::make_value(label_patch(v)));
	}
	
	object label_patch_factory()
	{
		const std::string name;
		return label_patch_factory(name);
	}


	object factory(data_patch const& v)
	{
		return object(core::make_value(v));
	}

	object data_patch_factory(std::string const& v)
	{
		return object(core::make_value(data_patch(v)));
	}

	object data_patch_factory()
	{
		const std::string name;
		return data_patch_factory(name);
	}


	object factory(bss_patch const& v)
	{
		return object(core::make_value(v));
	}

	object bss_patch_factory(std::string const& v)
	{
		return object(core::make_value(bss_patch(v)));
	}

	object bss_patch_factory()
	{
		const std::string name;
		return bss_patch_factory(name);
	}
}