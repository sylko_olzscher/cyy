/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */


#include <boost/program_options.hpp>
#include <boost/filesystem.hpp>
#include <boost/config.hpp>
#include <fstream>
#include <iostream>
#include "driver.h"
#include <cyy/version_info.h>

/**
 * Before we start with a real compiler we use 
 * a simple assembler (called cyysam, pronouncation /ˈsɛsəmɪ/).
 * The assembler will also be used as linker
 * 
 * Start with
 * @code
 * build/cyysam --verbose --source tools/asmblr/examples/p1.cysam
 * ./cyysam "inp" -V
 * @endcode
 */
int main(int argc, char* argv[])	{

	try
	{
		std::string config_file;
		std::string source_file = "src.cysam";
		std::string out_file = "a.cyobj";
		std::string cpp_file = "a.ipp";

		const boost::filesystem::path cwd = boost::filesystem::current_path();
		
		//
		//	generic options
		//
		boost::program_options::options_description generic("Generic options");
		generic.add_options()
		
		("help,h", "print usage message")
		("version,v", "print version string")
		("build,b", "last built timestamp and platform")
		("config,C", boost::program_options::value<std::string>(&config_file)->default_value("asmblr.cfg"), "configuration file")

		
		;

		//
		//	all asmblr options
		//
		boost::program_options::options_description asmblr("asmblr");
		asmblr.add_options()

			("source,s", boost::program_options::value(&source_file), "main source file")
			("output,o", boost::program_options::value(&out_file), "output file")
			("cpp,p", boost::program_options::value(&cpp_file), "C++ snippet")
			("include-path,I", boost::program_options::value< std::vector<std::string> >()->default_value(std::vector<std::string>(1, cwd.string()), cwd.string()), "include path")
			("verbose,V", boost::program_options::bool_switch()->default_value(false), "verbose mode")
			("run,r", boost::program_options::bool_switch()->default_value(false), "execute generated code immediately")
			;

		//
		//	all you can grab from the command line
		//
		boost::program_options::options_description cmdline_options;
        cmdline_options.add(generic).add(asmblr);
		
		//
		//	positional arguments
		//
		boost::program_options::positional_options_description p;
		p.add("source", -1);

		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).positional(p).run(), vm);
		boost::program_options::notify(vm);
 
		if (vm.count("help")) 
		{  
			std::cout 
			<< cmdline_options 
			<< std::endl
			;
			return EXIT_SUCCESS;
		}
		
		if (vm.count("version")) 
		{  
			std::cout 
			<< "cyysam assembl[e]r v" 
			<< CYY_VERSION
			<< std::endl
			;
			return EXIT_SUCCESS;
		}
		
	if (vm.count("build")) 
	{  
		std::cout 
		<< "last built at : "
		<< __DATE__
		<< " "
		<< __TIME__
		<< std::endl
		<< "Platform      : "
		<< CYY_PLATFORM
		<< std::endl
		<< "Compiler      : "
#if defined(BOOST_WINDOWS)
		//	boost 1.63.0 has no idea about MSVC v19
		<< CYY_COMPILER
#else
		<< BOOST_COMPILER
#endif
		<< std::endl
		<< "StdLib        : "
		<< BOOST_STDLIB
		<< std::endl

#if defined(BOOST_LIBSTDCXX_VERSION)
		<< "StdLib++      : "
		<< BOOST_LIBSTDCXX_VERSION 
		<< std::endl
#endif
				
		<< "BOOSTLib      : "
		<< BOOST_LIB_VERSION 
		<< " ("
		<< BOOST_VERSION
		<< ")"
		<< std::endl
		
		<< "build type    : "
		<< CYY_BUILD_TYPE
		<< std::endl
		<< "ODBC available: "
#if defined(CYY_ODBC_HEADER_AVAILABLE)
		<< "yes"
#else
		<< "no"
#endif
		<< std::endl
		<< "shared mutex  : "
#if defined(BOOST_NO_CXX14_HDR_SHARED_MUTEX)
		<< "no"
#else
		<< "yes"
#endif
		<< std::endl
		<< std::endl
		;
		return EXIT_SUCCESS;
	}
		

		std::ifstream ifs(config_file);
		if (!ifs)
 		{
			std::cout 
			<< "***info: config file: " 
			<< config_file 
			<< " not found"
			<< std::endl
			;
		}
		else
		{
			boost::program_options::store(boost::program_options::parse_config_file(ifs, asmblr), vm);
			boost::program_options::notify(vm);
		}
		 
		if (vm.count("include-path"))
		{
			std::cout 
			<< "Include paths are: " 
			;
			auto const& v = vm["include-path"].as< std::vector<std::string> >();
			std::copy(v.begin(), v.end(), std::ostream_iterator<std::string>(std::cout, " "));
			std::cout 
			<< std::endl
			;
		}

		//
		//	add extension if missing
		//	
		boost::filesystem::path sf(source_file);
		if (!sf.has_extension())	
		{
			sf.replace_extension( ".cysam" );
		}
		
		boost::filesystem::path of(out_file);
		if (!of.has_extension())	
		{
			of.replace_extension( ".cyobj" );
		}

		//	C++ snippet
		boost::filesystem::path cppf(cpp_file);
		if (!cppf.has_extension())
		{
			cppf.replace_extension(".ipp");
		}

		//	target filename
		boost::filesystem::path tf(of);
		tf.replace_extension( ".cyout" );
		
		if (vm.count("source"))
		{
			std::cout 
			<< "Input file is: " 
			<< sf
			<< std::endl
			;
		}
		else 
		{
			std::cout 
			<< "***warning: no source file specified" 
			<< std::endl
			;
		}
		
		if (vm.count("object"))
		{
			std::cout 
			<< "Object file is: " 
			<< of
			<< std::endl
			;
		}

		cyy::driver d(sf
			, vm["include-path"].as< std::vector<std::string> >()
			, of, cppf, tf
			, vm["verbose"].as< bool >()
			, vm["run"].as< bool >());
		d.run();
		 
	}
	catch(std::exception& e) 
	{
		std::cerr 
		<< e.what() 
		<< std::endl
		;
    }

	return EXIT_SUCCESS;
}

