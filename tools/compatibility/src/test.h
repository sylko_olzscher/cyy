/*
 * Copyright Sylko Olzscher 2017
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_COMPATIBILITY_TEST_H
#define CYY_COMPATIBILITY_TEST_H

#include <type_traits>
#include <utility>
#include <iostream>	//	cout
#include <cyy/util/demangle.h>
#include <utility>
#include <boost/variant.hpp>

namespace cyy 
{
    /*
     * entry
     */
    int test();
}

#endif  //  CYY_COMPATIBILITY_TEST_H
