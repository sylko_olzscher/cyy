/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "test.h"
#include <stdlib.h>
#include <tuple>
#include <chrono>
#include <iostream>

namespace cyy
{
template<int ...>
struct sequence { };

template<int N, int ...S>
struct seq_generator : seq_generator<N-1, N-1, S...> { };

template<int ...S>
struct seq_generator<0, S...> {
    typedef sequence<S...> type;
};


// ...
//     void delayed_dispatch() {
//         callFunc(typename seq_generator<sizeof...(Args)>::type());
//     }

//     template<int ...S>
//     void callFunc(sequence<S...>) {
//         func(std::get<S>(params) ...);
//     }


void func(int n, std::string s, std::chrono::seconds t)
{
    std::cout
            << n
            << ", "
            << s
            << ", "
            << t.count()
            << " seconds"
            << std::endl;
}

void func(std::string s, std::chrono::seconds t, int n)
{
    std::cout
            << s
            << ", "
            << t.count()
            << " seconds, "
            << n
            << std::endl;
}

/*
 *  Source: https://stackoverflow.com/questions/18991910/unpacking-arguments-from-tuples
 * 
 *  think that @Xeo's comment summed it up well. From 14.5.3 of the C++11 standard:

 *  A pack expansion consists of a pattern and an ellipsis, the instantiation of which produces zero or more instantiations of the pattern in a list.
 *  In your case, by the time you finish with the recursive template instantiation and end up in the partial specialization, you have

 *  f(std::get<N>(std::forward<Tuple>(t))...);
 *  ...where N is parameter pack of four ints (0, 1, 2, and 3). From the standardese above, the pattern here is

 *  std::get<N>(std::forward<Tuple>(t))
 *  The application of the ... ellipsis to the above pattern causes it to be expanded into four instantiations in list form, i.e.

 *  f(std::get<0>(t), std::get<1>(t), std::get<2>(t), std::get<3>(t));
 */
template<typename Tpl, int ...S>
void caller(Tpl& tpl, sequence<S...>)
{
    func(std::get<S>(std::forward<Tpl>(tpl))...);
}

template < template<class...> class T, class... Args>
void unpack(T<Args...>& tpl)
{
    //
    //  tuple and sequence definition
    //
    using tuple_type = T<Args...>;
    using seq = typename seq_generator < sizeof... (Args)>::type;

    //	This is the trick to pull out the parameter pack
    //	of the tuple: expansion...
    caller(tpl, seq());

//         func(typename seq_generator<sizeof...(Args)>::type());
//         func(std::get<seq>(tpl)...);
// 		return f(std::get<seq>(tpl)...);

}

/*
 * entry
 */
int test()
{
    //
    //  create a tuple
    //
    auto tpl1 = std::make_tuple(1, std::string("hello"), std::chrono::seconds(42));

    //
    //  unpack parameters
    //
    unpack(tpl1);

    auto tpl2 = std::make_tuple(std::string("hello"), std::chrono::seconds(42), 1);
    unpack(tpl2);


    return EXIT_SUCCESS;
}

}

