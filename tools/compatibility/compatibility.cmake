# top level files
set (compatibility)

set (compatibility_cpp

    tools/compatibility/src/main.cpp
    tools/compatibility/src/test.cpp
)

set (compatibility_h

    tools/compatibility/src/test.h

)

# define the prebuild lib
set (compatibility
  ${compatibility_cpp}
  ${compatibility_h}
)
