# top level files
set (prebuild)

set (prebuild_cpp

    tools/prebuild/src/main.cpp
)

set (prebuild_h

#    tools/prebuild/src/linker.h

)

# define the prebuild lib
set (prebuild
  ${prebuild_cpp}
  ${prebuild_h}
)
