/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyy/util/shared/console.h>
#include <boost/predef.h>	//	requires Boost 1.55
#if BOOST_OS_LINUX
#include <readline/readline.h>
#include <readline/history.h>
#endif
#include <sstream>
#include <iomanip>
#include <fstream>
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/predicate.hpp>
#include <cyy/util/verify_file_extension.h>
#include <cyy/io/parser/bom_parser.h>
#include <cyy/intrinsics/factory/bytes_factory.h>
#include <cyy/io.h>
#include <numeric>
#include <boost/predef.h>

namespace cyy
{
	console::console(std::ostream& out, std::istream& in)
	: out_(out)
	, in_(in)
	, exit_(false)
	, history_()
	, call_depth_(0)
	{}

	console::~console()
	{}
	
	int console::loop()
	{
		std::string line;
		std::stringstream	prompt;
		
		while (!exit_)
		{
			prompt.str("");
			set_prompt(prompt);
			
#if BOOST_OS_WINDOWS
			out_
				<< prompt.str()
				;
			//	read new command
			std::getline(in_, line);
			boost::trim( line );
			if (exec(line))	
			{

				//	save last command
				history_.push_back(line);

			}
			
#else
			//	read new command
			char *inp = ::readline(prompt.str().c_str());
			line = boost::trim_copy(std::string(inp));
			if (exec(line))	
			{

				//	readline history
				::add_history(inp);

				//	save last command
				history_.push_back(line);
			}
			::free(inp);
			
#endif
			
		}
		
		return EXIT_SUCCESS;
	}
	
	void console::set_prompt(std::iostream& os)	
	{
		os 
		<< history_.size()
		<< "> "
		;
	}
		
	bool console::exec(std::string const& line)
	{
		try 
		{
			if (line.empty())	
			{
				return false;
			}
			else if (boost::algorithm::iequals(line, "q"))	return quit();
			else if (boost::algorithm::iequals(line, "history"))	return history();
			else if (boost::algorithm::iequals(line, "ip"))	return cmd_ip();
			else return parse(line);
		}
		catch(std::exception const& ex)
		{
			std::cerr
			<< "***Warning: "
			<< ex.what()
			<< std::endl 
			;
			
		}
		return false;
	}
	
	bool console::quit()	
	{
		exit_ = true;
		shutdown();
		out_ << "Bye" << std::endl;
		return true;
	}
	
	void console::shutdown()
	{}
	
	bool console::history() const	
	{
		std::size_t counter = 0;
		out_ 
		<< std::setfill(' ')
		;
		
		for (const std::string& s : history_)	
		{
			out_
				<< std::setw(4)
				<< counter++
				<< ' '
				<< s
				<< std::endl
				;
		}
		return true;
	}
	
	bool console::cmd_run(tuple_t::const_iterator pos, tuple_t::const_iterator end)
	{
		if (pos != end)	
		{
			const std::string file_name = to_string(*pos);
			const boost::filesystem::path p = verify_extension(file_name, ".script");
			if (boost::filesystem::exists(p))	
			{
				return cmd_run_script(p);
			}
			else
			{
				std::cerr
					<< "***Warning: "
					<< "script file ["
					<< boost::filesystem::absolute(p, boost::filesystem::current_path())
					<< "] not found"
					<< std::endl
					;
			}
		}
		else
		{
			std::cerr
				<< "***Warning: missing script file name"
				<< std::endl
				;
		}
		return false;
	}
	
	bool console::cmd_run_script(boost::filesystem::path const& p)
	{
			
		std::ifstream infile(p.string(), std::ios::in);
		if (!infile.is_open())
		{
			std::cerr
				<< "***Warning: "
				<< "script file ["
				<< p
				<< "] not found"
				<< std::endl
				;
			return false;
		}
		
		//
		//	update call depth
		//
		call_depth_++;
		
		std::cerr
			<< "***Info: "
			<< "execute script ["
			<< p
			<< "] - "
			<< call_depth_
			<< std::endl
			;

		std::string line;

		//
		//	read/skip optional BOM
		//
		io::bom_parser bomp;
		auto code = bomp.parse(infile);
		switch (code)
		{
			case io::bom::UTF16BE:	
				//	FE FF big endian
				std::cerr << "***Warning: cannot read UTF16 big endian encoding" << std::endl;
				return false;
			case io::bom::UTF16LE:	
				//	FF FE
				std::cerr << "***Warning: cannot read UTF16 little endian encoding" << std::endl;
				return false;
			case io::bom::UTF32BE:	
				//	00 00 FE FF
				std::cerr << "***Warning: cannot read UTF32 big endian encoding" << std::endl;
				return false;
			case io::bom::UTF32LE:	
				//	FF FE 00 00
				std::cerr << "***Warning: cannot read UTF32 little endian encoding" << std::endl;
				return false;
			case io::bom::UTF7:		
				//	2B 2F 76 38, 2B 2F 76 39, 2B 2F 76 2B, 2B 2F 76 2F
				std::cerr << "***Warning: cannot read UTF7 encoding" << std::endl;
				return false;
			case io::bom::UTF1:		
				//	F7 64 4C
				std::cerr << "***Warning: cannot read UTF1 encoding" << std::endl;
				return false;
			case io::bom::UTFEBCDIC:	//	DD 73 66 73
				std::cerr << "***Warning: cannot read EBCDIC encoding" << std::endl;
				return false;
			case io::bom::SCSU:		//	0E FE FF
				std::cerr << "***Warning: cannot read SCSU encoding" << std::endl;
				return false;
			case io::bom::BOCU1:		//	FB EE 28
				std::cerr << "***Warning: cannot read BOCU1 encoding" << std::endl;
				return false;
			case io::bom::GB18030:	//	84 31 95 33
				std::cerr << "***Warning: cannot read GB18030 encoding" << std::endl;
				return false;
			default:
				break;
		}
		
		//	test for fail() state
		std::size_t line_counter = 0;
		while (std::getline(infile, line, '\n')) 
		{
			//	update read state
			line_counter++;	//	line counter

			boost::algorithm::trim(line);
			if (!line.empty() && !boost::algorithm::starts_with(line, ";"))
			{
				if (!exec(line))
				{
					std::cerr
						<< "***Error in "
						<< p
						<< ':'
						<< line_counter
						<< " -- "
						<< line
						<< std::endl
						;
				}
			}

		}

		//
		//	update call depth
		//
		call_depth_++;
		
		std::cerr
			<< "***Info: "
			<< p
			<< " done"
			<< std::endl
			;
		
		return true;
	}
	
	bool console::cmd_echo(tuple_t::const_iterator pos, tuple_t::const_iterator end)
	{
		for (; pos != end; ++pos)	
		{
			out_
			<< to_literal(*pos)
			<< ' '
			;
		}
		out_ << std::endl;
		return true;
	}

	bool console::cmd_list(tuple_t::const_iterator pos, tuple_t::const_iterator end)
	{
		if (pos != end)	
		{
			const std::string dir = to_string(*pos);
			if (boost::filesystem::exists(dir))
			{
				return cmd_list(dir);
			}
			return false;
		}
		cmd_list(boost::filesystem::current_path());
		return true;
	}
	
	bool console::cmd_list(boost::filesystem::path const& p)
	{
		if (boost::filesystem::is_directory(p))	
		{
			bytes total_size = 0;
			std::for_each(boost::filesystem::directory_iterator(p)
				, boost::filesystem::directory_iterator()
				, [&total_size](boost::filesystem::path const& p)
				{

				if (boost::filesystem::is_directory(p))	
				{
					//
					//	get folder size
					//
					;
					const bytes count = std::accumulate(boost::filesystem::recursive_directory_iterator(p), boost::filesystem::recursive_directory_iterator(), 0ULL, [](std::size_t sum, boost::filesystem::path const& wd){
						return boost::filesystem::is_directory(wd)
						? sum
						: sum + boost::filesystem::file_size(wd)
						;
					});

					//
					//	sum up
					//
					total_size += count;

					std::cout
//						<< "             "
						<< std::setw(12)
						<< std::setfill(' ')
						<< to_literal(factory(count))
						<< ' '
						<< '<'
						<< boost::algorithm::trim_copy_if(p.filename().string(), boost::is_any_of("\""))
						<< '>'
						<< std::endl
						;
				}
				else 
				{
					const bytes count(boost::filesystem::file_size(p));

					std::cout
						<< std::setw(12)
						<< std::setfill(' ')
						<< to_literal(factory(count))
						<< ' '
						<< boost::algorithm::trim_copy_if(p.filename().string(), boost::is_any_of("\""))
						<< std::endl
						;

					//
					//	sum up
					//
					total_size += count;

				}

			});

			std::cout
				<< std::setw(12)
				<< std::setfill(' ')
				<< to_literal(factory(total_size))
				<< ' '
				<< '<'
				<< boost::algorithm::trim_copy_if(p.filename().string(), boost::is_any_of("\""))
				<< "> -- total size"
				<< std::endl
				;

			return true;
		}
		
		std::cerr 
		<< "***Warning: "
		<< p 
		<< " is not a directory"
		<< std::endl 
		;
		return false;
	}

	bool console::cmd_ip() const
	{
		const std::string host = boost::asio::ip::host_name();
		std::cout
			<< "host name: "
			<< host
			<< std::endl
			;

		try
		{
			boost::asio::io_service io_service;
			boost::asio::ip::tcp::resolver resolver(io_service);
			boost::asio::ip::tcp::resolver::query query(host, "");
			boost::asio::ip::tcp::resolver::iterator iter = resolver.resolve(query);
			boost::asio::ip::tcp::resolver::iterator end; // End marker.
			while (iter != end)
			{
				boost::asio::ip::tcp::endpoint ep = *iter++;
				std::cout
					<< (ep.address().is_v4() ? "IPv4: " : "IPv6: ")
					<< ep
					<< std::endl;
			}
			return true;
		}
		catch (std::exception const& ex)
		{
			std::cerr
				<< "***Error: "
				<< ex.what()
				<< std::endl
				;
		}
		return false;
	}

	
}	//	cyy
