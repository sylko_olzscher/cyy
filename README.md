# CYY C++ Library #

The CYY library (pronunciation: /siː/) is mostly about support of dynamically typed values in C++, in several respects:

* [CORE](https://bitbucket.org/sylko_olzscher/cyy/wiki/CORE)
    * Ability to wrap every C++ data type in a uniform object by taking advantage of the C++ template mechanism. No macros required.
    * Overcomes some restrictions of the [boost::any](http://www.boost.org/doc/libs/master/doc/html/any.html) data type
    * It's **not** intrusive. Set of small libraries you can use as needed.
    * [Large set](https://bitbucket.org/sylko_olzscher/cyy/wiki/CORE.data.types) of predefined data types.
    * Support for moving ownership
    * Implementation of  the concept of an [empty value](https://en.wikipedia.org/wiki/Nullable_type)
    * Large set of unit tests
* [IO](https://bitbucket.org/sylko_olzscher/cyy/wiki/IO)
    * Built in support for serialization
    * Formatted output
    * Streams as intrinsic data types
    * Extremely usefull to implement protocol parser and serializer.
* [PARSER](https://bitbucket.org/sylko_olzscher/cyy/wiki/PARSER)
    * Parsing all intrinsic data types
    * Extensible
* [JSON](http://json.org/)
    * Handling JSON objects like CYY/C++ objects
    * Smooth integration of parsing and serialization 
* [STORE](https://bitbucket.org/sylko_olzscher/cyy/wiki/STORE)
    * Simple key-value store
    * Threadsafe
    * Provides a lock hierarchy
* [VM](https://bitbucket.org/sylko_olzscher/cyy/wiki/VM)
    * A tiny VM extends dynamic data types by dynamic code execution
    * Comes with an [assembler](https://bitbucket.org/sylko_olzscher/cyy/wiki/CYYSAM)
    * Extensible by a library manager
    * Complete asynchronously
* [SCHEDULER](https://bitbucket.org/sylko_olzscher/cyy/wiki/SCHEDULER)
    * Managing of single and periodic tasks
    * Load distribution to all available CPUs
    * Ease the burden of implementing thread safe tasks (The scheduler takes care for you)
    * Support of the [active object pattern](http://accu.org/index.php/journals/1956)
    * [Signal handling](http://solostec.de/anchor/index.php/posts/a-unique-singleton)
    * new task library
* [DLOAD](https://bitbucket.org/sylko_olzscher/cyy/wiki/DLOAD)
    * Simple wrapper to load DLLs/shared libraries at runtime
    * Works on Linux and Windows
    * It's a little like a *classloader* for C++
* [SQL](https://bitbucket.org/sylko_olzscher/cyy/wiki/SQL)
    * Generate SQL queries at runtime
    
## Introduction ##

* Current version is 0.4.1494492985. Interfaces are almost stable now. 
* Linux (64 bit) are supported. 
* Windows 7 (64 bit) or higher are supported.
* Requires [g++](https://gcc.gnu.org/) >= 6.2 or cl 19.00.24215.1 (this is VS 14.0) and [boost](http://www.boost.org/) >= 1.60.0

## How do I get set up? ##

* Buildsystem is based on [cmake](http://www.cmake.org/) >= 3.1
* Download or clone from [git](https://sylko_olzscher@bitbucket.org/sylko_olzscher/cyy.git) [repository](https://sylko_olzscher@bitbucket.org/sylko_olzscher/cyy/branch/master)

To build CYY, run:


```
#!shell

git clone https://bitbucket.org/sylko_olzscher/cyy.git
mkdir build
cd build
cmake ..
make -j4 all
sudo make install

```

To run a test, type

```
#!shell

./unitTest --report_level=detailed

```

To build with Visual Studion on Windows:

```
#!shell
mkdir projects
cd projects
cmake -Wno-dev -G "Visual Studio 15 2017 Win64" ..

```

This will generate a Solution file for Visual Studio 15. Open this file and select **Build All**. 
CMake may require a little help to find the Boost Library. In this case modify the variable *BOOST_ROOT* 
in CMakeList.txt to the correct path.





## License ##

CYY C++ library is free software under the terms of the [boost library](http://www.boost.org/users/license.html).

## Contact ##

Your pull requests are welcomed or you can post an [issue](https://bitbucket.org/sylko_olzscher/cyy/issues)