/*
 * Copyright Sylko Olzscher 2015-2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_XML_SERIALIZE_H
#define CYY_XML_SERIALIZE_H

#include <pugixml.hpp>
#include <cyy/cyy_export.h>
#include <cyy/object_interface_fwd.h>
#include <cyy/intrinsics/sets_fwd.h>
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/io/io_callback.h>

namespace cyy
{
	namespace xml
	{

		/**
		* Function type to query custom data type information
		*/
		using custom_callback = std::function<bool(pugi::xml_node, object const&)>;
	}

	/** 
	 * For correct representation of NULL values the XML namespace
	 * xmlns:xsi = "w3.org/2001/XMLSchema-instance" 
	 * must be declared.
	 *
	 * @return specified node
	 * @see https://www.w3.org/TR/2004/REC-xmlschema-2-20041028/datatypes.html#built-in-datatypes
	 */
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, cyy::object const& obj);
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, object const& obj, xml::custom_callback cb);
	CYY_XML_LIBRARY_API pugi::xml_node serialize_xml(pugi::xml_node, const char*, object const& obj, xml::custom_callback = xml::custom_callback());
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, cyy::tuple_t const& obj, xml::custom_callback = xml::custom_callback());
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, cyy::vector_t const& obj, xml::custom_callback = xml::custom_callback());
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, cyy::set_t const& obj, xml::custom_callback = xml::custom_callback());
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, cyy::param_map_t const& params, xml::custom_callback = xml::custom_callback());
	CYY_XML_LIBRARY_API bool serialize_xml(pugi::xml_node, cyy::buffer_t const& buffer);

	/**
	 * @return new created node 
	 */
	CYY_XML_LIBRARY_API pugi::xml_node serialize_xml(pugi::xml_node, cyy::param_t const& param, xml::custom_callback);

	/**
	 * Append a node with the specified name and set the value.
	 *
	 * @return created node
	 */
	CYY_XML_LIBRARY_API pugi::xml_node append_value(pugi::xml_node
		, std::string const& name
		, cyy::object const& obj
		, xml::custom_callback = xml::custom_callback());

	/**
	 * Iterates over the specified range and construct an object
	 * tree. Works only for data with the cyy XML serializer format.
	 * Returns an empty data set if result set was empty.
	 */
	CYY_XML_LIBRARY_API object load(pugi::xml_node, std::string const& query);

	/**
	 * Read data from specified node and create an object holding all
	 * data. Works recursiv.
	 */
	CYY_XML_LIBRARY_API object traverse(pugi::xml_node const& node);
}

#endif	//	CYY_XML_SERIALIZE_H
