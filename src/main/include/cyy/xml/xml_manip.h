/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_XML_MANIP_H
#define CYY_XML_MANIP_H

#include <pugixml.hpp>
#include <cyy/cyy_export.h>
#include <cyy/object.h>
#include <cyy/intrinsics/cyystddef.h>
#include <string>

namespace cyy
{
	//
	//	seems to work only in namespace cyy
	//

	CYY_XML_LIBRARY_API pugi::xml_attribute operator<<(pugi::xml_attribute, std::string const&);	
	CYY_XML_LIBRARY_API pugi::xml_attribute operator<<(pugi::xml_attribute, std::size_t);
	CYY_XML_LIBRARY_API pugi::xml_attribute operator<<(pugi::xml_attribute, index);

	CYY_XML_LIBRARY_API pugi::xml_attribute operator>>(pugi::xml_attribute, std::string&);
	CYY_XML_LIBRARY_API pugi::xml_attribute operator>>(pugi::xml_attribute, index&);

}

#endif	//	CYY_XML_MANIP_H
