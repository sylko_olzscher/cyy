/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_META_HPP
#define CYY_META_HPP

#include <type_traits>
#include <utility>
#include <iostream>	//	cout
#include <cyy/util/demangle.h>
#include <utility>
#include <boost/variant.hpp>

namespace cyy 
{
    
#if __GNUC__ > 5 || defined(_MSC_VER) 
    
    //
    //  std::make_index_sequence supported
    //
    
#else
    
    //
    //  Signal that we working with an old
    //  compiler version that don't support 
    //  some features from C++11 and later.
    //
#define CYY_COMPATIBILITY_MODE
    
    /**
     * Define our own sequence.
     */
    template<int ...>
    struct sequence 
    { };

    template<int N, int ...S>
    struct generate_sequence : generate_sequence<N-1, N-1, S...> 
    { };

    template<int ...S>
    struct generate_sequence<0, S...> 
    {
        using type = sequence<S...>;
    };
    
#endif
    
	/**
	 * Metafunction invocation.
	 * Typically type is the return type/value of a meta function.
	 */
    template <typename T>
    using invoke = typename T::type;

    /**
	 * Identity metafunction
	 */
    template <typename T>
    struct identity 
    {
        using type = T;
    };
	
	/**
	 * Tests if T is a specialization of Template
	 * 
	 * example:
	 * @code
	 * typedef std::tuple<int, std::string>	M;
	 * static_assert(is_instantiation_of <M, std::tuple>::value, "tuple required");
	 * @endcode
	 * 
	 * Original code comes from 
	 * @see http://stackoverflow.com/questions/11251376/how-can-i-check-if-a-type-is-an-instantiation-of-a-given-class-template#comment14786989_11251408
	 */
#if !defined(BOOST_MSVC)
	//	doesn't compile with VS 2015 update 2
    template <typename T, template <typename...> class Template>
    struct is_instantiation_of  : std::false_type 
    {};
	
    template <template <typename...> class Template, typename... Args>
    struct is_instantiation_of <Template<Args...>, Template> : std::true_type 
    {};
#endif
	
	/**
	 * Constant void metafunction
	 * part of C++ since C++17
	 */
    template <typename>
    struct the_void : identity<void> 
    {};
	
    template <typename T>
    using void_t = invoke<the_void<T>>;
	
    /**
	 * Tests if T is callable with the given Signature
	 */
    template <typename T, typename Signature, typename = void>
    struct is_callable : std::false_type 
    {
        static_assert(std::is_function<Signature>::value, "Signature must be a function type");
    };
	
    template <typename Fun, typename Ret, typename... Args>
    struct is_callable<Fun, Ret(Args...), /*std::*/void_t<decltype(invoke<Ret>(std::declval<Fun>(), std::declval<Args>()...))>>
    : std::true_type 
    {};	

	/**
	 * @see http://pdimov.com/cpp2/simple_cxx11_metaprogramming.html
	 */
	template<template<class...> class F, class L> 
	struct mp_transform_impl;

	template<template<class...> class F, class L>
	using mp_transform = typename mp_transform_impl<F, L>::type;

	template<template<class...> class F, template<class...> class L, class... T>
	struct mp_transform_impl<F, L<T...>>
	{
		//
		//	F<T>... transforms the parameter list
		//
		using type = L<F<T>...>;
	};

	namespace 
	{
		template<std::size_t I, typename F, typename Tuple> 
		struct for_each_impl 
		{
			static void for_each(const Tuple& tpl, F f) 
			{
				for_each_impl<I - 1, F, Tuple>::for_each(tpl, f);
				f(std::get<I>(tpl));
			}
		};
		
		template<typename F, typename Tuple> 
		struct for_each_impl<0, F, Tuple> 
		{
			static void for_each(const Tuple& tpl, F f) 
			{
				f(std::get<0>(tpl));
			}
		};
	}
	
	/**
	 * The infamous tuple iterator. This implementation is straight forward recursive implementation.
	 * @see http://stackoverflow.com/questions/5640429/generalizing-for-each-over-a-tuple-to-accept-a-variable-number-of-arguments
	 * 
	 * Other implementation are using std::index_sequence:
	 * @see http://codereview.stackexchange.com/questions/51407/stdtuple-foreach-implementation
	 */
	template<typename F, typename Tuple>
	F for_each(const Tuple& tpl, F f) 
	{
		for_each_impl<std::tuple_size<Tuple>::value - 1, F, Tuple>::for_each(tpl, f);
		return f;
	}

	//
	//	more generic approach
	//	work in progress
	//

	template<typename F, std::size_t N, typename ...Args> 
	struct mp_for_each_impl
	{
		typedef typename std::tuple_element<N, std::tuple<Args...>>::type U;
		
		static void invoke(F f, Args&&... args)
		{
			//	recursive call
			mp_for_each_impl<F, N - 1, Args...>::invoke(f, std::forward<Args>(args)...);
			
			//
			//	std::get< N >() return a value of type U
			//
			//	N is provides as template argument to the called function.
			//	This offers a path to use the index of the tuple element
			//	at compile time.
			//
			//const U value = std::get< N >(std::forward_as_tuple(std::forward<Args>(args)...));
			f.template call<N>(std::get< N >(std::forward_as_tuple(std::forward<Args>(args)...)));
		}
		
	};
     
	template<typename F, typename ...Args> 
	struct mp_for_each_impl< F, 0, Args... >
	{
		typedef typename std::tuple_element<0, std::tuple<Args...>>::type U;
		
		static void invoke(F f, Args&&... args)
		{
			//const U value = std::get< 0 >(std::forward_as_tuple(std::forward<Args>(args)...));
			f.template call<0>(std::get< 0 >(std::forward_as_tuple(std::forward<Args>(args)...)));
		}
	};


	/**
	 * Iterate over a variadic template parameter pack and
	 * call a functor/function for each element.
	 * 
	 * Example:
	 * @code
 		struct my_functor
		{
			template<typename T>
			void operator () (T&& t)
			{
				std::cout 
				<< "my_functor("
				<< demangle(typeid(T).name()) 
				<< ")"
				<< std::endl;
			}
		};
 
		template <typename ...Args>
		void calculate(Args&&... args)
		{
			mp_for_each<Args...>::invoke(my_functor(), std::forward<Args>(args)...);
		}
	 * @endcode
	 */
	template<class ...Args> 
	struct mp_for_each
	{
		using size = std::integral_constant<std::size_t, sizeof...(Args)>;
		
		template < typename F >
		static void invoke(F f, Args&&... args)
		{
			typedef mp_for_each_impl<F, size::value-1, Args...> impl;
			impl::invoke(f, std::forward<Args>(args)...);
		}
	};
  
	template<template<class...> class T, class... U> 
	struct mp_for_each<T<U...>>
	{
		//
		//	Extract the variadic template parameter pack
		//	and invoke the recursive implementation.
		//
		template < typename F >
		static void invoke(F f, U&&... args)
		{
			mp_for_each<U...>::invoke(f, std::forward<U>(args)...);
		}
	};


	//
	//	educational purposes only
	//
	template<typename ...Args> 
	struct mp_size_impl
	{
		using type = std::integral_constant<std::size_t, sizeof...(Args)>;
	};
 	template<template<class...> class L, class... T> 
 	struct mp_size_impl<L<T...>>
 	{
 		using type = typename mp_size_impl<T...>::type;
// 		using type = std::integral_constant<std::size_t, sizeof...(T)>;
 	};

	/**
	 * Calculate size of parameter pack
	 */
	template<typename ...L> 
	using mp_size = typename mp_size_impl<L...>::type;
    
    /**
     * Simply forward a parameter pack
     */
	template <typename F, typename ...Args>
	auto apply(F f, Args&&... args) -> typename F::result_type
	{
		return f(std::forward<Args>(args)...);
	}

	// index_sequence requires at least gcc 4.9
#if !defined(CYY_COMPATIBILITY_MODE) 
	/**
	 * Expand a tuple and apply the parameter pack to the
	 * specified function.
	 * index_sequence requires at least gcc 4.9
	 */
	template <typename F, typename Tpl, std::size_t... seq>
	auto apply(F f, Tpl&& tpl, std::index_sequence<seq...>)
	{
		//	This is the trick to pull out the parameter pack
		//	of the tuple: expansion...
		return f(std::get<seq>(std::forward<Tpl>(tpl))...);
	}

	/**
	 * index_sequence requires at least gcc 4.9
	 */
	template <typename F, typename Tpl, std::size_t... seq>
	auto apply(F f, Tpl& tpl, std::index_sequence<seq...>)
	{
		//	This is the trick to pull out the parameter pack
		//	of the tuple: expansion...
		return f(std::get<seq>(tpl)...);
	}
	
#else


	/**
	 * Expand a tuple and apply the parameter pack to the
	 * specified function.
	 * index_sequence requires at least gcc 4.9
	 */
	template <typename F, typename Tpl, int... seq>
	auto apply(F f, Tpl&& tpl, sequence<seq...>) -> typename F::result_type
	{
		//	This is the trick to pull out the parameter pack
		//	of the tuple: expansion...
		return f(std::get<seq>(std::forward<Tpl>(tpl))...);
	}

	/**
	 * index_sequence requires at least gcc 4.9.
     * Since the compiler has problems to distinguish between these
     * two task_apply() methods, we have renamed this function 
     * to task_apply_ref(...)
	 */
	template <typename F, typename Tpl, int... seq>
	auto apply_ref(F f, Tpl& tpl, sequence<seq...>) -> typename F::result_type
	{
		//	This is the trick to pull out the parameter pack
		//	of the tuple: expansion...
		return f(std::get<seq>(tpl)...);
	}
    
#endif
    

	/**
	 * Unspecialized struct to get a parameter pack.
     * This is specialized later for tuples. 
     * 
	 * @see mp_unpack<T<Args...>>
	 */
	template<class ...Args> 
	struct mp_unpack
	{
		template < typename F >
		static void invoke(F f, Args&&... args)
		{
			f(std::forward<Args>(args)...);
		}
	};
  
#if !defined(CYY_COMPATIBILITY_MODE) 

	/**
     * Specialized for tuples.
     * 
	 * Example:
	 * @code
	 * auto tpl = std::make_tuple(2, std::string("HELLO"));
	 * typedef cyy::mp_unpack<decltype(tpl)>	unpack_t;
	 * unpack_t::invoke([](auto &&... args){
	 * 		//	use the unpacked arguments
	 * }, tpl);
	 * @endcode
	 */
	template<template<class...> class T, class... Args> 
	struct mp_unpack<T<Args...>>
	{
		using tuple_type = T<Args...>;
//		using seq = std::index_sequence_for <Args...>;	//	this knocks the VC 2015.2 compiler out
		using seq = std::make_index_sequence < sizeof... (Args)>;
		//using seq = make_index_sequence < sizeof... (Args)>;
		
		template < typename F >
		static void invoke(F f, tuple_type& tpl)
		{
			apply(f, tpl, seq());
		}

		template < typename F >
		static void invoke(F f, tuple_type&& tpl) 
		{
			apply(f, std::forward<tuple_type>(tpl), seq());
		}
	};
#else
	/**
     * Specialized for tuples.
     * 
	 * Example:
	 * @code
	 * auto tpl = std::make_tuple(2, std::string("HELLO"));
	 * typedef cyy::mp_unpack<decltype(tpl)>	unpack_t;
	 * unpack_t::invoke([](auto &&... args){
	 * 		//	use the unpacked arguments
	 * }, tpl);
	 * @endcode
	 */
	template<template<class...> class T, class... Args> 
	struct mp_unpack<T<Args...>>
	{
		using tuple_type = T<Args...>;
        using seq = typename generate_sequence < sizeof... (Args)>::type;
		
		template < typename F >
		static void invoke(F f, tuple_type&& tpl) 
		{
			apply(f, std::forward<tuple_type>(tpl), seq());
		}
		
		template < typename F >
		static void invoke(F f, tuple_type& tpl)
		{
			apply_ref(f, tpl, seq());
		}
        
    };
#endif
	
#if __CYY_EXPERIMENTAL     
	template<class ...Args> 
	struct mp_unpack2
	{
		template < typename R >
		static R invoke(std::function<R(Args&&...)> f, Args&&... args)
		{
			return f(std::forward<Args>(args)...);
		}
	};
	
	template<template<class...> class T, class... Args> 
	struct mp_unpack2<T<Args...>>
	{
		using tuple_type = T<Args...>;
		using seq = std::make_index_sequence < sizeof... (Args)>;
		
		template < typename R >
		static R invoke(std::function<R(Args&&...)> f, tuple_type& tpl)
		{
			return invoke_impl<R>(f, tpl, seq());
		}

		template < typename R, std::size_t... idxs >
		static R invoke_impl(std::function<R(Args&&...)> f, tuple_type& tpl, std::index_sequence<idxs...>)
		{
			//	expansion
			return f(std::get<idxs>(tpl)...);
		}
	};
#endif  //  __CYY_EXPERIMENTAL
	
	namespace 
	{
		template <std::size_t N, typename ...Args>
		boost::variant<Args...> get_by_index_impl(std::size_t idx, std::tuple<Args...> const& tpl)
		{
			static_assert(N < sizeof...(Args), "index out of range");
			return (N == idx)
			? std::get<N>(tpl)
			: get_by_index_impl<N + 1>(idx, tpl)
			;
		}
	}
	

	/**
	 * Get a tuple element by index at runtime.
	 * boost::variant<> does the trick to map a set of different
	 * data types.
	 */
	template <typename ...Args>
	boost::variant<Args...> get_by_index(std::size_t idx, std::tuple<Args...> const& tpl)
	{
		return get_by_index_impl<0>(idx, tpl);
	}

}
#endif	//	CYY_META_HPP
