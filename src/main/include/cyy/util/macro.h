/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_UTIL_MACRO_H
#define CYY_UTIL_MACRO_H


#ifdef __GNUC__
	#define DEPRECATED(func) func __attribute__ ((deprecated))
#elif defined(_MSC_VER)
	#define DEPRECATED(func) __declspec(deprecated) func
#else
	#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
	#define DEPRECATED(func) func
#endif


//	example:
//	DEPRECATED(void foo(int a, float b));

#endif	//	CYY_UTIL_MACRO_H
