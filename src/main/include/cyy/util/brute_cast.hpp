/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#ifndef CYY_UTIL_BRUTE_CAST_HPP
#define CYY_UTIL_BRUTE_CAST_HPP

#include <cyy/container/step.hpp>
#include <cstdint>
#include <algorithm>
#include <climits>	//	CHAR_BIT
#include <limits>
#include <array>
#include <vector>
#include <list>
#include <type_traits>
#include <tuple>
#include <cstring>	//	std::memcpy
#include <boost/concept_check.hpp>
#include <boost/assert.hpp>

namespace cyy 
{

    //
    //  forwarding
    //
	template < typename T, typename U >
	inline T brute_cast(U const& src);
    
	template < typename T, typename U, std::size_t N >
	inline T brute_cast(U const (&src)[ N ]);
    
	template < typename R, std::size_t OFFSET, typename U, std::size_t N >
	inline R const& brute_cast(U const(&src)[N]);
    
	template < typename R, typename U >
	inline R brute_cast(std::vector<U> const& src);
    
	template < typename T, typename U, std::size_t N >
	inline bool brute_cast(std::list<T> const& src, U(&trg)[N]);
    
	template < typename T, typename U, std::size_t N >
	T brute_cast(std::array< U, N > const& a);
    
	template < typename T, typename U, std::size_t N >
	inline void brute_cast(T(&target)[ N ], U const& src);
    
	template < typename T, typename U, std::size_t N >
	inline void brute_reverse_cast(T(&target)[ N ], U const& src);
    
	/**
	 * Enables or force the cast between all types that are POD and of the same
	 * size.
	 * 
	 * @code
		std::uint64_t a = 1;
		std::uint64_t *b = &a;
		a = brute_cast<std::uint64_t>(b);
	 * @endcode
	 *
	 * Interestingly data type int doesn't work on 64 bit linux machines using gcc 5.x
	 * sizeof(int) = 4
	 * sizeof(int*) = 8
	 * To be honest, I didn't expect this.
	 */
	template < typename T, typename U >
	inline T brute_cast(U const& src)
	{
		static_assert(sizeof(T) == sizeof(U), "incompatible data types");
		union
		{
			mutable U source_;
			T target_;
		} u_;
		u_.source_ = src;
		return u_.target_;
	}
	
	/**
	 * The same thing with arrays. Construct a POD from
	 * array elements.
	 * 
	 * @code
	 * const char c[4] = { 1, 2, 3, 4 };
	 * const std::uint32_t value = brute_cast<std::uint32_t>(c);
	 * @endcode
	 */
	template < typename T, typename U, std::size_t N >
	inline T brute_cast(U const (&src)[ N ])
	{
		static_assert(sizeof(T) == N * sizeof(U), "incompatible data types");
		union
		{
			mutable U source_[ N ];
			T target_;
		} u_;
		
		std::copy_n(src, N, u_.source_);
		return u_.target_;
	}
	
	/**
	 * Convert a range from an array into a POD
	 */
	template < typename R, std::size_t OFFSET, typename U, std::size_t N >
	inline R const& brute_cast(U const(&src)[N])
	{
		static_assert(OFFSET + sizeof(R) <= N * sizeof(U), "incompatible data types");
		return *reinterpret_cast<R const*>(&src[OFFSET]);
	}

	/**
	 * Convert the content of a vector into a POD type
	 * 
	 * @code
	 * const vector<char> vec { 1, 2, 3, 4 };
	 * const std::uint32_t value = brute_cast<std::uint32_t>(vec);
	 * @endcode
	 */
	template < typename R, typename U >
	inline R brute_cast(std::vector<U> const& src)
	{
		enum { DIM = sizeof(R) / sizeof(U) };
		union
		{
			mutable U source_[DIM];
			R target_;
		} u_;

		if (src.size() >= DIM)
		{
			std::copy_n(src, DIM, u_.source_);
		}
		else
		{
			std::copy_n(src, src.size(), u_.source_);
		}
		return u_.target_;
	}

	/**
	 * Convert a list into an array.
	 */
	template < typename T, typename U, std::size_t N >
	inline bool brute_cast(std::list<T> const& src, U(&trg)[N])
	{
		using type = typename std::decay<U>::type;
		enum { SIZE = sizeof(U) * N };

		if (SIZE <= sizeof(T) * src.size())
		{
			if (sizeof(T) > sizeof(U))
			{
				enum { STEPS = sizeof(T) / sizeof(U) };
				type tmp[STEPS];
				std::size_t offset{ 0 };
				for (auto const& e : src)
				{
					//	split e into array elements
					brute_cast(tmp, e);
					for (std::size_t idx = 0; idx < STEPS; ++idx)
					{
						trg[idx + offset] = tmp[idx];
					}
					offset += STEPS;
				}
			}
			else if (sizeof(T) < sizeof(U))
			{
				//	ToDo: implement
				enum { STEPS = sizeof(U) / sizeof(T) };
				//type tmp[STEPS];
				std::size_t offset{ 0 };
				BOOST_ASSERT_MSG(false, "not implemented yet");
			}
			else
			{
				//	special case of T == U
				//std::copy_n(src, N, trg);
				BOOST_ASSERT_MSG(false, "not implemented yet");
			}
			return true;
		}
		return false;
	}

	/**
	 * @brief Construct a POD from std::array elements.
	 *
	 * The same thing with std::array. 
	 *
	 * @code
	 * const std::array<char, 4 > c = { 1, 2, 3, 4 };
	 * const std::uint32_t value = brute_cast<std::uint32_t>(c);
	 * @endcode
	 */
	template < typename T, typename U, std::size_t N >
	T brute_cast(std::array< U, N > const& a)
	{
		static_assert(sizeof(T) == N * sizeof(U), "incompatible data types");
		union
		{
			mutable std::array< U, N > source_;
			T target_;
		} u_;

		u_.source_ = a;
		return u_.target_;
	}

	/**
	 * This time we split a POT into pieces and fill
	 * an array. Since C++ doesn't allow arrays as return
	 * types, we use a reference parameter.
	 * @code
	 * uint32_t value{ 42 };
	 * char r[4] = { 0 };
	 * cyy::brute_cast(r, value);
	 * @endcode
	 */
	template < typename T, typename U, std::size_t N >
	inline void brute_cast(T(&target)[ N ], U const& src)
	{
		static_assert(N * sizeof(T) ==  sizeof(U), "incompatible data types");
		//	CHAR_BIT == std::numeric_limits<char>::digits == 8
		const auto size = sizeof(T) * CHAR_BIT;
		for (auto idx : step<std::size_t, std::size_t>(0, N, 1))
		{
			const T tmp = static_cast<T>(src >> (idx * size));
			target[idx] = tmp;
		}
	}

	template < typename T, typename U, std::size_t N >
	inline void brute_reverse_cast(T(&target)[ N ], U const& src)
	{
		static_assert(N * sizeof(T) ==  sizeof(U), "incompatible data types");
		const auto size = sizeof(T) * CHAR_BIT;
		for (auto idx : step<std::size_t, std::size_t>(0, N, 1))
		{
			const T tmp = static_cast<T>(src >> (idx * size));
			target[N - idx - 1] = tmp;
		}
	}
	
	/**
	 * Overwrite the value of a single byte in an (integer) value.
	 * Assuming little endian ordering.
	 */
	template < typename T >
	inline bool set_byte_be(T& src, std::size_t idx, std::uint8_t value)
	{
		BOOST_ASSERT_MSG(sizeof(T) > idx, "index out of range");
		
		if (idx < sizeof(T))
		{
			union 
			{
				mutable std::uint8_t source_[ sizeof(T) ];
				T target_;
			}  u_;
			
			boost::ignore_unused_variable_warning(u_);
			u_.target_ = src;
			u_.source_[ idx ] = value;
			std::swap(u_.target_, src);
			return true;
		}
		return false;
	}
	
	/**
	 * Overwrite the value of a single byte in an (integer) value.
	 * Assuming big endian ordering.
	 */
	template < typename T >
	inline bool set_byte_le(T& src, std::size_t idx, std::uint8_t value)
	{
		return set_byte_be<T>(src, sizeof(T) - idx - 1, value);
	}
	
	/**
	 * Promotes i to a type printable as a number, regardless of type
	 *
	 * @see https://isocpp.org/wiki/faq/input-output#print-char-or-ptr-as-number
	 */
	template <typename T>
	auto promote_to_printable_integer_type(T i) -> decltype(+i)
	{
		typedef typename std::make_unsigned<T>::type U;
		return +i & std::numeric_limits<U>::max();
	}

	/**
	 * Specialized for enums
	 */
	template <typename T, class = typename std::enable_if< std::is_enum<T>::value >::type>
	auto promote_to_printable_integer_type(T e) -> decltype(+static_cast< typename std::underlying_type<T>::type >(e))
	{
		typedef typename std::underlying_type<T>::type	U;
		return promote_to_printable_integer_type(static_cast<U>(e));
	}
	
	//	Idea is to merge an arbitrary list of arguments into one value
	template < typename T, typename ...Args >
	T merge(Args&&... args)
	{
//		typedef typename std::tuple<Args...> tuple_type;
		//	ToDo: add up the size of each tempülate pack element and compare this size of T
//		static_assert(sizeof(T) == sizeof(tuple_type), "incompatible data types");

		//const std::size_t size = sizeof...(Args);

		//
		//	ToDo: initialize union
		//

		//union
		//{
		//	mutable tuple_type source_;
		//	T target_;
		//} u_;

		//u_.source_ = tuple_type(std::forward<Args>(args)...);
		//return u_.target_;

		return T();
	}



}	//	cyy 

#endif // CYY_UTIL_BRUTE_CAST_HPP
