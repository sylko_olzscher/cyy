/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_UTIL_DEMANGLE_H
#define CYY_UTIL_DEMANGLE_H

#include <string>
#include <typeinfo>


namespace cyy 
{
	std::string demangle( const char* mangled_name );

	template < typename T >
	std::string demangle()
	{
		return demangle(typeid(T).name());
	}
}
#endif
