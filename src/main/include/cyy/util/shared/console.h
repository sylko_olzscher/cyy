/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_TOOLS_CONSOLE_H
#define CYY_TOOLS_CONSOLE_H

#include <string>
#include <iostream>
#include <list>
#include <cyy/intrinsics/sets.hpp>
#include <boost/filesystem.hpp>

namespace cyy
{
	class console
	{
	public:
		console(std::ostream&, std::istream&);
		virtual ~console();
		
	protected:		
		virtual bool parse(std::string const&) = 0;
		
	protected:
		int loop();
 		virtual void set_prompt(std::iostream& os);
		/**
		 * Last chance to exit
		 */
 		virtual void shutdown();
 		bool exec(std::string const&);
 		bool quit();
 		bool history() const;
		
		//	commands
 		bool cmd_run(tuple_t::const_iterator pos, tuple_t::const_iterator end);
 		bool cmd_run_script(boost::filesystem::path const&);
 		bool cmd_echo(tuple_t::const_iterator pos, tuple_t::const_iterator end);
 		bool cmd_list(tuple_t::const_iterator pos, tuple_t::const_iterator end);
 		bool cmd_list(boost::filesystem::path const&);
		bool cmd_ip() const;

	protected:
		std::ostream&	out_;
		std::istream&	in_;
		bool exit_;
		std::list< std::string >	history_;
		std::size_t call_depth_;
	};
}	//	cyy

#endif	//	CYY_TOOLS_CONSOLE_H
