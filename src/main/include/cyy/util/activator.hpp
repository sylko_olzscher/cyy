/*
 * Copyright Sylko Olzscher 2017
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_ACTIVATOR_HPP
#define CYY_ACTIVATOR_HPP

#include <atomic>

namespace cyy 
{
	/**
	 * Helper class to increase and deacrease a value.
	 */
	template < typename T >
	class activator
	{
	public:
		explicit activator(T& val) 
			: val_(val)
		{
			++val_;
		}

		virtual ~activator() 
		{
			--val_;
		}

	private:
		T&	val_;
	};

	/**
	 * Helper class to increase and deacrease an (atomic)
	 * value.
	 */
	template < typename T >
	class activator< typename std::atomic< T > >
	{
	public:
		explicit activator(std::atomic< T >& val)
			: val_(val)
		{
			val_.fetch_add(1, std::memory_order_relaxed);
		}

		virtual ~activator()
		{
			val_.fetch_sub(1, std::memory_order_relaxed);
		}

	private:
		std::atomic< T >&	val_;
	};

}

#endif	//	CYY_ACTIVATOR_HPP
