/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

/** @file native_io.h
 * 
 * This file implements the native serializer and de-serializer
 * methods. The serializer methods requires a parser that fills
 * the stream in a correct manner and has already detected
 * the data type (T). This also clarifies that de-serializer
 * is not the (full) complement to the serialier function.
 * For a complete de-serialization is a parser required.
 */
#ifndef CYY_IO_NATIVE_IO_H
#define CYY_IO_NATIVE_IO_H

#include <cyy/cyy_export.h>
#include <cyy/io/io_callback.h>

namespace cyy 
{

	/**
	 * Write the value in a sort of TLV format into stream.
	 */
	CYY_IO_LIBRARY_API bool serialize_native(std::ostream& os, object const& obj, io::custom_callback cb);
	
	/**
	 * Read the value from stream.
	 * 
	 * @param code the data type code to read
	 */
	CYY_IO_LIBRARY_API object deserialize_native(std::istream& is, std::uint32_t code);

	
}	//	cyy

#endif // CYY_IO_NATIVE_IO_H
