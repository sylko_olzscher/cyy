/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#ifndef CYY_IO_WRITE_BINARY_HPP
#define CYY_IO_WRITE_BINARY_HPP

#include <iostream>
#include <type_traits>
#include <array>
#include <boost/assert.hpp>

namespace cyy 
{
	namespace io	
	{
		template < typename T, std::size_t N = sizeof(T) >
		std::size_t write_binary_lsb(std::ostream& io, T const* p)
		{
			static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value, "arithmetic data type required");
			static_assert(sizeof(T) >= N, "type size exceeded");
			BOOST_ASSERT_MSG(io.good(), "bad state");
			//const auto pos = io.tellp();
			io.write(reinterpret_cast<const std::ostream::char_type*>(p), N);
			//const auto count = io.tellp() - pos;
			//BOOST_ASSERT_MSG(count == N, "write incomplete");
			return N;
		}

		/**
		 * Copies N last bytes (LSB) from the arithmetic value into the stream. Default is to write all
		 * available bytes. If N exceeds the size of the integral value
		 * the compiler emits an error message.
		 * 
		 * @tparam T object to write
		 * @tparam N bytes to write
		 */
		template < typename T, std::size_t N = sizeof(T) >
		std::size_t write_binary(std::ostream& io, T const& v)
		{
			return write_binary_lsb<T, N>(io, &v);
		}
		

		/**
		 * Copies N first bytes (MSB) from the arithmetic value into the stream. Default is to write all
		 * available bytes. If N exceeds the size of the integral value
		 * the compiler emits an error message.
		 *
		 * @tparam T object to write
		 * @tparam N bytes to write
		 */
		template < typename T, std::size_t N = sizeof(T) >
		std::size_t write_binary_msb(std::ostream& io, T const* p)
		{
			static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value, "arithmetic data type required");
			static_assert(sizeof(T) >= N, "type size exceeded");			
			BOOST_ASSERT_MSG(io.good(), "bad state");
			//const auto pos = io.tellp();
			io.write(reinterpret_cast<const std::ostream::char_type*>(p) + (sizeof(T) - N), N);
			//const auto count = io.tellp() - pos;
			//BOOST_ASSERT_MSG(count == N, "write incomplete");
			return N;
		}

		/**
		 * Write an array of elements
		 */
		template < typename T, std::size_t N >
		std::size_t write_binary_array(std::ostream& io, T const(&p)[N])
		{
			static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value, "arithmetic data type required");
			io.write(reinterpret_cast<const std::ostream::char_type*>(p), sizeof(T) * N);
			BOOST_ASSERT_MSG(io.good(), "bad state");
			return sizeof(T) * N;
		}
		
		/**
		 * Write a std::array
		 */
		template < typename T, std::size_t N >
		std::size_t write_binary_array(std::ostream& io, std::array< T, N > const& a)
		{
			typedef typename std::array< T, N > array_type;
			static_assert(std::is_arithmetic<T>::value || std::is_enum<T>::value, "arithmetic data type required");
			static_assert(N == std::tuple_size<array_type>::value, "invalid template parameters");
			io.write(reinterpret_cast<const std::ostream::char_type*>(a.data()), N * sizeof(T));
			BOOST_ASSERT_MSG(io.good(), "bad state");
			return a.size() * N;
		}

		
	}	//	io
}	//	cyy

#endif // CYY_IO_WRITE_BINARY_HPP
