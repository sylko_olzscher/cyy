/*
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 * 
 * Copyright Sylko Olzscher 2016
 */

/** @file text_io.h
 * This file contains all the boring details
 * of writing the value of an object into
 * an output stream. Enjoy it.
 */

#ifndef CYY_IO_TEXT_H
#define CYY_IO_TEXT_H

#include <iostream>
#include <cstdint>
#include <cyy/intrinsics/type_traits.h>
#include <cyy/io/io_callback.h>

namespace cyy 
{
	namespace io	
	{

		/**
		 * Writes the data type signature to stream.
		 * 
		 * @param os output stream
		 */
		template < typename T >
		std::ostream& write_type(std::ostream& os)
		{
			typedef typename type_traits< typename std::decay<T>::type >::code_ code;
			os << types::name(code::value);
			return os;
		}	
		
		/**
		 * Resembles the behavior of the std::basic_ostream::operator<<().
		 * 
		 * @param os output stream
		 * @param value value to serialize
		 */
		template < typename T >
		std::ostream& write_value(std::ostream& os, T const& value)
		{
			os << value;
			return os;
		}	
		
		/**
		 * Write a boolean value as "true" of "false".
		 * Format flag boolalpha will not be used.
		 */
		std::ostream& write_value(std::ostream& os, bool value);
		
		/**
		 * Write the numeric value not the ASCII representation.
		 */
		std::ostream& write_value(std::ostream& os, std::int8_t value);

		/**
		 * Write the numeric value in decimal notation.
		 */
		std::ostream& write_value(std::ostream& os, std::uint8_t value);

		/**
		 * Write a version in major.minor notation.
		 * 
		 * @see version_format.h
		 */
		std::ostream& write_value(std::ostream& os, version const& value);

		/**
		 * Write a revision in a.b.x.y notation.
		 * 
		 * @see version_format.h
		 */
		std::ostream& write_value(std::ostream& os, revision const& value);

		/**
		 * Write a buffer as hex values.
		 */
		std::ostream& write_value(std::ostream& os, buffer_t const& value);

		/**
		 * Write the name of the op code
		 */
		std::ostream& write_value(std::ostream& os, op value);

		/**
		 * Write the value of the index in decimal notation
		 */
		std::ostream& write_value(std::ostream& os, index value);

		/**
		 * Write the value of a diff in decimal notation
		 */
		std::ostream& write_value(std::ostream& os, diff value);

		/**
		 * Write the value of a bytes class in decimal notation
		 */
		std::ostream& write_value(std::ostream& os, bytes value);

		/**
		 * Write the value of a color in hex notation.
		 */
		std::ostream& write_value(std::ostream& os, color_8 const& value);

		/**
		 * Write the value of a color in hex notation.
		 */
		std::ostream& write_value(std::ostream& os, color_16 const& value);

		/**
		 * Write the value of a MAC address
		 */
		std::ostream& write_value(std::ostream& os, mac48 const& value);

		/**
		* Write the value of an EUI-64 address
		*/
		std::ostream& write_value(std::ostream& os, mac64 const& value);

		/**
		 * Write the value of a MD5 digest
		 */
		std::ostream& write_value(std::ostream& os, cyy::crypto::digest_md5 const& value);
		
		/**
		 * Write the value of a SHA1 digest
		 */
		std::ostream& write_value(std::ostream& os, cyy::crypto::digest_sha1 const& value);

		/**
		 * Write the value of a SHA512 digest
		 */
		std::ostream& write_value(std::ostream& os, cyy::crypto::digest_sha512 const& value);

		/**
		 * Write values of an attribute map recursively
		 */
		std::ostream& write_value(std::ostream& os, attr_map_t const& value, custom_callback cb);
		
		/**
		 * Write values of a parameter map recursively
		 */
		std::ostream& write_value(std::ostream& os, param_map_t const& value, custom_callback cb);
		
		/**
		 * Write values of an attribute recursively
		 */
		std::ostream& write_value(std::ostream& os, attr_t const& value, custom_callback cb);
		
		/**
		 * Write values of a parameter recursively
		 */
		std::ostream& write_value(std::ostream& os, param_t const& value, custom_callback cb);
		
		/**
		 * Write values of a tuple recursively
		 */
		std::ostream& write_value(std::ostream& os, tuple_t const& value, custom_callback cb);

		/**
		 * Write values of a vector recursively
		 */
		std::ostream& write_value(std::ostream& os, vector_t const& value, custom_callback cb);

		/**
		 * Write values of a set recursively
		 */
		std::ostream& write_value(std::ostream& os, set_t const& value, custom_callback cb);

		
		/**
		 * Write a UUID tag in a xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx format.
		 */
		std::ostream& write_value(std::ostream& os, boost::uuids::uuid value);
		
		/**
		 * Delimit the string with double-quotes (") to ensure that paths with embedded spaces 
		 * will round trip correctly. Ampersand (&) is used as an escape character, so the path 
		 * can itself contain double quotes.
		 */
		std::ostream& write_value(std::ostream& os, boost::filesystem::path const& value);
		
		/**
		 * Write an IP address.
		 */
		std::ostream& write_value(std::ostream& os, boost::asio::ip::address const& value);
		
		/**
		 * Write a time stamp. Example: 2015.04.20 09:46:02,5765
		 */
		std::ostream& write_value(std::ostream& os, std::chrono::system_clock::time_point const& value);
		
		/**
		 * Write a time span value in binary format a tick count
		 * 
		 * @tparam R an arithmetic type representing the number of ticks
		 * @tparam P a std::ratio representing the tick period (i.e. the number of seconds per tick)
		 */
		template < typename R, typename P >
 		std::ostream& write_value(std::ostream& os, std::chrono::duration<R, P> const& value)
		{
			//	write count of ticks
			os << std::dec << value.count();
			return os;
		}

		
		/**
		 * Write tribool value (true, false or indeterminate)
		 */
		std::ostream& write_value(std::ostream& os, boost::logic::tribool value);
		
		/**
		 * Writes a the value and the type information to stream
		 * in syntactical unique way.
		 * 
		 * @param os output stream
		 * @param value value to serialize
		 */
		template < typename T >
		std::ostream& write_value_type(std::ostream& os, T const& value)
		{
			write_value< T >(os, value);
			return write_type< T >(os);
		}
		
		/**
		 * Write a string values to stream and escapes all non-printable
		 * characters
		 */
		template <>
		std::ostream& write_value_type<std::string>(std::ostream& os, std::string const& value);
			
		/**
		 * Write a time stamp. Example: 2015.04.20 09:46:02,5765
		 */
		template <>
		std::ostream& write_value_type<std::chrono::system_clock::time_point>(std::ostream& os, std::chrono::system_clock::time_point const& value);

		/**
		 * Write a version in major.minor notation.
		 * 
		 * @see version_format.h
		 */
		template <>
		std::ostream& write_value_type<version>(std::ostream& os, version const& value);

		/**
		 * Write a revision in a.b.x.y notation.
		 * 
		 * @see version_format.h
		 */
		template <>
		std::ostream& write_value_type<revision>(std::ostream& os, revision const& value);
		
		/**
		 * Write a buffer as hex values.
		 */
		template <>
		std::ostream& write_value_type(std::ostream& os, buffer_t const& value);
		
		/**
		 * Write the value of a bytes class in decimal notation
		 */
		std::ostream& write_value_type(std::ostream& os, bytes value);
		
		/**
		 * Write the value of a color in hex notation.
		 * A color with 8 bytes each channel is prefixed by "#".
		 */
		template <>
		std::ostream& write_value_type<color_8>(std::ostream& os, color_8 const& value);

		/**
		 * Write the value of a color in hex notation.
		 * A color with 16 bytes each channel is prefixed by "##".
		 */
		template <>
		std::ostream& write_value_type<color_16>(std::ostream& os, color_16 const& value);
		
		/**
		 * Write the value of a MAC address
		 */
		template <>
		std::ostream& write_value_type<mac48>(std::ostream& os, mac48 const& value);
		
		/**
		* Write the value of a EUI-64 address
		*/
		template <>
		std::ostream& write_value_type<mac64>(std::ostream& os, mac64 const& value);

		/**
		* Write the value of a MD5 digest
		*/
		template <>
		std::ostream& write_value_type<crypto::digest_md5>(std::ostream& os, crypto::digest_md5 const& value);

		/**
		* Write the value of a SHA1 digest
		*/
		template <>
		std::ostream& write_value_type<crypto::digest_sha1>(std::ostream& os, crypto::digest_sha1 const& value);

		/**
		* Write the value of a SHA512 digest
		*/
		template <>
		std::ostream& write_value_type<crypto::digest_sha512>(std::ostream& os, crypto::digest_sha512 const& value);

		/**
		 * Write values of a tuple recursively
		 */
		std::ostream& write_value_type(std::ostream& os, tuple_t const& value, custom_callback cb);
		
		/**
		 * Write values of a vector recursively
		 */
		std::ostream& write_value_type(std::ostream& os, vector_t const& value, custom_callback cb);

		/**
		 * Write values of a set recursively
		 */
		std::ostream& write_value_type(std::ostream& os, set_t const& value, custom_callback cb);

		/**
		* Write type/values of an attribute map recursively
		*/
		std::ostream& write_value_type(std::ostream& os, attr_map_t const& value, custom_callback cb);

		/**
		* Write type/values of a parameter map recursively
		*/
		std::ostream& write_value_type(std::ostream& os, param_map_t const& value, custom_callback cb);

		/**
		* Write values of an attribute recursively
		*/
		std::ostream& write_value_type(std::ostream& os, attr_t const& value, custom_callback cb);

		/**
		* Write values of a parameter recursively
		*/
		std::ostream& write_value_type(std::ostream& os, param_t const& value, custom_callback cb);

		
		/**
		 * Write a UUID tag in a xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx format.
		 */
		std::ostream& write_value_type(std::ostream& os, boost::uuids::uuid value);
		
		/**
		 * Delimit the string with double-quotes (") to ensure that paths with embedded spaces 
		 * will round trip correctly. Ampersand (&) is used as an escape character, so the path 
		 * can itself contain double quotes.
		 */
		template <>
		std::ostream& write_value_type<boost::filesystem::path>(std::ostream& os, boost::filesystem::path const& value);

		/**
		 * Write an IP address.
		 */
		template <>
		std::ostream& write_value_type(std::ostream& os, boost::asio::ip::address const& value);
		
	}	//	io
}	//	cyy

#endif // CYY_IO_TEXT_H
