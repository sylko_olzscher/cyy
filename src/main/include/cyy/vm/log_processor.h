/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

/** @file: log_processor.h
 *
 * This file is not intendent to be part of a library. Include this file to use it 
 * in your ptoject.
 */
#ifndef CYY_LOG_PROCESSOR_H
#define CYY_LOG_PROCESSOR_H

#include <cyy/scheduler/logging/log.h>
#include <cyy/vm/vm_dispatcher.h>
#include <cyy/vm/code_generator.h>
#include <cyy/io/format/boost_format.h>
#include <cyy/value_cast.hpp>

namespace cyy 
{
	inline void register_logger(logger_type& logger, vm_dispatcher& vm)
	{
		vm.async_run(register_function("log.msg.trace", 1, [&logger](context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(true);
			CYY_LOG_TRACE(logger, "session ["
				<< uuid_short_format(ctx.get_tag())
				<< "] - "
				<< string_cast(frame.at(0)));
		}));

		vm.async_run(register_function("log.msg.debug", 1, [&logger](context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(true);
			CYY_LOG_DEBUG(logger, "session ["
				<< uuid_short_format(ctx.get_tag())
				<< "] - "
				<< string_cast(frame.at(0)));
		}));
		vm.async_run(register_function("log.msg.info", 1, [&logger](context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(true);
			CYY_LOG_INFO(logger, "session ["
				<< uuid_short_format(ctx.get_tag())
				<< "] - "
				<< string_cast(frame.at(0)));
		}));
		vm.async_run(register_function("log.msg.warning", 1, [&logger](context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(true);
			CYY_LOG_WARNING(logger, "session ["
				<< uuid_short_format(ctx.get_tag())
				<< "] - "
				<< string_cast(frame.at(0)));
		}));
		vm.async_run(register_function("log.msg.error", 1, [&logger](context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(true);
			CYY_LOG_ERROR(logger, "session ["
				<< uuid_short_format(ctx.get_tag())
				<< "] - "
				<< string_cast(frame.at(0)));
		}));
		vm.async_run(register_function("log.msg.fatal", 1, [&logger](context& ctx) {
			const cyy::vector_t frame = ctx.stack_frame(true);
			CYY_LOG_FATAL(logger, "session ["
				<< uuid_short_format(ctx.get_tag())
				<< "] - "
				<< string_cast(frame.at(0)));
		}));

	}
}

#endif // CYY_LOG_PROCESSOR_H

