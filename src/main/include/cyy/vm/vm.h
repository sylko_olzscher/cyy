/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */
#ifndef CYY_VM_H
#define CYY_VM_H

#include <cyy/cyy_export.h>
#include <cyy/vm/stack.h>
#include <cyy/vm/librarian.h>
#include <iostream>
#include <boost/uuid/uuid.hpp>

namespace cyy 
{
	class memory;
	class context;
	class vm_dispatcher;

	/**
	 * Implements a simple stack oriented machine
	 * 
	 * @see op_codes.h
	 */
	class CYY_VM_LIBRARY_API vm
	{
		friend class context;
		friend class vm_dispatcher;
		
	public:
		vm(std::ostream& = std::cout, std::ostream& = std::cerr);
		vm(boost::uuids::uuid, std::ostream& = std::cout, std::ostream& = std::cerr);
		
		/**
		 * @return VM tag
		 */
		boost::uuids::uuid tag() const;
		
		/**
		 * Execute instructions stored in the
		 * memory 
		 */
		boost::system::error_code const& run(memory&);
		boost::system::error_code const& run_prg(vector_t&&);
		
		/**
		 * out of band execution (sync)
		 */
		boost::system::error_code const& squeeze(vector_t&&);

	private:
		/**
		 * direct call of function
		 */
		bool direct(std::string const&, vector_t&&);

		/**
		 * execute a single operation
		 */
		void execute(memory&, op const&);

		/**
		 * invoke a library function
		 */
		void invoke(memory& mem);

		/**
		 * invoke a library function
		 *
		 * @return true if function was found
		 */
		bool invoke(memory& mem, std::string const&);

		/**
		 * halt VM
		 */
		void halt(memory& mem);
		
		/**
		 * mem[sp] = mem[mem[sp]]
		 */
		void load(memory& mem);

		/**
		 * jump always, pc = x		
		 */
		void ja(memory& mem);
		
		/**
		 * je <label> (jump when equal/true)
		 */
		void je(memory& mem);

		/**
		 * jne <label> (jump when not equal/false)
		 */
		void jne(memory& mem);

		/**
		 * store addr/value, temp = mem[sp++]; mem[mem[sp++]] = temp
		 */
		void stav(memory& mem);
		
		/**
		 * store value/address, temp = mem[sp++]; mem[temp] = mem[sp++]
		 */
		void stva(memory& mem);
		
		/**
		 * Push return address to the stack and transfers control
		 * to the specified function.
		 */
		void call(memory& mem);
		
		/**
		 * return, pc = mem[sp++]
		 */
		void ret(memory& mem);

		/**
		 * push, mem[--sp] = mem[x]
		 */
		void psh(memory& mem);
		
		/**
		 * Turn debug mode on or off
		 */
		void dbg(memory&, bool arg1);
		
		/**
		 * Removed the top elements from stack,
		 * performs an add operation and put result onto
		 * top of stack.
		 * 
		 * @code
		 * temp = mem[sp++]; mem[sp] = mem[sp] + temp; cy = carry
		 * @endcode
		 * 
		 * @return carry flag (for integer operations)
		 */
		void add();
		
		/**
		 * Removed the top elements from stack,
		 * performs a sub operation and put result onto
		 * top of stack.
		 * 
		 * @code
		 * temp = mem[sp++]; mem[sp] = mem[sp] - temp; 
		 * @endcode
		 * 
		 */
		void sub();
		void mul();
		void div();
		
		/**
		 * Swap two top elements of stack
		 */
		void swap();

		/**
		 * add to sp, sp = (sp + s)
		 * increase SP by value s (multiple pops)
		 */
		void asp();

		/**
		 * push releative
		 * mem[--sp] = mem[bp + s]
		 */
		void pr();

		/**
		 * Cannot name this function dup() because ::dup() 
		 * is C system call.
		 * 
		 * temp = mem[sp]; 
		 * mem[--sp] = temp
		 */
		void dupe();
		
	private:
		boost::uuids::uuid	tag_;
		std::ostream	&out_, &err_;
		stack stack_;
		boost::system::error_code	last_error_;
		bool cmp_register_;
		librarian	lib_;
		bool debug_;
	};
}

#endif // CYY_VM_H

