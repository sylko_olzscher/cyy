/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

/** @file base64.hpp
 *	base64 is a data encoding scheme whereby binary-encoded data is
 *	converted to printable ASCII characters. It is defined as a MIME
 *	content transfer encoding for use in internet e-mail.
 * 	The characters used are A-Z, a-z and 0-9 with the "+" and "/" symbols.
 *	The "=" symbol as a special suffix code.
 *
 *	Full specifications for base64 are contained in RFC 1421 and RFC 2045 -
 *	MIME (Multipurpose Internet Mail Extensions). The scheme works
 *	on 8 bit data. The resultant base64-encoded data has a length that
 *	is greater than the original length by the ratio 4:3.
 *
 *	Three bytes are concatenated, then split to form 4 groups of 6-bits each;
 * 	and each 6-bits gets translated to an encoded printable ASCII character,
 *	via a table lookup. An encoded string is therefore longer than
 *	the original by about 1/3. The "=" character is used to pad the end out
 *	to an even multiple of four.
 *
 *	Example:
 *	@code
const std::string inp = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce ornare ullamcorper ipsum ac gravida.";
std::string out = cyy::crypto::base64::encode<>(inp.begin(), inp.end());

std::string result;
auto bi = std::back_inserter( result );
cyy::crypto::base64::decode<>( out, bi );
BOOST_ASSERT(inp == result);

//	use a vector as output
std::vector<char> result2;
auto bv = std::back_inserter( result2 );
cyy::crypto::base64::decode<>( out, bv );

 *	@endcode
 */
#ifndef CYY_CRYPTO_BASE64_HPP
#define CYY_CRYPTO_BASE64_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <string>
#include <iterator>
#include <cyy/intrinsics/digest.h>
#include <cyy/intrinsics/buffer.hpp>

#include <boost/assert.hpp>

namespace cyy 
{
	namespace crypto	
	{
		namespace base64	
		{

			static const char nop = -1;

			/**
			*	@return values from char encoding_data[] array
			*/
			char decoding_char(int);

			/** base64 decoding
			*
			*	Standard iterator library provides the usefull std::back_insert() to get
			*	an output iterator for standard container.
			*
			* @param input - base64 encoded string
			* @param out - output iterator
			* @return true if successful, false if input string contains non-base64 symbols
			*/
			template < typename I >
			bool decode(const std::string &inp, I out)
			{

				// for each 4-bytes sequence from the input, extract 4 6-bits sequences by dropping first two bits
				// and regenerate into 3 8-bits sequence

				unsigned phase = 0;
				char base64code0;
				char base64code1;
				char base64code2 = 0;   // initialized to 0 to suppress warnings
				char base64code3;
				for (std::string::const_iterator pos = inp.begin(); pos != inp.end(); ++pos) 	{
					switch (phase)
					{
					case 0:
						base64code0 = decoding_char(static_cast<int>(*pos));
						if (base64code0 == nop)
						{
							// non base64 character
							return false;
						}
						phase++;
						break;
					case 1:
						base64code1 = decoding_char(static_cast<int>(*pos));
						if (base64code1 == nop)
						{
							// non base64 character
							return false;
						}

						*out = ((base64code0 << 2) | ((base64code1 >> 4) & 0x3));
						++out;
						phase++;
						break;
					case 2:
						if (*pos == '=')
						{
							// padding , end of input
							assert((base64code1 & 0x0f) == 0);
							return true;
						}
						base64code2 = decoding_char(static_cast<int>(*pos));
						if (base64code2 == nop)
						{	// non base64 character
							return false;
						}

						*out = ((base64code1 << 4) & 0xf0) | ((base64code2 >> 2) & 0x0f);
						++out;
						phase++;
						break;
					case 3:
						if (*pos == '=')
						{
							// padding , end of input
							assert((base64code2 & 0x03) == 0);
							return true;
						}

						base64code3 = decoding_char(static_cast<int>(*pos));
						if (base64code3 == nop)
						{
							// non base64 character
							return false;
						}

						*out = (((base64code2 << 6) & 0xc0) | base64code3);
						++out;
						phase = 0;
						break;
					default:
						BOOST_ASSERT_MSG(false, "base64 decoding error");
						break;
					}
				}

				switch (phase)
				{
				case 1:
					// we need at least two input bytes for first byte output
					return false;
				default:
					break;
				}

				return true;

			}

			/**
			*	@return values from char encoding_data[] array
			*/
			char encoding_char(int);

			/** base64 encoding
			*
			*	Dereferenced iterator must return a value of type "char".
			*
			* @param begin - start of arbitrary input range
			* @param end - end of arbitrary input range
			* @return base64 encoded string
			*/
			template < typename I >
			std::string encode(I begin, I end)
			{
				const std::size_t length = std::distance(begin, end);

				// allocate space for output string
				std::string out;
				out.reserve(((length + 2) / 3) * 4);

				// for each 3-bytes sequence from the input, extract 4 6-bits sequences and encode using 
				// encoding_data lookup table.
				// if input do not contains enough chars to complete 3-byte sequence,use pad char '=' 
				unsigned phase = 0;
				int base64code0 = 0;
				int base64code1 = 0;
				int base64code2 = 0;
				int base64code3 = 0;
				for (I pos = begin; pos != end; ++pos) 		{
					switch (phase)
					{
					case 0:
						base64code0 = (*pos >> 2) & 0x3f;	// 1-byte 6 bits
						out += encoding_char(base64code0);
						base64code1 = (*pos << 4) & 0x3f;	// 1-byte 2 bits +
						phase++;
						break;
					case 1:
						base64code1 |= (*pos >> 4) & 0x0f; // 2-byte 4 bits
						out += encoding_char(base64code1);
						base64code2 = (*pos << 2) & 0x3f;  // 2-byte 4 bits + 
						phase++;
						break;
					case 2:
						base64code2 |= (*pos >> 6) & 0x03; // 3-byte 2 bits
						base64code3 = *pos & 0x3f;		  // 3-byte 6 bits
						out += encoding_char(base64code2);
						out += encoding_char(base64code3);
						phase = 0;
						break;
					default:
						BOOST_ASSERT_MSG(false, "base64 encoding error");
						break;
					}
				}

				//	closing
				switch (phase)
				{
				case 2:
					out += encoding_char(base64code2);
					out += '=';
					break;
				case 1:
					out += encoding_char(base64code1);
					out += '=';
					out += '=';
					break;
				default:
					break;
				}

				return out;
			}


		}	//	namespace base64

		//
		//	for convience
		//

		/**
		 * decrypt content (remove encryption)
		 */
		cyy::buffer_t decode_base64(std::string const& s);

		/**
		 * encrypt content (add encryption)
		 */
		std::string encode_base64(std::string const& s);

		std::string encode_base64(crypto::digest_sha1::value_type const& s);
		std::string encode_base64(crypto::digest_sha256::value_type const& s);
		std::string encode_base64(crypto::digest_sha512::value_type const& s);

	}	//	crypto
}	//	cyy

#endif	//	CYY_CRYPTO_BASE64_HPP
