/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2016 Sylko Olzscher.
 *
 */

#ifndef CYY_CRYPTO_AES_H
#define CYY_CRYPTO_AES_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
  #pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <string>
#include <openssl/sha.h>
#include <openssl/aes.h>
//#include <cyy/intrinsics/digest.h>
#include <cyy/cyy_export.h>
#include <cyy/intrinsics/buffer.hpp>
#include <array>
#include <cstdint>

namespace cyy
{
	namespace crypto
	{
		struct aes_128_key
		{
			//	16 bytes
			typedef std::array<std::uint8_t, 128 / 8>	key_type;
			key_type	aes_key_;
			
			aes_128_key()
			: aes_key_({ { 0 } })
			{}
			
			aes_128_key(key_type const& key)
			: aes_key_(key)
			{}
			
			aes_128_key(key_type&& key)
			: aes_key_(std::forward<key_type>(key))
			{}
		};

		struct aes_192_key
		{
			//	24 bytes
			typedef std::array<std::uint8_t, 192 / 8>	key_type;
			key_type aes_key_;
			
			aes_192_key()
			: aes_key_({ { 0 } })
			{}
			
			aes_192_key(key_type const& key)
			: aes_key_(key)
			{}
			
			aes_192_key(key_type&& key)
			: aes_key_(std::forward<key_type>(key))
			{}
		};

		struct aes_256_key
		{
			//	32 bytes
			typedef std::array<std::uint8_t, 256 / 8>	key_type;
			key_type aes_key_;

			aes_256_key()
			: aes_key_({ { 0 } })
			{}
			
			aes_256_key(key_type const& key)
			: aes_key_(key)
			{}
			
			aes_256_key(key_type&& key)
			: aes_key_(std::forward<key_type>(key))
			{}
		};


		void encrypt(buffer_t&, buffer_t const&, aes_256_key const&);
		void decrypt(buffer_t&, buffer_t const&, aes_256_key const&);

	}

}

#endif	//	CYY_CRYPTO_AES_H

