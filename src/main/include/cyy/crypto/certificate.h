/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_CRYPTO_CERTIFICATE_H
#define CYY_CRYPTO_CERTIFICATE_H

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
  #pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <openssl/bn.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
#include <openssl/x509.h>
#include <memory>
#include <string>

namespace cyy 
{
	namespace crypto 
	{
		using BN_ptr = std::unique_ptr<BIGNUM, decltype(&::BN_free)>;
		using RSA_ptr = std::unique_ptr<RSA, decltype(&::RSA_free)>;
		using EVP_KEY_ptr = std::unique_ptr<EVP_PKEY, decltype(&::EVP_PKEY_free)>;
		using BIO_FILE_ptr = std::unique_ptr<BIO, decltype(&::BIO_free)>;
		using X509_REQ_ptr = std::unique_ptr<X509_REQ, decltype(&::X509_REQ_free)>;
		//using X509_NAME_ptr = std::unique_ptr<X509_NAME, decltype(&::???)>;

		/**
		 * Create an RSA private key
		 *
		 * @code
		 * openssl genrsa -des3 -out server.key 1024
		 * @endcode
		 */
		RSA_ptr generate_RSA_private_key(int bits = 1024, BN_ULONG exp = RSA_3);

		/**
		 * Convert RSA to PKEY
		 */
		EVP_KEY_ptr generate_public_key(RSA*);

		/**
		 * Create an RSA private key
		 *
		 * @code
		 * openssl genrsa -des3 -out server.key 1024
		 * @endcode
		 */
		bool write_RSA_private_key(std::string const&, int bits = 1024, BN_ULONG exp = RSA_3);

		//	server.key
		bool write_RSA_key(std::string const&, RSA*);
		//	server.pem
		bool write_EVP_key(std::string const&, EVP_PKEY*);


		/**
		 * Generate certificate request.
		 * 
		 * @param rsa_pk will be freed inside the function. Use RSA_ptr
		 * with release().
		 *
		 * @code
		 * openssl req -new -key server.key -out server.csr
		 * @endcode
		 */
		bool gen_X509_request(RSA* rsa_pk
			, std::string const& file_name
			, std::string const& country
			, std::string const& province
			, std::string const& city
			, std::string const& org
			, std::string const& common);

		//	ToDo:
		//	search for: "Generate self-signed certificate"
		//	http://community.actian.com/w/files/5/58/Ossl_server.c
	}
	
}

#endif	//	CYY_CRYPTO_CERTIFICATE_H

