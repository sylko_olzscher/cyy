/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/


#ifndef CYY_MATH_BINARY_OR_H
#define CYY_MATH_BINARY_OR_H

#include <cyy/cyy_export.h>
#include <cyy/object.h>

namespace cyy 
{
	CYY_LIBRARY_API object binary_or(object const&, object const&);
	CYY_LIBRARY_API object operator|(object const&, object const&);
}

#endif // CYY_MATH_BINARY_OR_H

