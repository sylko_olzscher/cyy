/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */
/** @file size_policy.hpp
 *
 * Size policy is an attempt to generalize sizeof() and ...size()
 * methods of individual classes and containers. We distinguish between 
 * the bytes allocated on stack (sizeof()) and the that bytes allocated 
 * on the heap (size()). 
 */
#ifndef CYY_INTRINSICS_SIZE_POLICY_HPP
#define CYY_INTRINSICS_SIZE_POLICY_HPP

#include <cyy/cyy_export.h>
#include <type_traits>
#include <string>
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/intrinsics/null.h>
#include <cyy/intrinsics/eod.h>
#include <cyy/object_cast.hpp>
#include <cyy/intrinsics/cyystddef.h>

#include <boost/config.hpp>

namespace cyy
{
	/**
	 * To specialize for all types which are not PODs
	 * but are of fixed size
	 */
	template < typename T >
	struct is_fixed_size : std::is_pod< T >::type 
	{};
	
	/**
	 * @return the size in bytes required to serialize
	 * this value in raw binary format.
	 */
	template < typename T >
	std::size_t get_binary_size(T const& /* unused */ )
	{
		static_assert(is_fixed_size< T >::value, "get_binary_size() to specialize");
		return sizeof(T);
	}
	

	//
	//	specializations
	//
	
	//
	//	specializations - string
	//

	/**
	 * @return length of the string
	 */
	std::size_t get_binary_size(std::string  const& value );

	//
	//	specializations - buffer_t
	//
	
	/**
	 * @return length of the buffer
	 */
	std::size_t get_binary_size(buffer_t  const& value );

	/**
	 * specializations - null. null is of fixed size
	 * bit requires no stack size
	 */
	template <>
	struct is_fixed_size< null > : std::true_type
	{};
	
	/**
	 * A null type has no value.
	 * 
	 * @return 0 
	 */
	std::size_t get_binary_size(null const& /* unused */ );

	/**
	 * specializations - undef. undef is of fixed size
	 * bit requires no stack size
	 */
	template <>
	struct is_fixed_size< undef > : std::true_type
	{};
	
	/**
	 * A undef type has no value.
	 * 
	 * @return 0 
	 */
	std::size_t get_binary_size(undef const& /* unused */ );

	/**
	 * specializations - eod. eod is of fixed size
	 * bit requires no stack size
	 */
	template <>
	struct is_fixed_size< eod > : std::true_type
	{};
	
	/**
	 * A eod type has no value.
	 * 
	 * @return 0 
	 */
	std::size_t get_binary_size(eod const& /* unused */ );
	
	
	/**
	 * An index has a fixed size
	 */
	template <>
	struct is_fixed_size< index > : std::true_type
	{};
	
	/**
	 * A diff has a fixed size
	 */
	template <>
	struct is_fixed_size< diff > : std::true_type
	{};

	/**
	 * A std::complex<T> has a fixed size
	 * if the internal data type is fixed size
	 */
	template <typename T >
	struct is_fixed_size< std::complex< T > > : is_fixed_size< T >
	{};

	/**
	 * A std::chrono::system_clock::time_point has a fixed size
	 */
	template <>
	struct is_fixed_size< std::chrono::system_clock::time_point > : std::true_type
	{};

	/**
	 * A version has a fixed size
	 */
	template <>
	struct is_fixed_size< version > : std::true_type
	{};
	
	/**
	 * A revision has a fixed size
	 */
	template <>
	struct is_fixed_size< revision > : std::true_type
	{};

	/**
	 * A time span has a fixed size
	 * 
	 * @tparam R an arithmetic type representing the number of ticks
	 * @tparam P a std::ratio representing the tick period (i.e. the number of seconds per tick)
	 */
	template <typename R, typename P>
	struct is_fixed_size< std::chrono::duration<R, P> > : std::true_type
	{};
	
	/**
	 * A VM op has a fixed size
	 */
	template <>
	struct is_fixed_size< op > : std::true_type
	{};

	/**
	 * Any color has a fixed size
	 */
	template <typename T >
	struct is_fixed_size< color< T > > : std::true_type
	{};

	/**
	 * A MAC48 address has a fixed size
	 */
	template <>
	struct is_fixed_size< mac48 > : std::true_type
	{};

	/**
	 * A MAC64 address has a fixed size
	 */
	template <>
	struct is_fixed_size< mac64 > : std::true_type
	{};
	
	/**
	 * A MD5 hash has a fixed size
	 */
	template <>
	struct is_fixed_size< cyy::crypto::digest_md5 > : std::true_type
	{};

	/**
	 * A SHA1 hash has a fixed size
	 */
	template <>
	struct is_fixed_size< cyy::crypto::digest_sha1 > : std::true_type
	{};
	
	/**
	 * A SHA512 hash has a fixed size
	 */
	template <>
	struct is_fixed_size< cyy::crypto::digest_sha512 > : std::true_type
	{};

	
}	//	cyy
	

#endif // CYY_INTRINSICS_SIZE_POLICY_HPP
 
