/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#ifndef CYY_INTRINSICS_GENERIC_FACTORY_HPP
#define CYY_INTRINSICS_GENERIC_FACTORY_HPP

#include <cyy/intrinsics/factory.h>
#include <cyy/intrinsics/type_traits.h>
#include <cyy/util/meta.hpp>
#include <initializer_list>

namespace cyy 
{
	/**
	 * position_t is an empty struct tag type used to insert the current parameter pack
	 * size at compile time.
	 */
	struct position_t
	{};

	/**
	 * Calls object factory dependend from specified
	 * type code.
	 * 
	 * @param v R-value to initialize object factory.
	 *
	 *	example
	 * @code
	 cyy::create<cyy::types::CYY_BOOL>(true);
	 * @endcode
	 */
	template < types::code C >
	object create(typename reverse_type< C >::type&& v)
	{
		return factory(std::forward<typename reverse_type< C >::type>(v));
	}
	
	namespace
	{
		//
		//	anonymous namespace
		//

		//
		//	the transform() method help to tweak the factory() method
		//
		
		//	default
		template < typename T >
		object transform(std::size_t, T&& v)
		{
#if defined(CYY_DISABLE_EXPLICT_FACTORY)
			return core::make_value(std::forward<T>(v));
#else
			return factory(std::forward<T>(v));
#endif
		}
		
		/**
		 * @brief specialization to prevent nested objects
		 * 
		 * Identity function for objects as R-value reference
		 */
		inline object transform(std::size_t, object&& obj)
		{
			return obj;
		}

		/**
		 * @brief specialization to prevent nested objects
		 *
		 * Identity function for objects as const L-value reference
		 */
		inline object transform(std::size_t, object const& obj)
		{
			return obj;
		}

		/**
		 * @brief specialization to prevent nested objects
		 *
		 * Identity function for objects as L-value reference
		 */
		inline object transform(std::size_t, object& obj)
		{
			return obj;
		}

		/**
		 * @brief specialization to handle C-strings as std::string
		 */
		template < std::size_t N >
		object transform(std::size_t, char const(&v)[ N ])
		{
			return factory(std::string(v, N - 1));
		}
		
		/**
		 * @brief get current parameter pack size
		 */
// 		template <>
		inline object transform/*<position_t>*/(std::size_t size, position_t&&)
		{
			return index_factory(size);
		}

		/**
		 * @brief Transforms (any) pair of type std::pair<std::size_t, T>
		 * into an object of type attribute.
		 */
		template <typename T >
		object transform(std::size_t, std::pair<std::size_t, T>&& attr)
		{
			return set_factory(attr.first, std::forward<T>(attr.second));
		}
		
		/**
		 * @brief Transforms (any) pair of type std::pair<std::string, T>
		 * into an object of type parameter.
		 */
		template <typename T >
		object transform(std::size_t, std::pair<std::string, T>&& param)
		{
			return set_factory(param.first, std::forward<T>(param.second));
		}

	}

	namespace workbench
	{
		//
		//	workbench namespace
		//

		template <typename T>
		void tuple_builder(tuple_t& tpl, T&& v)
		{
			tpl.push_back(transform(0, std::forward<T>(v)));
		}

		//void tuple_builder(tuple_t& tpl, object&& obj)
		//{
		//	tpl.push_back(std::move(obj));
		//}

		/**
		 * An attribute map (attr_map_t) will be converted into
		 * a tuple of attributes
		 */
		template <>
		inline void tuple_builder<attr_map_t>(tuple_t& tpl, attr_map_t&& am)
		{
			std::for_each(am.begin(), am.end(), [&tpl](const_attr_t const& attr){
				tpl.push_back(factory(attr.first, attr.second));
			});
		}

		/**
		 * A parameter map (param_map_t) will be converted into
		 * a tuple of parameters
		 */
		template <>
		inline void tuple_builder<param_map_t>(tuple_t& tpl, param_map_t&& pm)
		{
			std::for_each(pm.begin(), pm.end(), [&tpl](const_param_t const& param){
				tpl.push_back(factory(param.first, param.second));
			});
		}

		template < typename T, typename ...Args >
		void tuple_builder(tuple_t& tpl, T&& v, Args&&... args)
		{
			static const std::size_t count = sizeof...(Args);
			tpl.push_back(transform(count, std::forward<T>(v)));
			tuple_builder(tpl, std::forward<Args>(args)...);
		}
	}

	/**
	 *	Tuple generator accepts an arbitrary list of object types.
	 *
	 *	Example:
	 *	@code
	 tuple_t tpl = cyy::tuple_generator( "1", 2, std::chrono::seconds(3));
	 *	@endcode
	 */
	template < typename ...Args >
	tuple_t tuple_generator(Args&&... args)
	{
		tuple_t tpl;
		workbench::tuple_builder(tpl, std::forward<Args>(args)...);
		return tpl;
	}

	/**
	 * With C++17 this functor object can be substituted by
	 * a lambda function with [](auto&&... args){ ; }
	 */
	struct tuple_gen_functor
	{
#if defined(CYY_COMPATIBILITY_MODE) 
        using result_type = void;
#endif
        
		tuple_t& data_;
		tuple_gen_functor(tuple_t& data)
			: data_(data)
		{}

		template<typename ...Args>
		void operator() (Args&&... args)
		{
 			workbench::tuple_builder(data_, std::forward<Args>(args)...);
		}
	};

	/**
	 * tuple generator specialized for std::tuple<...>
	 *
	 * @param arg an arbitrary std::tuple<...>. Only rvalues are
	 * accepted.
	 *
	 *	Example:
	 *	@code
	 tuple_t tpl = cyy::tuple_generator(std::make_tuple(std::string("1"), 2, std::chrono::seconds(3)));
	 *	@endcode
	 *
	 * Tuple generator doesn't work with tuples in compatibility mode. 
	 * This problem is open yet.
	 */
	template < typename ...Args >
	tuple_t tuple_generator(std::tuple<Args... >&& arg)
	{
		using tuple_type = typename std::tuple<Args...>;
		using unpack_t = mp_unpack<tuple_type>;

    //
    //  tuple generator doesn't work with tuples in compatibility mode.
    //  This problem is open yet.
    //
        
		tuple_t tpl;
//		unpack_t::invoke(tuple_gen_functor(tpl), std::move(arg));
		unpack_t::invoke(tuple_gen_functor(tpl), std::forward<tuple_type>(arg));
		return tpl;
	}

	/**
	 *	Tuple object factory accepts an arbitrary list of object types.
	 *
	 *	example:
	 *	@code
	 auto obj = cyy::tuple_factory( "1", 2, std::chrono::seconds(3));
	 *	@endcode
	 */
	template < typename ...Args >
	object tuple_factory(Args&&... args)
	{
		return factory(tuple_generator(std::forward<Args>(args)...));
	}

	/**
	 * Tuple object factory specialized for std::tuple<...>
	 */
	template < typename ...Args >
	object tuple_factory(std::tuple<Args... >&& tpl)
	{
		typedef typename std::tuple<Args...> tuple_type;
		return factory(tuple_generator(std::forward<tuple_type>(tpl)));
	}

	namespace workbench
	{
		//
		//	anonymous namespace
		//

		template < typename T >
		struct vector_factory_helper
		{
			typedef T type;
			object operator()(std::initializer_list<T> il)
			{
				vector_t vec;
				std::for_each(il.begin(), il.end(), [&vec](T const& v){

					vec.push_back(factory(v));

				});
				return factory(vec);
			}
		};

		/**
		 * vector factory specialized for attribute values.
		 */
		template < typename T >
		struct vector_factory_helper<std::pair< std::size_t, T> >
		{
			typedef typename std::pair<std::size_t, T> value_type;
			object operator()(std::initializer_list<value_type> il)
			{
				vector_t vec;
				std::for_each(il.begin(), il.end(), [&vec](value_type const& v){
					vec.push_back(set_factory(v.first, v.second));
				});
				return factory(vec);
			}
		};

		/**
		 * vector factory specialized for parameter values.
		 */
		template < typename T >
		struct vector_factory_helper<std::pair< std::string, T> >
		{
			typedef typename std::pair<std::string, T> value_type;
			object operator()(std::initializer_list<value_type> il)
			{
				vector_t vec;
				std::for_each(il.begin(), il.end(), [&vec](value_type const& v){
					vec.push_back(set_factory(v.first, v.second));
				});
				return factory(vec);
			}
		};

		/**
		 *	Treat C strings as std::string.
		 *
		 *	example:
		 *	@code
		 auto obj = cyy::vector_factory({ "A", "B", "C" });
		 *	@endcode
		 */
		template <>
		struct vector_factory_helper<const char*>
		{
			object operator()(std::initializer_list<const char*> il)
			{
				vector_t vec;
				std::for_each(il.begin(), il.end(), [&vec](std::string v){
					vec.push_back(factory(v));
				});
				return factory(vec);
			}
		};

		/**
		 * Specialized for objects.
		 */
		template <>
		struct vector_factory_helper<object>
		{
			object operator()(std::initializer_list<object> il)
			{
				vector_t vec;
				std::for_each(il.begin(), il.end(), [&vec](object const& obj){
					vec.push_back(obj);
				});
				return factory(vec);
			}
		};

	}

	template < typename T >
	object vector_factory(std::initializer_list<T> il)
	{
		return workbench::vector_factory_helper<T>()(il);
	}

	/**
	 * Transform a C++ vector into a cyy vector.
	 * Precondition is existing factory method for data type T.
	 */
	template < typename T >
	object vector_factory(std::vector< T > const& data)
	{
		vector_t vec;
		vec.reserve(data.size());
		for (auto const& v : data)
		{
			vec.push_back(factory(v));
		}
		return factory(vec);
	}

	/**
	*	Allows aggregate initialization of sets.
	*/
	template < typename T >
	object set_factory(std::initializer_list<T> il)
	{
		set_t s;
		std::for_each(il.begin(), il.end(), [&s](T const& v){
			s.insert(factory(v));
		});
		return factory(s);
	}

	template <>
	object set_factory<const char*>(std::initializer_list<const char*> il);
	
	template <>
	object set_factory<object>(std::initializer_list<object> obj_list);

	namespace workbench
	{
		//
		//	workbench namespace
		//

		template < typename T >
		struct param_map_factory_helper
		{};

		template < typename T >
		struct param_map_factory_helper<std::pair< std::size_t, T> >
		{
			typedef typename std::pair< std::size_t, T> value_type;

			object operator()(std::initializer_list<value_type> il)
			{
				attr_map_t amap;
				std::size_t pos = std::distance(il.begin(), il.end());
				std::for_each(il.begin(), il.end(), [&amap, &pos](value_type const& attr){
					pos--;
					amap.emplace(attr_t(attr.first, transform(pos, attr.second)));
				});
				return factory(amap);
			}
		};

		template < typename T >
		struct param_map_factory_helper<std::pair< std::string, T> >
		{
			typedef typename std::pair< std::string, T> value_type;

			object operator()(std::initializer_list<value_type> il)
			{
				param_map_t pmap;
				std::size_t pos = std::distance(il.begin(), il.end());
				std::for_each(il.begin(), il.end(), [&pmap, &pos](value_type const& param){
					pos--;
					pmap.emplace(param_t(param.first, transform(pos, param.second)));
				});
				return factory(pmap);
			}
		};

	}

	/**
	 * Create a parameter map.
	 *
	 * Example:
	 * @code
	 auto map = cyy::param_map_factory({ cyy::make_parameter("1", 1.1), cyy::make_parameter("2", 2.2) });
	 * @endcode
	 *
	 * Note that all elements have to be of the same type. This is a restriction from the C++ initializer list not from
	 * the parameter map.
	 */
	template < typename T >
	object param_map_factory(std::initializer_list<T> il)
	{
		return workbench::param_map_factory_helper<T>()(il);
	}

	/**
	 * create an empty parameter map
	 */
	CYY_LIBRARY_API object param_map_factory();
	

	namespace workbench
	{
		//
		//	workbench namespace
		//

		template <std::size_t idx1, std::size_t idx2, typename T>
		void attr_map_builder(attr_map_t& amap, T&& v)
		{
			amap.emplace(attr_t(idx1, transform(idx2, std::forward<T>(v))));
		}

		template <std::size_t idx1, std::size_t idx2, typename T, typename ...Args >
		void attr_map_builder(attr_map_t& amap, T&& v, Args&&... args)
		{
			//std::cout << idx1 << '/' << idx2 << '/' << count << std::endl;
			amap.emplace(attr_t(idx1, transform(idx2, std::forward<T>(v))));
			attr_map_builder<idx1 + 1, idx2 - 1>(amap, std::forward<Args>(args)...);
		}
	}
	

	/**
	 * Create an attribute map of all specified parameters. The attribute 
	 * index is calculated automatically from 0 to the size of the template 
	 * parameter pack. 
	 * 
	 * example:
	 * @code
	 * auto map1 = attr_map_factory(12, "hello", boost::uuids::random_generator()());
	 * @endcode
	 */
	template < typename ...Args >
	attr_map_t attr_map_factory(Args&&... args)
	{
		typedef std::integral_constant<std::size_t, sizeof...(Args)>	argc;
		attr_map_t amap;
		workbench::attr_map_builder<0, argc::value - 1>(amap, std::forward<Args>(args)...);
		return amap;
	}
	
	/**
	 * create an empty attribute map
	 */
	CYY_LIBRARY_API object attr_map_factory();
	
	
}	//	cyy

#endif // CYY_INTRINSICS_GENERIC_FACTORY_HPP

