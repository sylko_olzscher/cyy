/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#ifndef CYY_INTRINSICS_SET_FACTORY_H
#define CYY_INTRINSICS_SET_FACTORY_H

#include <cyy/cyy_export.h>
#include <cyy/object.h>
#include <cyy/intrinsics/sets.hpp>
#include <cyy/intrinsics/cyystddef.h>
#include <cyy/detail/make_value.hpp>
#include <type_traits>
#include <boost/config.hpp>

namespace cyy 
{
	
	CYY_LIBRARY_API object factory(attr_map_t const&);
	CYY_LIBRARY_API object factory(param_map_t const&);
	
	CYY_LIBRARY_API object factory(attr_t const&);
	CYY_LIBRARY_API object factory(attr_t&&);
	CYY_LIBRARY_API object factory(std::size_t, object const&);
	CYY_LIBRARY_API object factory(std::size_t, object &&);
	CYY_LIBRARY_API object factory(index, object const&);
	
	CYY_LIBRARY_API object factory(param_t const&);
	CYY_LIBRARY_API object factory(param_t&&);
	CYY_LIBRARY_API object factory(std::string, object const&);
	CYY_LIBRARY_API object factory(std::string, object &&);

	CYY_LIBRARY_API object factory(tuple_t const&);
	CYY_LIBRARY_API object factory(vector_t const&);
	CYY_LIBRARY_API object factory(set_t const&);

	CYY_LIBRARY_API object factory(tuple_t&&);
	CYY_LIBRARY_API object factory(vector_t&&);
	CYY_LIBRARY_API object factory(set_t&&);

	/**
	 * @return empty tuple object
	 */
	CYY_LIBRARY_API object tuple_factory();
	
	/**
	 * @return empty vector object
	 */
	CYY_LIBRARY_API object vector_factory();
	
	/**
	 * @return empty set object
	 */
	CYY_LIBRARY_API object set_factory();

	/**
	 * Create an attribute
	 */
	template < typename T >
	attr_t attr_factory(std::size_t idx, T&& v)
	{
		return attr_t(idx, object(core::make_value(std::forward<T>(v))));
	}

	/**
	 * Create an attribute - specialized for C strings
	 */
	template < std::size_t N >
	attr_t attr_factory(std::size_t idx, char const(&v)[N])
	{
		static_assert(N > 0, "invalid string length");
		return attr_t(idx, object(core::make_value(std::string(v, N - 1))));
	}

	/**
	 * Create a parameter
	 */
	template < typename T >
	param_t param_factory(std::string name, T&& v)
	{
		return param_t(name, object(core::make_value(std::forward<T>(v))));
	}

	/**
	 * Create a parameter - specialized for C strings
	 */
	template < std::size_t N >
	param_t param_factory(std::string name, char const(&v)[N])
	{
		static_assert(N > 0, "invalid string length");
		return param_t(name, object(core::make_value(std::string(v, N - 1))));
	}

	/**
	 * Create an object of type attribute 
	 * 
	 * @tparam T this type will implicitely converted into an object
	 * @param v the values to construct the object from
	 */
	template < typename T >
	object set_factory(std::size_t idx, T&& v )
	{
		return factory(idx, object(core::make_value(std::forward<T>(v))));
	}

	/**
	 * Create an object of type attribute - specialized for C strings
	 *
	 * @tparam N size of the string
	 * @param v C string as array of char's
	 */
	template < std::size_t N >
	object set_factory( std::size_t idx, char const(&v)[ N ])
	{
		static_assert(N > 0, "invalid string length");
		return factory(idx, object(core::make_value(std::string(v, N - 1))));
	}
	object set_factory(std::size_t idx, object&& v);

	/**
	 * Create an object of type parameter 
	 * 
	 * @tparam T this type will implicitely converted into an object
	 * @param v the values to construct the object from
	 */
	template < typename T >
	object set_factory(std::string name, T&& v )
	{
		return factory(name, object(core::make_value(std::forward<T>(v))));
	}
	
	template < std::size_t N >
	object set_factory(std::string name, char const(&v)[N])
	{
		static_assert(N > 0, "invalid string length");
		return factory(name, object(core::make_value(std::string(v, N - 1))));
	}
	object set_factory(std::string name, object&& v);

	/**
	 * helper template to create an attribute value.
	 */
	template < typename T >
	BOOST_CONSTEXPR auto make_attribute(std::size_t&& idx, T&& v) -> std::pair<std::size_t, typename std::decay<T>::type> 
	{
		return std::make_pair(std::forward<std::size_t>(idx), std::forward<T>(v));
	}
	
	/**
	 * helper template to create an attribute value. This specialization converts
	 * C-string values to std::string
	 */
	template < std::size_t N >
	BOOST_CONSTEXPR auto make_attribute(std::size_t&& idx, char const(&v)[N]) -> std::pair<std::size_t, std::string> 
	{
		return std::make_pair(std::forward<std::size_t>(idx), std::string(v, N - 1));
	}

	/**
	 * helper template to create a parameter value 
	 */
	template < typename T >
	BOOST_CONSTEXPR auto make_parameter(std::string&& name, T&& v) -> std::pair<std::string, typename std::decay<T>::type>
	{
		return std::make_pair(std::forward<std::string>(name), std::forward<T>(v));
	}

	/**
	 * helper template to create a parameter value. This specialization converts
	 * C-string values to std::string
	 */
	template < std::size_t N >
	BOOST_CONSTEXPR auto make_parameter(std::string&& name, char const(&v)[N]) -> std::pair<std::string, std::string>
	{
		return std::make_pair(std::forward<std::string>(name), std::string(v, N - 1));
	}


}	//	cyy

#endif // CYY_INTRINSICS_SET_FACTORY_H

