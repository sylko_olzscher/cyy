/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */
/** @file type_traits.hpp
 *
 * Contains all available type traits.
 * There is a discussion about this topic on 
 * http://stackoverflow.com/questions/1708458/template-metaprogram-converting-type-to-unique-number
 * Using a list has some advantages but is hard to extend for external libraries.
 */
#ifndef CYY_INTRINSICS_TYPE_TRAITS_HPP
#define CYY_INTRINSICS_TYPE_TRAITS_HPP

#include <boost/config.hpp>
#include <cyy/intrinsics/type_codes.h>
#include <type_traits>
#include <cyy/intrinsics/null.h>
#include <cyy/intrinsics/eod.h>
#include <cyy/intrinsics/version.h>
#include <cyy/intrinsics/op.h>
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/intrinsics/cyystddef.h>
#include <cyy/intrinsics/color.hpp>
#include <cyy/intrinsics/mac.h>
#include <cyy/intrinsics/digest.h>
#include <cyy/intrinsics/bytes.h>
#include <cyy/chrono/chrono.h>
#include <string>
#include <chrono>
#include <complex>
#include <memory>
#if defined(CYY_ODBC_HEADER_AVAILABLE)
#if defined(_WIN32) || defined(__CYGWIN__)
#include <boost/asio.hpp>
#endif
#include <sql.h> 
#include <sqlext.h>
#include <odbcinst.h>
#endif

namespace cyy 
{
	
	/**
	 * Fallback for types without specialized type traits
	 */
	template < typename T >
	struct type_traits
	{
		typedef T type;
		typedef std::integral_constant<code_t, types::CYY_CUSTOM_TYPES>	code_;
		typedef null subtype;	// to specify
	};

	/**
	 * Fallback for undefined type codes
	 * Specializing a integer value offers a route to specializes
	 * for other values from other enums.
	 * 
	 * It would be nice to identify a datatype by two typecodes. So could
	 * @code
	 * reverse_type< types::CYY_FLOAT32, types::CYY_FLOAT32 >:: type
	 * @endcode
	 * result in a std::complex<float> datatype. But this would be the ambiguous.
	 * Additionally all used subtypes had to be valid owners of a valid type 
	 * code. This is too ambitious and impractical.
	 */
	template < code_t/*, code_t = types::CYY_NULL*/ >
	struct reverse_type
	{
		typedef undef type;
	};
	
	/**
	 * Fallback for ownership definition
	 * is false.
	 */
	template < typename T >
	struct is_owner : std::false_type
	{};
	
	
	// 	CYY_NULL: void, null 
	template <>
	struct type_traits< null >
	{
		typedef null type;
		typedef std::integral_constant<code_t, types::CYY_NULL>	code_;
		typedef null subtype;	// none
	};
	
	template <>
	struct type_traits< void >
	{
		typedef void type;
		typedef std::integral_constant<code_t, types::CYY_NULL>	code_;
		typedef null subtype;	// none
	};

	template <>
	struct reverse_type < types::CYY_NULL >
	{
		typedef null type;
		static_assert(type_traits<type>::code_::value == types::CYY_NULL, "assignment error");
	};

	// 	CYY_EOD - end of data
	template <>
	struct type_traits< eod >
	{
		typedef null type;
		typedef std::integral_constant<code_t, types::CYY_EOD>	code_;
		typedef null subtype;	// none
	};
	
	template <>
	struct reverse_type < types::CYY_EOD >
	{
		typedef eod type;
		static_assert(type_traits<type>::code_::value == types::CYY_EOD, "assignment error");
	};

	// 	CYY_BOOL: bool 
	template <>
	struct type_traits< bool >
	{
		typedef std::int8_t type;
		typedef std::integral_constant<code_t, types::CYY_BOOL>	code_;
		typedef null subtype;	// none
	};
	
	template <>
	struct reverse_type < types::CYY_BOOL >
	{
		typedef bool type;
		static_assert(type_traits<type>::code_::value == types::CYY_BOOL, "assignment error");
	};
	
	/**
	 * CYY_INT8: std::int8_t
	 */
	template <>
	struct type_traits< std::int8_t >
	{
		typedef std::int8_t type;
		typedef std::integral_constant<code_t, types::CYY_INT8>	code_;
		typedef null subtype;	// none
	};

	template <>
	struct reverse_type < types::CYY_INT8 >
	{
		typedef std::int8_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_INT8, "assignment error");
	};
	
	// 	CYY_UINT8: std::uint8_t
	template <>
	struct type_traits< std::uint8_t >
	{
		typedef std::uint8_t type;
		typedef std::integral_constant<code_t, types::CYY_UINT8>	code_;
		typedef null subtype;	// none
	};

	template <>
	struct reverse_type < types::CYY_UINT8 >
	{
		typedef std::uint8_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_UINT8, "assignment error");
	};
	
	// 	CYY_INT16: std::int16_t
	template <>
	struct type_traits< std::int16_t >
	{
		typedef std::int16_t type;
		typedef std::integral_constant<code_t, types::CYY_INT16>	code_;
		typedef null subtype;	// none
	};

	template <>
	struct reverse_type < types::CYY_INT16 >
	{
		typedef std::int16_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_INT16, "assignment error");
	};
	
	/**
	 * CYY_UINT16: std::uint16_t
	 */
	template <>
	struct type_traits< std::uint16_t >
	{
		typedef std::uint16_t type;
		typedef std::integral_constant<code_t, types::CYY_UINT16>	code_;
		typedef null subtype;	// none
	};

	/**
	 * CYY_UINT16: std::uint16_t - reverse
	 */
	template <>
	struct reverse_type < types::CYY_UINT16 >
	{
		typedef std::uint16_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_UINT16, "assignment error");
	};
	
	/**
	 * CYY_INT32: std::int32_t
	 */
	template <>
	struct type_traits< std::int32_t >
	{
		typedef std::int32_t type;
		typedef std::integral_constant<code_t, types::CYY_INT32>	code_;
		typedef null subtype;	// none
	};

	/**
	 * CYY_INT32: std::int32_t - reverse
	 */
	template <>
	struct reverse_type < types::CYY_INT32 >
	{
		typedef std::int32_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_INT32, "assignment error");
	};

	/**
	 * CYY_UINT32: std::uint32_t
	 */
	template <>
	struct type_traits< std::uint32_t >
	{
		typedef std::int32_t type;
		typedef std::integral_constant<code_t, types::CYY_UINT32>	code_;
		typedef null subtype;	// none
	};

	/**
	 * CYY_UINT32: std::uint32_t - reverse
	 */
	template <>
	struct reverse_type < types::CYY_UINT32 >
	{
		typedef std::uint32_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_UINT32, "assignment error");
	};

	/**
	 * CYY_INT64: std::int64_t
	 */
	template <>
	struct type_traits< std::int64_t >
	{
		typedef std::int64_t type;
		typedef std::integral_constant<code_t, types::CYY_INT64>	code_;
		typedef null subtype;	// none
	};

	/**
	 * CYY_INT64: std::int64_t - reverse
	 */
	template <>
	struct reverse_type < types::CYY_INT64 >
	{
		typedef std::int64_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_INT64, "assignment error");
	};

	/**
	 * CYY_UINT64: std::uint64_t
	 */
	template <>
	struct type_traits< std::uint64_t >
	{
		typedef std::uint64_t type;
		typedef std::integral_constant<code_t, types::CYY_UINT64>	code_;
		typedef null subtype;	// none
	};

	/**
	 * CYY_UINT64: std::uint64_t - reverse
	 */
	template <>
	struct reverse_type < types::CYY_UINT64 >
	{
		typedef std::uint64_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_UINT64, "assignment error");
	};

	/**
	 * 	CYY_FLOAT32: float
	 */
	template <>
	struct type_traits< float >
	{
		typedef float type;
		typedef std::integral_constant<code_t, types::CYY_FLOAT32>	code_;
		typedef null subtype;	// none
	};

	/**
	 * 	CYY_FLOAT32: float - reverse
	 */
	template <>
	struct reverse_type < types::CYY_FLOAT32 >
	{
		typedef float type;
		static_assert(type_traits<type>::code_::value == types::CYY_FLOAT32, "assignment error");
	};
	
	// 	CYY_FLOAT64: double
	template <>
	struct type_traits< double >
	{
		typedef double type;
		typedef std::integral_constant<code_t, types::CYY_FLOAT64>	code_;
		typedef null subtype;	// none
	};

	template <>
	struct reverse_type < types::CYY_FLOAT64 >
	{
		typedef double type;
		static_assert(type_traits<type>::code_::value == types::CYY_FLOAT64, "assignment error");
	};

	// 	CYY_FLOAT80: long double - usually the same as double
	template <>
	struct type_traits< long double >
	{
		typedef long double type;
		typedef std::integral_constant<code_t, types::CYY_FLOAT80>	code_;
		typedef null subtype;	// none
	};

	template <>
	struct reverse_type < types::CYY_FLOAT80 >
	{
		typedef long double type;
		static_assert(type_traits<type>::code_::value == types::CYY_FLOAT80, "assignment error");
	};

	// 	
	// 	standard library
	//
	
	// 	CYY_COMPLEX32: std::complex<float>
	template <>
	struct type_traits< std::complex<float> >
	{
		typedef std::complex<float> type;
		typedef std::integral_constant<code_t, types::CYY_COMPLEX32>	code_;
		typedef type::value_type subtype;	// float
	};

	template <>
	struct reverse_type < types::CYY_COMPLEX32 >
	{
		typedef std::complex<float> type;
		static_assert(type_traits<type>::code_::value == types::CYY_COMPLEX32, "assignment error");
	};
	
	// 	CYY_COMPLEX64: std::complex<double>
	template <>
	struct type_traits< std::complex<double> >
	{
		typedef std::complex<double> type;
		typedef std::integral_constant<code_t, types::CYY_COMPLEX64>	code_;
		typedef type::value_type subtype;	// double
	};

	template <>
	struct reverse_type < types::CYY_COMPLEX64 >
	{
		typedef std::complex<double> type;
		static_assert(type_traits<type>::code_::value == types::CYY_COMPLEX64, "assignment error");
	};
	
	// 	CYY_COMPLEX80: std::complex<long double>
	template <>
	struct type_traits< std::complex<long double> >
	{
		typedef std::complex<long double> type;
		typedef std::integral_constant<code_t, types::CYY_COMPLEX80>	code_;
		typedef type::value_type subtype;	// long double
	};

	template <>
	struct reverse_type < types::CYY_COMPLEX80 >
	{
		typedef std::complex<long double> type;
		static_assert(type_traits<type>::code_::value == types::CYY_COMPLEX80, "assignment error");
	};

	/**
	 * CYY_STRING - std::string
	 */
	template <>
	struct type_traits< std::string >
	{
		typedef std::string type;
		typedef std::integral_constant<code_t, types::CYY_STRING>	code_;
		typedef std::string::value_type subtype;	// char
	};

	/**
	 * CYY_STRING - std::string - reverse
	 */
	template <>
	struct reverse_type < types::CYY_STRING >
	{
		typedef std::string type;
		static_assert(type_traits<type>::code_::value == types::CYY_STRING, "assignment error");
	};

	//
	//
 	//	note: there is no support for std::wstring anymore
	
	
	// 	CYY_TIME_STAMP - std::chrono::system_clock::time_point
	template <>
	struct type_traits< std::chrono::system_clock::time_point >
	{
		typedef std::chrono::system_clock::time_point type;
		typedef std::integral_constant<code_t, types::CYY_TIME_STAMP>	code_;
		typedef type::duration subtype;	//!< Duration, a std::chrono::duration type used to measure the time since epoch
	};

	template <>
	struct reverse_type < types::CYY_TIME_STAMP >
	{
		typedef std::chrono::system_clock::time_point type;
		static_assert(type_traits<type>::code_::value == types::CYY_TIME_STAMP, "assignment error");
	};
	
	// 	CYY_NANOSECONDS: std::chrono::nanoseconds
	template <>
	struct type_traits< std::chrono::nanoseconds >
	{
		typedef std::chrono::nanoseconds type;
		typedef std::integral_constant<code_t, types::CYY_NANOSECONDS>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	template <>
	struct reverse_type < types::CYY_NANOSECONDS >
	{
		typedef std::chrono::nanoseconds type;
		static_assert(type_traits<type>::code_::value == types::CYY_NANOSECONDS, "assignment error");
	};
	
	// 	CYY_MICROSECONDS: std::chrono::microseconds
	template <>
	struct type_traits< std::chrono::microseconds >
	{
		typedef std::chrono::microseconds type;
		typedef std::integral_constant<code_t, types::CYY_MICROSECONDS>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	template <>
	struct reverse_type < types::CYY_MICROSECONDS >
	{
		typedef std::chrono::microseconds type;
		static_assert(type_traits<type>::code_::value == types::CYY_MICROSECONDS, "assignment error");
	};

	/**
	 * CYY_MILLISECONDS: std::chrono::milliseconds
	 */
	template <>
	struct type_traits< std::chrono::milliseconds >
	{
		typedef std::chrono::milliseconds type;
		typedef std::integral_constant<code_t, types::CYY_MILLISECONDS>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	/**
	 * CYY_MILLISECONDS: std::chrono::milliseconds - reverse
	 */
	template <>
	struct reverse_type < types::CYY_MILLISECONDS >
	{
		typedef std::chrono::milliseconds type;
		static_assert(type_traits<type>::code_::value == types::CYY_MILLISECONDS, "assignment error");
	};

	// 	CYY_SECONDS - std::chrono::seconds
	template <>
	struct type_traits< std::chrono::seconds >
	{
		typedef std::chrono::seconds type;
		typedef std::integral_constant<code_t, types::CYY_SECONDS>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	template <>
	struct reverse_type < types::CYY_SECONDS >
	{
		typedef std::chrono::seconds type;
		static_assert(type_traits<type>::code_::value == types::CYY_SECONDS, "assignment error");
	};

	/**
	 * @brief CYY_MINUTES - std::chrono::minutes
	 */
	template <>
	struct type_traits< std::chrono::minutes >
	{
		typedef std::chrono::minutes type;
		typedef std::integral_constant<code_t, types::CYY_MINUTES>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	template <>
	struct reverse_type < types::CYY_MINUTES >
	{
		typedef std::chrono::minutes type;
		static_assert(type_traits<type>::code_::value == types::CYY_MINUTES, "assignment error");
	};

	/**
	 * @brief CYY_HOURS  - std::chrono::hours
	 */
	template <>
	struct type_traits< std::chrono::hours >
	{
		typedef std::chrono::hours type;
		typedef std::integral_constant<code_t, types::CYY_HOURS>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	template <>
	struct reverse_type < types::CYY_HOURS >
	{
		typedef std::chrono::hours type;
		static_assert(type_traits<type>::code_::value == types::CYY_HOURS, "assignment error");
	};

	/** @brief CYY_DAYS
	 * 
	 * Encoding the data type cyy::chrono::days
	 */
	template <>
	struct type_traits< cyy::chrono::days >
	{
		typedef cyy::chrono::days type;
		typedef std::integral_constant<code_t, types::CYY_DAYS>	code_;
		typedef type::rep subtype;	//!< an arithmetic type representing the number of ticks
	};

	template <>
	struct reverse_type < types::CYY_DAYS >
	{
		typedef cyy::chrono::days type;
		static_assert(type_traits<type>::code_::value == types::CYY_DAYS, "assignment error");
	};
	
	
	// 	
 	//	data types defined in cyy library
	//
	
// 	CYY_VERSION,
	template <>
	struct type_traits< version >
	{
		typedef version type;
		typedef std::integral_constant<code_t, types::CYY_VERSION>	code_;
		typedef type::major_type subtype;	// this type holds the complete version info
	};

	template <>
	struct reverse_type < types::CYY_VERSION >
	{
		typedef version type;
		static_assert(type_traits<type>::code_::value == types::CYY_VERSION, "assignment error");
	};

	// 	CYY_REVISION,
	template <>
	struct type_traits< revision >
	{
		typedef revision type;
		typedef std::integral_constant<code_t, types::CYY_REVISION>	code_;
		typedef type::major_type subtype;	// this type holds the complete version info
	};

	template <>
	struct reverse_type < types::CYY_REVISION >
	{
		typedef revision type;
		static_assert(type_traits<type>::code_::value == types::CYY_REVISION, "assignment error");
	};
	
	// 	CYY_OP
	template <>
	struct type_traits< op >
	{
		typedef op type;
		typedef std::integral_constant<code_t, types::CYY_OP>	code_;
		typedef type::value_type subtype;	// contains op code enum
	};

	template <>
	struct reverse_type < types::CYY_OP >
	{
		typedef op type;
		static_assert(type_traits<type>::code_::value == types::CYY_OP, "assignment error");
	};

	// 	CYY_BUFFER
	template <>
	struct type_traits< buffer_t >
	{
		typedef buffer_t type;
		typedef std::integral_constant<code_t, types::CYY_BUFFER>	code_;
		typedef type::value_type subtype;	// char
	};

	template <>
	struct reverse_type < types::CYY_BUFFER >
	{
		typedef buffer_t type;
		static_assert(type_traits<type>::code_::value == types::CYY_BUFFER, "assignment error");
	};
	
	// 	CYY_INDEX - std::size_t
	template <>
	struct type_traits< index >
	{
		typedef index type;
		typedef std::integral_constant<code_t, types::CYY_INDEX>	code_;
		typedef type::value_type subtype;	// std::size_t
	};

	template <>
	struct reverse_type < types::CYY_INDEX >
	{
		typedef index type;
		static_assert(type_traits<type>::code_::value == types::CYY_INDEX, "assignment error");
	};

	// 	CYY_DIFF - std::ptrdiff_t
	template <>
	struct type_traits< diff >
	{
		typedef diff type;
		typedef std::integral_constant<code_t, types::CYY_DIFF>	code_;
		typedef type::value_type subtype;	// std::ptrdiff_t
	};

	template <>
	struct reverse_type < types::CYY_DIFF >
	{
		typedef diff type;
		static_assert(type_traits<type>::code_::value == types::CYY_DIFF, "assignment error");
	};
	
	// 	CYY_BYTES - std::uint64_t
	template <>
	struct type_traits< bytes >
	{
		typedef bytes type;
		typedef std::integral_constant<code_t, types::CYY_BYTES>	code_;
		typedef type::value_type subtype;	// std::uint64_t
	};

	template <>
	struct reverse_type < types::CYY_BYTES >
	{
		typedef bytes type;
		static_assert(type_traits<type>::code_::value == types::CYY_BYTES, "assignment error");
	};

	/**
	 * CYY_RGB_COLOR8 - color_8
	 */
	template <>
	struct type_traits< color_8 >
	{
		typedef color_8 type;
		typedef std::integral_constant<code_t, types::CYY_RGB_COLOR8>	code_;
		typedef type::channel_type subtype;	// std::uint8_t
	};

	/**
	 * CYY_RGB_COLOR8 - color_8 - reverse
	 */
	template <>
	struct reverse_type < types::CYY_RGB_COLOR8 >
	{
		typedef color_8 type;
		static_assert(type_traits<type>::code_::value == types::CYY_RGB_COLOR8, "assignment error");
	};

	/**
	 * CYY_RGB_COLOR16 - color_16
	 */
	template <>
	struct type_traits< color_16 >
	{
		typedef color_16 type;
		typedef std::integral_constant<code_t, types::CYY_RGB_COLOR16>	code_;
		typedef type::channel_type subtype;	// std::uint16_t
	};

	/**
	 * CYY_RGB_COLOR16 - color_16 - reverse
	 */
	template <>
	struct reverse_type < types::CYY_RGB_COLOR16 >
	{
		typedef color_16 type;
		static_assert(type_traits<type>::code_::value == types::CYY_RGB_COLOR16, "assignment error");
	};

	// 	CYY_MAC48 - mac48
	template <>
	struct type_traits< mac48 >
	{
		typedef mac48 type;
		typedef std::integral_constant<code_t, types::CYY_MAC48>	code_;
		typedef type::address_type	subtype;	//	std::array< std::uint8_t, 6 >
	};

	template <>
	struct reverse_type < types::CYY_MAC48 >
	{
		typedef mac48 type;
		static_assert(type_traits<type>::code_::value == types::CYY_MAC48, "assignment error");
	};

	// 	CYY_MAC64 - mac64
	template <>
	struct type_traits< mac64 >
	{
		typedef mac64 type;
		typedef std::integral_constant<code_t, types::CYY_MAC64>	code_;
		typedef type::address_type	subtype;	//	std::array< std::uint8_t, 8 >
	};

	template <>
	struct reverse_type < types::CYY_MAC64 >
	{
		typedef mac64 type;
		static_assert(type_traits<type>::code_::value == types::CYY_MAC64, "assignment error");
	};

	// 	CYY_MD5 - digest_md5
	template <>
	struct type_traits< crypto::digest_md5 >
	{
		typedef crypto::digest_md5 type;
		typedef std::integral_constant<code_t, types::CYY_MD5>	code_;
		typedef type::value_type	subtype;	//	std::array<std::uint8_t, 16>
	};

	template <>
	struct reverse_type < types::CYY_MD5 >
	{
		typedef crypto::digest_md5 type;
		static_assert(type_traits<type>::code_::value == types::CYY_MD5, "assignment error");
	};

	// 	CYY_SHA1 - digest_sha1
	template <>
	struct type_traits< crypto::digest_sha1 >
	{
		typedef crypto::digest_sha1 type;
		typedef std::integral_constant<code_t, types::CYY_SHA1>	code_;
		typedef type::value_type	subtype;	//	std::array<std::uint8_t, 20>
	};

	template <>
	struct reverse_type < types::CYY_SHA1 >
	{
		typedef crypto::digest_sha1 type;
		static_assert(type_traits<type>::code_::value == types::CYY_SHA1, "assignment error");
	};

	/**
	 * CYY_SHA512 - digest_sha512
	 */
	template <>
	struct type_traits< crypto::digest_sha512 >
	{
		typedef crypto::digest_sha512 type;
		typedef std::integral_constant<code_t, types::CYY_SHA512>	code_;
		typedef type::value_type	subtype;	//	std::array<std::uint8_t, 64>
	};

	/**
	 * CYY_SHA512 - digest_sha512 (reverse)
	 */
	template <>
	struct reverse_type < types::CYY_SHA512 >
	{
		typedef crypto::digest_sha512 type;
		static_assert(type_traits<type>::code_::value == types::CYY_SHA512, "assignment error");
	};

	//
	//	ODBC/SQL support
	//
#if defined(CYY_ODBC_HEADER_AVAILABLE)

	/**
	* CYY_SQL_TIME - SQL_TIMESTAMP_STRUCT
	*/
	template <>
	struct type_traits< SQL_TIMESTAMP_STRUCT >
	{
		typedef SQL_TIMESTAMP_STRUCT type;
		typedef std::integral_constant<code_t, types::CYY_SQL_TIME>	code_;
		typedef null subtype;	// none
	};

	/**
	* CYY_SQL_TIME - SQL_TIMESTAMP_STRUCT (reverse)
	*/
	template <>
	struct reverse_type < types::CYY_SQL_TIME >
	{
		typedef SQL_TIMESTAMP_STRUCT type;
		static_assert(type_traits<type>::code_::value == types::CYY_SQL_TIME, "assignment error");
	};

#endif

	//
	//	complex data types
	//
	
	/**
	 * Specialized for arrays
	 */
	template < typename T, std::size_t N >
	struct type_traits< T[N] >
	{
		typedef T type[N];
		typedef std::integral_constant<code_t, types::CYY_ARRAY>	code_;
		typedef typename std::remove_const< T >::type subtype;
		typedef std::integral_constant<std::size_t, N>	dim_;	//!< size of an array type along a specified dimension 
	};
	
	//	could not provide a reverse_type<> template

	/**
	 * Detect shared types
	 */
	template < typename T >
	struct is_shared_type: std::false_type
	{};
	
	/**
	 * @return true for all shared pointers
	 */
	template < typename T >
	struct is_shared_type<std::shared_ptr<T> >: std::true_type
	{};

	
}	//	cyy

#endif // CYY_INTRINSICS_TYPE_TRAITS_HPP
