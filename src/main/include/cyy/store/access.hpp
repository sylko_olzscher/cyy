/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_STORE_ACCESS_HPP
#define CYY_STORE_ACCESS_HPP

#include <cyy/store/table.h>
#include <cyy/util/meta.hpp>

namespace cyy 
{
	namespace store	
	{
		/**
		 * Encodes the request for a read lock
		 */
		struct table_read
		{
			table_read(std::string const& name)
			: table_name_(name)
			{}
			const std::string table_name_;
		};
		
		/**
		 * Encodes the request for a write lock
		 */
		struct table_write
		{
			table_write(std::string const& name)
			: table_name_(name)
			{}
			const std::string table_name_;			
		};

		/**
		 * Encodes the request for an upgradable lock
		 */
		struct table_upgrade
		{
			table_upgrade(std::string const& name)
			: table_name_(name)
			{}
			const std::string table_name_;			
		};

		typedef enum 
		{
			ACCESS_UNDEF,
			ACCESS_READ,
			ACCESS_WRITE,
			ACCESS_UPGRADE,
		} acces_type;
		
		/**
		 * alias to define access level 
		 */
		template< acces_type e >
		using access_level = typename std::integral_constant<acces_type, e>::type;
		
		
		/**
		 * This template and the following specializations decodes
		 * the access level depended from the choosen name 
		 * specifier.
		 */
		template < typename T >
		struct access_calculator
		{
			typedef access_level< ACCESS_UNDEF > type;
		};
		
		template <>
		struct access_calculator<table_read>
		{
			typedef access_level< ACCESS_READ > type;
		};

		template <>
		struct access_calculator<table_write>
		{
			typedef access_level< ACCESS_WRITE > type;
		};

		template <>
		struct access_calculator<table_upgrade>
		{
			typedef access_level< ACCESS_UPGRADE > type;
		};

		
		namespace //	anonymous
		{
			template < typename T >
			struct get_lock_type_impl
			{};

			template <>
			struct get_lock_type_impl< access_level< ACCESS_READ > >
			{
				using type = shared_lock_type;	
			};

			template <>
			struct get_lock_type_impl< access_level< ACCESS_WRITE > >
			{
				using type = exclusive_lock_type;	
			};

			template <>
			struct get_lock_type_impl< access_level< ACCESS_UPGRADE > >
			{
				using type = upgrade_lock_type;	
			};
		}
		
		/**
		 * alias for table lock types
		 */
		template< typename T >
		using table_lock_type = typename get_lock_type_impl<typename access_calculator< T >::type>::type;
		

		namespace //	anonymous
		{
			template < typename AL, typename T >
			struct get_param_type_impl
			{
//				using type = void;
			};
			
			template < typename T >
			struct get_param_type_impl< access_level< ACCESS_READ >::type, T > 
			{
				//	It's essential to add constness first
				using ref_type = typename std::add_lvalue_reference< typename std::add_const< T >::type >::type;
				using ptr_type = typename std::add_pointer< typename std::add_const< T >::type >::type;
			};

			template < typename T >
			struct get_param_type_impl< access_level< ACCESS_WRITE >::type, T > 
			{
				using ref_type = typename std::add_lvalue_reference< T >::type;
				using ptr_type = typename std::add_pointer< T >::type;
			};

			template < typename T >
			struct get_param_type_impl< access_level< ACCESS_UPGRADE >::type, T > 
			{
				using ref_type = typename std::add_lvalue_reference< T >::type;
				using ptr_type = typename std::add_pointer< T >::type;
			};
		}

  		template < typename T >
  		struct get_param_type
  		{
			typedef typename access_calculator< T >::type access_type;
  			using ref_type = typename get_param_type_impl<access_type, table>::ref_type;
  			using ptr_type = typename get_param_type_impl<access_type, table>::ptr_type;
 		};
		
		/**
		 * alias for table reference types
		 */
		template< typename T >
		using table_ref_type = typename get_param_type_impl<typename access_calculator< T >::type, table>::ref_type;

		/**
		 * alias for table pointer types
		 */
		template< typename T >
		using table_ptr_type = typename get_param_type_impl<typename access_calculator< T >::type, table>::ptr_type;

		
  		template <template<class...> class R, typename ...Tbls>
  		struct transform_impl
  		{
 			using ref_type = mp_transform<table_ref_type, R<Tbls...>>;
 			using ptr_type = mp_transform<table_ptr_type, R<Tbls...>>;
			using lock_type = mp_transform<table_lock_type, R<Tbls...>>;
  		};		
		
		
		/**
		 * Locks the Lockable objects supplied as arguments 
		 * in an unspecified and indeterminate order in a way that 
		 * avoids deadlock.
		 * 
		 * We are faced with a problem that arises from the implementation
		 * of the lock methos in boost. Lock() requires at least two 
		 * parameters. It doesn't work with one lockable.
		 * The problem should not occur with the STL.
		 */
#if !defined(CYY_COMPATIBILITY_MODE) 
		struct sync_functor
		{
           
            //
            //  rvalues of boost::shared_lock<boost::shared_mutex> and
            //  boost::unique_lock<boost::shared_mutex>
            //
			template<typename ...Args>
			void operator() (Args&&... args)
			{
//  				std::cout 
//  				<< "sync_functor("
//  				<< sizeof...(Args)
//  				<< ")"
//  				<< std::endl;
				
				cyy::thread::lock(std::forward<Args>(args)...);
			}
// #endif
			
            //
            //  references of boost::shared_lock<boost::shared_mutex> and
            //  boost::unique_lock<boost::shared_mutex>
            //
			template<typename ...Args>
			void operator() (Args&... args)
			{
				cyy::thread::lock(args...);
			}
			
			template<typename Lockable>
			void operator() (Lockable& arg)
			{
//  				std::cout 
//  				<< "sync_functor(1)"
//  				<< std::endl;
				
				arg.lock();
			}
		};
        
#else
		
// 		CYY_COMPATIBILITY_MODE
		struct sync_functor
		{
            using result_type = int;    //  dummy
            
		
            //
            //  references of boost::shared_lock<boost::shared_mutex> and
            //  boost::unique_lock<boost::shared_mutex>
            //
			template<typename ...Args>
			int operator() (Args&... args)
			{
				cyy::thread::lock(args...);
                return 2;
			}
			
			template<typename Lockable>
			int operator() (Lockable& arg)
			{
//  				std::cout 
//  				<< "sync_functor(1)"
//  				<< std::endl;
				
				arg.lock();
                return 3;
			}
			
//             call of ‘(cyy::store::sync_functor) (boost::shared_lock<boost::shared_mutex>&, boost::shared_lock<boost::shared_mutex>&)’ is ambiguous
// 			int operator() (boost::shared_lock<boost::shared_mutex>& sm1, boost::shared_lock<boost::shared_mutex>& sm2)
//             {
//                 cyy::thread::lock(sm1, sm2);
//                 return 4;
//             }
//             
// //             call of ‘(cyy::store::sync_functor) (boost::shared_lock<boost::shared_mutex>&, boost::unique_lock<boost::shared_mutex>&)’ is ambiguous
// 			int operator() (boost::shared_lock<boost::shared_mutex>& sm1, boost::unique_lock<boost::shared_mutex>& sm2)
//             {
//                 cyy::thread::lock(sm1, sm2);
//                 return 5;
//             }
		};
#endif
        
		
	}	//	store
}	//	cyy 

#endif // CYY_STORE_ACCESS_HPP
