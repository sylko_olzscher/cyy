/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#ifndef CYY_VALUE_CAST_HPP
#define CYY_VALUE_CAST_HPP

#include <cyy/object_cast.hpp>
#include <cyy/intrinsics/cyystddef.h>
#include <boost/uuid/nil_generator.hpp>

namespace cyy 
{

	/**
	 * Try to cast an object to type T. If casting failed
	 * a default value is used.
	 * 
	 * @param obj object to cast
	 * @param def default value
	 * @tparam T cast object 
	 * @return a const reference of the value or the default value if 
	 * the type cast wasn't successful.
	 */
	template < typename T >
	T const& value_cast(object const& obj, T const& def = T()) BOOST_NOEXCEPT
	{
		const T * ptr = object_cast<T>(obj);
		return (ptr != nullptr)
		? *ptr
		: def
		;
	}
	
	/**
	 * Assuming datatype index(std::size_t)
	 */
	inline std::size_t index_cast(object const& obj, std::size_t def = 0) BOOST_NOEXCEPT
	{
		const index idx(def);
		return *value_cast(obj, idx);
	}
	
	/**
	 * Assuming datatype diff(std::ptrdiff_t);
	 */
	inline std::ptrdiff_t diff_cast(object const& obj, std::ptrdiff_t def = 0) BOOST_NOEXCEPT
	{
		const diff delta(def);
		return *value_cast(obj, delta);
	}
	
	/**
	 * @param obj object to cast
	 * @param def default string to use if cast was not sucessful
	 * @return a const reference of the value or the default value if 
	 * the type cast wasn't successful.
	 */
	inline std::string const& string_cast(object const& obj, std::string const& def = "") BOOST_NOEXCEPT
	{
		return value_cast(obj, def);
	}

	/**
	* @brief Assuming datatype boost::uuids::uuid;
	*/
	inline boost::uuids::uuid uuid_cast(object const& obj, boost::uuids::uuid def = boost::uuids::nil_uuid()) BOOST_NOEXCEPT
	{
		return value_cast(obj, def);
	}

	/**
	 * Extracts the value of a given object of type parameter.
	 * If casting failed a default value is used.
	 * 
	 * @param obj object to cast. It's required that obj is a parameter
	 * @param def default value
	 * @tparam T cast object 
	 */
	template < typename T >
	T const& param_cast(object const& obj, T const& def = T()) BOOST_NOEXCEPT
	{
		BOOST_ASSERT_MSG(primary_type_code_test<types::CYY_PARAMETER>(obj), "wrong type");
		return (primary_type_code_test<types::CYY_PARAMETER>(obj))
		? value_cast<T>(object_cast<param_t>(obj)->second, def)
		: def 
		;
	}
	
	/**
	 * Extracts the value of a given object of type attribute.
	 * If casting failed the default value is used instead.
	 * 
	 * @param obj object to cast. It's required that obj is an attribute
	 * @param def default value
	 * @tparam T cast object 
	 */
	template < typename T >
	T const& attr_cast(object const& obj, T const& def = T()) BOOST_NOEXCEPT
	{
		BOOST_ASSERT_MSG(primary_type_code_test<types::CYY_ATTRIBUTE>(obj), "wrong type");
		return (primary_type_code_test<types::CYY_ATTRIBUTE>(obj))
		? value_cast<T>(object_cast<attr_t>(obj)->second, def)
		: def 
		;
	}
	
	/**
	 * read a vector of the specified data type
	 * example
	 * @code
	 std::vector<std::string> vec = vector_cast<int>(obj);
	 * @endcode
	 */
	template < typename T >
	std::vector< T > vector_cast(object const& obj, T const& def = T()) BOOST_NOEXCEPT
	{
		std::vector< T > result;
		const vector_t* vec = object_cast<vector_t>(obj);
		if (vec != nullptr)
		{
			for (auto const& obj : *vec)
			{
				result.push_back(value_cast<T>(obj, def));
			}
		}
		return result;
	}

}	//	cyy

#endif // CYY_VALUE_CAST_HPP
