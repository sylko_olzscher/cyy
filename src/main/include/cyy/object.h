/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#ifndef CYY_OBJECT_H
#define CYY_OBJECT_H

#include <cyy/cyy_export.h>
#include <cyy/object_interface_fwd.h>
#include <cyy/object_type.h>
#include <cstdint>
#include <boost/assert.hpp>
#include <boost/config.hpp>

namespace cyy 
{

	/**@brief Generic data type 
	 * 
	 * Provides a runtime dynamically typed value for C++, similar to the way languages
	 * with runtime type systems work (e.g. Python). It can hold types from a predetermined
	 * set of types (ints, bools, arrays of other dynamics, etc), similar to something
	 * like boost::any, but the syntax is intended to be a little more like using the
	 * native type directly.
	 * 
	 * An CYY-object is a lightweight object which only contains a pointer to its internal
	 * shared value. Thus it can be copied around without worrying about
	 * performance. Further it is not necessary to create or pass pointers to CYY-
	 * objects.
	 * 
	 * <ul>
	 * <li>To get a pointer to the shared value use object_cast<T>() or value_cast<T>(). 
	 * (There are more casts available.)</li>
	 * <li>To do some math with object include cyy/math.h</li>
	 * <li>To create objects include cyy/intrinsics/factory.h</li>
	 * <li>To compare objects include cyy/intrinsics/compare.h</li>
	 * <li>To get the size include cyy/intrinsics/size.h</li>
	 * </ul>
	 */
	class CYY_LIBRARY_API object
	{
		friend CYY_LIBRARY_API void swap(object&, object&);
		friend CYY_LIBRARY_API bool is_shared(object const&);
		friend CYY_LIBRARY_API bool is_array(object const&);
		friend CYY_LIBRARY_API bool is_comparable(object const& obj);
		friend CYY_LIBRARY_API bool is_sortable(object const& obj);
		friend CYY_LIBRARY_API bool has_ownership(object const&);
		friend class CYY_LIBRARY_API object_tracker;
		
	public:
		/**
		 * To use an object in a STL container class
		 * a default constructor is required (std::vector is an exception).
		 * Otherwise the use of the empty factory() method
		 * is highly recommended. 
		 */
		object();
		object(const object& other);
		object(object&& other) BOOST_NOEXCEPT;
		object(core::shared_object);
		virtual ~object() BOOST_NOEXCEPT;
		
		/**
		 * @throws copy_error if other object claims ownership of the object
		 */
		object& operator=(const object& other);
		object& operator=(object&& other);

		/**
		 * @return type information generated by the implementation.
		 */
		const std::type_info & type() const;
		
		/**
		 * @return object type information generated by the 
		 * template based type traits mechanism provided by the 
		 * CYY library
		 */
		types::object_code code() const;
		

		/**
		* Objects are identically if they share the same address.
		* This is possible since different objects can share the same
		* value.
		*
		*	@return true if both object values share the same address.
		*/
		bool is_identical( object const& ) const;
		
		
		/**
		 * Offers a route for safe copying.
		 * Could throw.
		 * 
		 * @return a copy of object 
		 * @throws clone_error if object claims ownership of the object
		 */
		object clone() const;
		
		/**
		* Reset internal value to null.
		*/
		void clear();
		
		/** @brief implementing the concept of an empty/NULL value
		 * 
		 * Checks whether object contains a value or null.
		 * 
		 * @return true if object contains a value, false if object 
		 * does not contain a value (null).
		 */
		explicit operator bool() const;
		
		/**
		 * allow to access private member value_
		 */
		template < typename T >
		friend const T * object_cast(object const&) BOOST_NOEXCEPT;


	private:
		core::shared_object	value_;
	};

	//	swap
	CYY_LIBRARY_API void swap(object&, object&);

	
}	//	cyy

#endif // CYY_OBJECT_H
