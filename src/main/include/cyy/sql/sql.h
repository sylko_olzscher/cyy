/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */
/** @file sql.h
 * This library is pure fun. Goal is to use the intellisence mechanism
 * of the IDE to produce valid SQL syntax.
 * 
 * Example:
 * @code
	cyy::sql::table tbl("TCustomers");
	tbl.add_column("ident", cyy::types::CYY_BOOST_UUID, true);
	tbl.add_column("name", cyy::types::CYY_STRING, false, 64);
	tbl.add_column("age", cyy::types::CYY_TIME_STAMP);
	tbl.add_column("flag", cyy::types::CYY_BOOL);
	
	auto cmd = cyy::sql::factory(tbl, cyy::sql::custom_callback());
	cmd->insert(cyy::sql::SQLITE);
	
	//	INSERT INTO TCustomers (ident, name, age, flag) VALUES (?, ?, julianday(?), ?)
	const std::string s = cmd->to_str();
	std::cout << s << std::endl;
	
	//	this is amazing
	const std::chrono::system_clock::time_point tp = std::chrono::system_clock::now();
	cmd->update({4}, cyy::sql::SQLITE)->where(cyy::sql::column(3) < tp);
	//	UPDATE TCustomers SET flag = ? WHERE (age < julianday('2015-07-01 16:02:48')) 
	std::cout << cmd->to_str() << std::endl;

	//	this is amazing too
	cmd->update({3}, cyy::sql::SQLITE)->where(cyy::sql::column(4) == true);
	//	UPDATE TCustomers SET age = julianday(?) WHERE (flag = 1) 
	std::cout << cmd->to_str() << std::endl;
 * @endcode
 */
#ifndef CYY_SQL_H
#define CYY_SQL_H

#include <memory>
#include <sstream>
#include <cyy/cyy_export.h>
#include <cyy/sql/table.h>
#include <cyy/sql/dialects.h>
#include <cyy/sql/sql_callback.h>

namespace cyy 
{
	namespace sql 
	{
		class command;
		class sql_select;
		class sql_select_all;
		class sql_select_distinct;
		class sql_select_count;
		class sql_from;
		class sql_where;
		class sql_group;
		class sql_having;
		class sql_order;
 		class sql_update;
 		class sql_insert;
 		class sql_create;
		class sql_delete;	//	delete is a C++ keyword
// 		class alter;
 		class sql_drop;
		
		typedef std::shared_ptr< sql_select > select_ptr;
		typedef std::shared_ptr< sql_select_all > select_all_ptr;
		typedef std::shared_ptr< sql_select_distinct > select_distinct_ptr;
		typedef std::shared_ptr< sql_select_count > select_count_ptr;
		typedef std::shared_ptr< sql_from > from_ptr;
		typedef std::shared_ptr< sql_where > where_ptr;
		typedef std::shared_ptr< sql_group > group_ptr;
		typedef std::shared_ptr< sql_having > having_ptr;
		typedef std::shared_ptr< sql_order > order_ptr;
		typedef std::shared_ptr< sql_insert > insert_ptr;
		typedef std::shared_ptr< sql_create > create_ptr;
		typedef std::shared_ptr< sql_update > update_ptr;
		typedef std::shared_ptr< sql_delete > delete_ptr;
		typedef std::shared_ptr< sql_drop > drop_ptr;

		std::shared_ptr< command > factory(table const&, custom_callback);
		class CYY_SQL_LIBRARY_API command 
		: public std::enable_shared_from_this< command >
		{
		public:
			command(table const&, custom_callback);
			select_ptr select(dialect dia);
			select_ptr select(std::string const&, dialect dia);
			/**
			 * Accept an initializer list with column indices.
			 * Indices in the range from (1...n].
			 * 
			 * @code
			 * cmd->select_columns({1,2,3})->from();
			 * @endcode
			 */
			select_ptr select(std::initializer_list<std::size_t> list, dialect dia);
			select_ptr select(std::vector<std::size_t> const&, dialect dia);
			select_count_ptr count(dialect dia);

			update_ptr update(std::initializer_list<std::size_t> list, dialect dia);
			update_ptr update(std::vector<std::size_t> const& vec, dialect dia);

			/**
			 * Update all data columns. Is to complement by a where clause containing
			 * the PK.
			 */
			update_ptr update(dialect dia);

			insert_ptr insert(dialect);
			create_ptr create(dialect);
			//	delete is a C++ keyword
			delete_ptr remove(dialect);
			drop_ptr drop(dialect);
			void alter();


			std::string to_str() const;
			
		private:
			/**
			 * reset internal stream buffer
			 */
			void clear();
			void clear(std::string const&);
			
		public:
			friend std::shared_ptr< command > factory(table const&, custom_callback);
			
		private:
			const table table_;
			custom_callback cb_;
			std::stringstream stream_;
		};
		
		class CYY_SQL_LIBRARY_API sql_select 
		: public std::enable_shared_from_this< sql_select >
		{
		public:
			sql_select(std::ostream&, const table*, dialect dia);
			sql_select(std::ostream&, const table*, dialect dia, std::string const&);
			select_all_ptr all();
			select_all_ptr all(std::string const&);
			select_distinct_ptr distinct();
			select_distinct_ptr distinct(std::string const&);
			select_count_ptr count();
			from_ptr from(std::string const&);
			from_ptr from();

			template < typename LIST >
			from_ptr operator[](LIST const& list)
			{
				list.serialize(stream_, table_, dia_);
				stream_ << ' ';
				return from();
			}

			
		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;		
		};
		
		class CYY_SQL_LIBRARY_API sql_select_all
		: public std::enable_shared_from_this< sql_select_all >
		{
		public:
			sql_select_all(std::ostream&, const table*, dialect dia);
			sql_select_all(std::ostream&, const table*, dialect dia, std::string const&);
			
			from_ptr from(std::string const&);
			from_ptr from();

		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;
		};

		class CYY_SQL_LIBRARY_API sql_select_distinct
		: public std::enable_shared_from_this< sql_select_distinct >
		{
		public:
			sql_select_distinct(std::ostream&, const table*, dialect dia);
			sql_select_distinct(std::ostream&, const table*, dialect dia, std::string const&);
			
			from_ptr from(std::string const&);
			
		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;
		};

		class CYY_SQL_LIBRARY_API sql_select_count
		: public std::enable_shared_from_this< sql_select_count >
		{
		public:
			sql_select_count(std::ostream&, const table*, dialect dia);
			
			template < typename EXPR >
			where_ptr where(EXPR const& expr)
			{
				stream_
					<< " WHERE "
					;
				expr.serialize(stream_, table_, dia_);
				return std::make_shared< sql_where >(stream_, table_);
			}

			/**
			 * Creates a where clause containing all elements of the primary key
			 * chained with and AND condition.
			 */
			where_ptr where_pk();

		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;
		};

		/**
		* The DELETE statement is used to delete rows in a table.
		*/
		class CYY_SQL_LIBRARY_API sql_delete
			: public std::enable_shared_from_this< sql_delete >
		{
		public:
			sql_delete(std::ostream&, const table*, dialect dia, custom_callback cb);

			template < typename EXPR >
			where_ptr where(EXPR const& expr)
			{
				stream_
					<< "WHERE "
					;
				expr.serialize(stream_, table_, dia_);
				return std::make_shared< sql_where >(stream_, table_);
			}

			/**
			 * Creates a where clause containing all elements of the primary key
			 * chained with and AND condition.
			 */
			where_ptr where_pk();

		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;
		};

		
		class CYY_SQL_LIBRARY_API sql_from
		: public std::enable_shared_from_this< sql_from >
		{
		public:
			sql_from(std::ostream&, const table*, dialect dia, std::string const&);
			
			template < typename EXPR >
			where_ptr where(EXPR const& expr)
			{
				stream_ 
				<< "WHERE "
				;
				expr.serialize(stream_, table_, dia_);
				return std::make_shared< sql_where >(stream_, table_);
			}

			where_ptr where(const char*);

			group_ptr group_by(std::string const&);

		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;
		};
		
		class CYY_SQL_LIBRARY_API sql_where
		: public std::enable_shared_from_this< sql_where >
		{
		public:
			sql_where(std::ostream&, const table*);
			
			group_ptr group_by(std::string const&);
			//	ASC, DESC
			order_ptr order_by(std::string const&, bool asc = true);
			order_ptr order_by(std::size_t index, bool asc = true);
			
		private:
			std::ostream& stream_;
			const table* table_;
			
		};

		class CYY_SQL_LIBRARY_API sql_group
		: public std::enable_shared_from_this< sql_group >
		{
		public:
			sql_group(std::ostream&, const table*, std::string const&);

			having_ptr having(std::string const&);

			/**
			 * skip having clause
			 */
			order_ptr order_by(std::string const&, bool asc = true);

		private:
			std::ostream& stream_;
			const table* table_;
		};
		
		class CYY_SQL_LIBRARY_API sql_having
		: public std::enable_shared_from_this< sql_having >
		{
		public:
			sql_having(std::ostream&, const table*, std::string const&);
			
			order_ptr order_by(std::string const&, bool asc = true);

		private:
			std::ostream& stream_;
			const table* table_;			
		};

		class CYY_SQL_LIBRARY_API sql_order
		: public std::enable_shared_from_this< sql_order >
		{
		public:
			sql_order(std::ostream&, const table*, std::string const&, bool asc = true);
			
		private:
			std::ostream& stream_;
			const table* table_;			
		};

		class CYY_SQL_LIBRARY_API sql_insert
		: public std::enable_shared_from_this< sql_insert >
		{
		public:
			sql_insert(std::ostream&, const table*, dialect dia, std::string const&);
			
		private:
			void write_columns(dialect dia);
			void write_values(dialect dia);
			
		private:
			std::ostream& stream_;
			const table* table_;			
		};
		
		class CYY_SQL_LIBRARY_API sql_update
		: public std::enable_shared_from_this< sql_update >
		{
		public:
			sql_update(std::ostream&, const table*, dialect dia);
			
			template < typename EXPR >
			where_ptr where(EXPR const& expr)
			{
				stream_ 
				<< "WHERE "
				;
				expr.serialize(stream_, table_, dia_);
				return std::make_shared< sql_where >(stream_, table_);
			}

			/**
			 * Creates a where clause containing all elements of the primary key
			 * chained with and AND condition.
			 */
			where_ptr where_pk();
			
		private:
			std::ostream& stream_;
			const table* table_;
			const dialect dia_;
		
		};
		
		/**
		 * auto increment is not supported on purpose.
		 */
		class CYY_SQL_LIBRARY_API sql_create
		: public std::enable_shared_from_this< sql_create >
		{
		public:
			sql_create(std::ostream&, const table*, dialect dia, custom_callback cb);
			
		private:
			void write_columns(dialect dia, custom_callback);
			void write_primary_key();
			void write_unique_constraints(bool);
			void write_unique_constraint_members(unique_constraint const&);
			
		private:
			std::ostream& stream_;
			const table* table_;
		};

		/**
		* remove table from database
		*/
		class CYY_SQL_LIBRARY_API sql_drop
			: public std::enable_shared_from_this< sql_drop >
		{
		public:
			sql_drop(std::ostream&, const table*, dialect dia, custom_callback cb);

		private:
			std::ostream& stream_;
			const table* table_;
		};

	}
}

#endif	//	CYY_SQL_H
 
