#ifndef CYY_POSIX_OS_H
#define CYY_POSIX_OS_H
//	This file is generated automatically. Don't change manually.
//	Generated at Wed Jun  7 17:30:59 2017

namespace cyy
{
	constexpr char pre_system[] = "Linux";
	constexpr char pre_node[] = "rollercoaster";
	constexpr char pre_release[] = "4.11.3-1-default";
	constexpr char pre_version[] = "#1 SMP PREEMPT Thu May 25 17:55:04 UTC 2017 (7262353)";
	constexpr char pre_machine[] = "x86_64";
	constexpr char pre_locale[] = "C.utf8";
}
#endif // CYY_POSIX_OS_H
