/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_SCHEDULER_TASK_FWD_H
#define CYY_SCHEDULER_TASK_FWD_H

#include <cyy/scheduler/thread/sync.hpp>
#include <memory>
#include <map>
#include <cstddef>
#include <future>
#include <list>
#include <tuple>
#include <boost/system/error_code.hpp>

namespace cyy 
{
	//	some forward definitions
	class ruler;
	class broker;
	
	namespace scheduler 
	{
		class factory;
	}
	
	using shared_task = std::shared_ptr< broker >;
	using task_map = std::map< std::size_t, shared_task >;
	using result_type = boost::system::error_code;
	using async_result_type = thread::future<result_type>;
	
	/**
	 * declare an empty task message
	 */
	using empty_msg_t = std::tuple<>;

	/**
	* 0 is an invalid task id
	*/
	constexpr std::size_t NO_TASK = 0;

}

#endif	//	CYY_SCHEDULER_TASK_FWD_H



