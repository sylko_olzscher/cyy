/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */
#ifndef CYY_SCHEDULER_LOG_H
#define CYY_SCHEDULER_LOG_H

#include <cyy/scheduler/logging/severity.h>
#include <cyy/scheduler/logging/logger.hpp>
#include <cyy/scheduler/logging/entry.hpp>

/**
 * full logging record
 */
typedef cyy::entry<true, true, true >	cyy_log_entry;
typedef cyy::logger<cyy_log_entry> 		logger_type;

/**
 * logging record without thread ID
 */
typedef cyy::entry<true, true, false >	cyy_log_entry_without_threadid;
typedef cyy::logger<cyy_log_entry_without_threadid> logger_type_without_threadid;

//
//	Some shortcuts
//	Note that the (only) variable __msg could hide the visibility
//	of a an outside variable with the same name.
//
#define CYY_LOG(l,s,m)	\
	{	\
		auto __msg = cyy_log_entry::factory(s);	\
		(*__msg) << m;	\
		l.push(__msg);	\
	}

#define CYY_LOG_TRACE(l,m)	\
	CYY_LOG(l,cyy::logging::severity::LEVEL_TRACE,m)

#define CYY_LOG_DEBUG(l,m)	\
	CYY_LOG(l,cyy::logging::severity::LEVEL_DEBUG,m)

#define CYY_LOG_INFO(l,m)	\
	CYY_LOG(l,cyy::logging::severity::LEVEL_INFO,m)

#define CYY_LOG_WARNING(l,m)	\
	CYY_LOG(l,cyy::logging::severity::LEVEL_WARNING,m)
	
#define CYY_LOG_ERROR(l,m)	\
	CYY_LOG(l,cyy::logging::severity::LEVEL_ERROR,m)

#define CYY_LOG_FATAL(l,m)	\
	CYY_LOG(l,cyy::logging::severity::LEVEL_FATAL,m)
	
#endif	//	CYY_SCHEDULER_LOG_H