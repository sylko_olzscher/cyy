# top level files
set (unit_test)

set (test_cpp

	unitTest/src/main.cpp
	unitTest/src/test_core.cpp
	unitTest/src/test_io.cpp
	unitTest/src/test_vm.cpp
	unitTest/src/test_scheduler.cpp
	unitTest/src/test_dload.cpp
	unitTest/src/test_store.cpp
	unitTest/src/test_sql.cpp
	unitTest/src/test_crypto.cpp
	unitTest/src/test_xml.cpp
)

set (test_h

	unitTest/src/test_core.h
	unitTest/src/test_io.h
	unitTest/src/test_vm.h
	unitTest/src/test_scheduler.h
	unitTest/src/test_dload.h
	unitTest/src/test_store.h
	unitTest/src/test_sql.h
	unitTest/src/test_crypto.h
	unitTest/src/test_xml.h
)


# define the unit test executable
set (unit_test
  ${test_cpp}
  ${test_h}
)