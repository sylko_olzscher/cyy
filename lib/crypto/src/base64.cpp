/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */
#include <cyy/crypto/base64.hpp>

namespace cyy	
{
	namespace crypto	
	{
		namespace base64	
		{
			namespace 
			{	//	anonymous

				static const char decoding_data[] =
				{
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, 62, nop, nop, nop, 63,
					52, 53, 54, 55, 56, 57, 58, 59, 60, 61, nop, nop, nop, nop, nop, nop,
					nop, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14,
					15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, nop, nop, nop, nop, nop,
					nop, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40,
					41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, nop, nop, nop, nop, nop,

					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop,
					nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop, nop
				};

				static const char encoding_data[] =
				{ "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/" };
			}	//	anonymous

			char decoding_char(int idx)		
			{
				BOOST_ASSERT(idx < 0xFF);
				return decoding_data[idx];
			}

			char encoding_char(int idx)		
			{
				BOOST_ASSERT(idx < 64);
				return encoding_data[idx];
			}


		}	//	namespace base64

		cyy::buffer_t decode_base64(std::string const& s)		
		{
			cyy::buffer_t result;
			auto bi = std::back_inserter(result);
			base64::decode<>(s, bi);
			return result;
		}

		std::string encode_base64(std::string const& s)		
		{
			return base64::encode<>(s.begin(), s.end());
		}

		std::string encode_base64(crypto::digest_sha1::value_type const& s)
		{
			return base64::encode<>(s.begin(), s.end());
		}

		std::string encode_base64(crypto::digest_sha256::value_type const& s)
		{
			return base64::encode<>(s.begin(), s.end());
		}

		std::string encode_base64(crypto::digest_sha512::value_type const& s)
		{
			return base64::encode<>(s.begin(), s.end());
		}

	}	//	crypto
}	//	cyy
