/*
 * Copyright Sylko Olzscher 2017
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyy/crypto/certificate.h>
#include <boost/core/ignore_unused.hpp>

namespace cyy 
{
	namespace crypto
	{
		RSA_ptr generate_RSA_private_key(int bits, BN_ULONG exp)
		{
			//	http://stackoverflow.com/questions/5927164/how-to-generate-rsa-private-key-using-openssl
			RSA_ptr rsa(RSA_new(), ::RSA_free);
			BN_ptr bn(BN_new(), ::BN_free);

			int rc = ::BN_set_word(bn.get(), exp);
			if (rc != 1)	return rsa;

			//
			//	generate RSA private key
			//
			rc = ::RSA_generate_key_ex(rsa.get(), bits, bn.get(), NULL);
			return rsa;
		}

		EVP_KEY_ptr generate_public_key(RSA* rsa)
		{
			// Convert RSA to PKEY
			EVP_KEY_ptr pkey(EVP_PKEY_new(), ::EVP_PKEY_free);
			int rc = EVP_PKEY_set1_RSA(pkey.get(), rsa);
			boost::ignore_unused(rc);
			return pkey;
		}

		bool write_RSA_private_key(std::string const& file_name, int bits, BN_ULONG exp)
		{
			RSA_ptr rsa(generate_RSA_private_key(bits, exp));

			// Convert RSA to PKEY
			//EVP_KEY_ptr pkey(EVP_PKEY_new(), ::EVP_PKEY_free);
			//rc = EVP_PKEY_set1_RSA(pkey.get(), rsa.get());

			//
			//	write RSA private key
			//
			//std::cout
			//	<< "write RSA private key "
			//	<< ssl_private_key
			//	<< std::endl;

			BIO_FILE_ptr key(BIO_new_file(file_name.c_str(), "w"), ::BIO_free);
			int rc = ::PEM_write_bio_RSAPrivateKey(key.get(), rsa.get(), NULL, NULL, 0, NULL, NULL);
			return (rc == 1);
		}

		bool write_RSA_key(std::string const& file_name, RSA* rsa)
		{
			BIO_FILE_ptr key(BIO_new_file(file_name.c_str(), "w"), ::BIO_free);
			int rc = ::PEM_write_bio_RSAPrivateKey(key.get(), rsa, NULL, NULL, 0, NULL, NULL);
			return (rc == 1);

		}
		bool write_EVP_key(std::string const& file_name, EVP_PKEY* pkey)
		{
			BIO_FILE_ptr pem(BIO_new_file(file_name.c_str(), "w"), ::BIO_free);
			int rc = ::PEM_write_bio_PUBKEY(pem.get(), pkey);
			return (rc == 1);
		}

		bool gen_X509_request(RSA* rsa
			, std::string const& file_name
			, std::string const& country
			, std::string const& province
			, std::string const& city
			, std::string const& org
			, std::string const& common)
		{
			int version = 1;
			// 1. set version of x509 req
			X509_REQ_ptr x509_req(X509_REQ_new(), ::X509_REQ_free);
			int rc = X509_REQ_set_version(x509_req.get(), version);
			if (rc != 1)	return false;

			// 2. set subject of x509 req
			X509_NAME * x509_name = X509_REQ_get_subject_name(x509_req.get());

			rc = ::X509_NAME_add_entry_by_txt(x509_name, "C", MBSTRING_ASC, (const unsigned char*)country.c_str(), -1, -1, 0);
			if (rc != 1)	return false;

			rc = X509_NAME_add_entry_by_txt(x509_name, "ST", MBSTRING_ASC, (const unsigned char*)province.c_str(), -1, -1, 0);
			if (rc != 1)	return false;

			rc = ::X509_NAME_add_entry_by_txt(x509_name, "L", MBSTRING_ASC, (const unsigned char*)city.c_str(), -1, -1, 0);
			if (rc != 1)	return false;

			rc = ::X509_NAME_add_entry_by_txt(x509_name, "O", MBSTRING_ASC, (const unsigned char*)org.c_str(), -1, -1, 0);
			if (rc != 1)	return false;

			rc = ::X509_NAME_add_entry_by_txt(x509_name, "CN", MBSTRING_ASC, (const unsigned char*)common.c_str(), -1, -1, 0);
			if (rc != 1)	return false;

			// 3. set public key of x509 req
			EVP_KEY_ptr pkey(EVP_PKEY_new(), ::EVP_PKEY_free);
			rc = ::EVP_PKEY_assign_RSA(pkey.get(), rsa);
			if (rc != 1)	return false;
			rsa = NULL;   // will be free rsa when EVP_PKEY_free(pKey)

			rc = ::X509_REQ_set_pubkey(x509_req.get(), pkey.get());
			if (rc != 1)	return false;

			// 4. set sign key of x509 req
			rc = ::X509_REQ_sign(x509_req.get(), pkey.get(), EVP_sha1());    // return x509_req->signature->length
			if (rc < 0)	return false;

			BIO_FILE_ptr csr(BIO_new_file(file_name.c_str(), "w"), ::BIO_free);
			rc = ::PEM_write_bio_X509_REQ(csr.get(), x509_req.get());

			return (rc == 1);
		}

	}
}
