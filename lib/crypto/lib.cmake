# top level files
set (crypto_lib)

set (crypto_cpp

	lib/crypto/src/aes.cpp
	lib/crypto/src/base64.cpp
	lib/crypto/src/certificate.cpp
	src/main/include/cyy/crypto/scrambler.hpp
)

set (crypto_h

	# include all
	src/main/include/cyy/crypto/crypto.h
	src/main/include/cyy/crypto/aes.h
	src/main/include/cyy/crypto/base64.hpp
	src/main/include/cyy/crypto/certificate.h

)

set (crypto_hash


	src/main/include/cyy/crypto/md5.h
	src/main/include/cyy/crypto/sha1.h
	src/main/include/cyy/crypto/sha256.h
	src/main/include/cyy/crypto/sha512.h

	lib/crypto/src/md5.cpp
	lib/crypto/src/sha1.cpp
	lib/crypto/src/sha256.cpp
	lib/crypto/src/sha512.cpp

)


set (crypto_factory

	src/main/include/cyy/intrinsics/factory/crypto_factory.h

	lib/crypto/src/intrinsics/factory/crypto_factory.cpp

)
source_group("factory" FILES ${crypto_factory})
source_group("hash" FILES ${crypto_hash})

# define the core lib
set (crypto_lib
  ${crypto_cpp}
  ${crypto_h}
  ${crypto_hash}  
  ${crypto_factory}  
)