# top level files
set (xml_lib)

set (xml_cpp

	lib/xml/src/xml_io.cpp
	lib/xml/src/xml_manip.cpp

)

set (xml_h

	src/main/include/cyy/xml/xml_io.h
	src/main/include/cyy/xml/xml_manip.h
)

# define the XML library
set (xml_lib
  ${xml_cpp}
  ${xml_h}
)