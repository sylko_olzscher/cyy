/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyy/xml/xml_io.h>
#include <cyy/xml/xml_manip.h>
#include <cyy/intrinsics/type_codes.h>
#include <cyy/intrinsics/type_traits.h>
#include <cyy/intrinsics/size.h>
#include <cyy/value_cast.hpp>
#include <cyy/io.h>
#include <cyy/io/format/buffer_format.h>
#include <cyy/io/classification.h>
#include <cyy/parser/type_parser.h>
#include <cyy/intrinsics/factory.h>
#include <cyy/parser/type_parser.h>
#include <cyy/parser/boost_parser.h>
#include <cyy/parser/chrono_parser.h>

namespace cyy
{
	bool serialize_xml(pugi::xml_node node, object const& obj)
	{
		return serialize_xml(node, obj, xml::custom_callback());
	}

	pugi::xml_node serialize_xml(pugi::xml_node parent, const char* name, object const& obj, xml::custom_callback cb)
	{
		auto node = parent.append_child(name);
		serialize_xml(node, obj, cb);
		return node;
	}

	bool serialize_xml(pugi::xml_node node, object const& obj, xml::custom_callback cb)
	{
		switch (get_code(obj))
		{
			case types::CYY_NULL:	
				//	null, void
				node.append_attribute("xsi:nil") << "true";
				return true;

			case types::CYY_OBJECT:
				//	nested object - try to avoid this
				break;
			
			//	native C++ types
			case types::CYY_BOOL:	//	bool 
			case types::CYY_INT8: // std::int8_t
			case types::CYY_UINT8: // std::uint8_t
			case types::CYY_INT16: // std::int16_t
			case types::CYY_UINT16: // std::uint16_t
			case types::CYY_INT32: // std::int32_t
			case types::CYY_UINT32: // std::uint32_t
			case types::CYY_INT64: // std::int64_t
			case types::CYY_UINT64: // std::uint64_t
			
			case types::CYY_FLOAT32: // float
			case types::CYY_FLOAT64: // double
			case types::CYY_FLOAT80: // long double - usually the same as double
			
			case types::CYY_COMPLEX32: // std::complex<float>
			case types::CYY_COMPLEX64: // std::complex<double>
			case types::CYY_COMPLEX80: // std::complex<long double>

			//	standard library
			case types::CYY_STRING:	//	std::string
			//	note there is no support for std::wstring anymore
			case types::CYY_TIME_STAMP: // std::chrono::system_clock::time_point
			case types::CYY_NANOSECONDS: // std::chrono::nanoseconds
			case types::CYY_MICROSECONDS: // std::chrono::microseconds
			case types::CYY_MILLISECONDS: // std::chrono::milliseconds
			case types::CYY_SECONDS:	//	std::chrono::seconds
			case types::CYY_MINUTES:	//	std::chrono::minutes
			case types::CYY_HOURS:		//	std::chrono::hours
			case types::CYY_DAYS:		//	chrono::days
			
			//	data types defined in cyy library
			case types::CYY_VERSION:
			case types::CYY_REVISION:
			case types::CYY_OP:		//	VM operation
			case types::CYY_INDEX:	//	std::size_t
			case types::CYY_DIFF:	//	std::ptrdiff_t
			case types::CYY_BYTES:	//	std::uint64_t byte counter
			case types::CYY_RGB_COLOR8:	//	color with 8 bits per channel
			case types::CYY_RGB_COLOR16: // color with 16 bits per channel
			case types::CYY_MAC48:	//	media access control address (MAC)
			case types::CYY_MD5: // md5 digest
			case types::CYY_SHA1: // sha1 digest
			case types::CYY_SHA512: // sha512 digest
			
			case types::CYY_ATTR_MAP: // std::map< std::size_t, object >
			case types::CYY_ATTRIBUTE: // std::pair<std::size_t, object>
			case types::CYY_LOCKABLE:	//	std::pair<T,mutex>
			case types::CYY_TABLE:	//	defined in CYY store library
				
			//	datatypes from boost library
			case types::CYY_BOOST_UUID:	//	boost::uuids::uuid
			case types::CYY_BOOST_PATH:	//	boost::filesystem::path
			case types::CYY_BOOST_ERROR: // boost::system::error_code
			case types::CYY_BOOST_IP_ADDRESS: // boost::asio::ip::address
			case types::CYY_BOOST_TCP_ENDPOINT: // boost::asio::ip::tcp::endpoint
			case types::CYY_BOOST_TCP_SOCKET: // boost::asio::ip::tcp::socket
			case types::CYY_BOOST_TRIBOOL:	//	boost::logic::tribool
			{
				const std::string value = to_string(obj);
				node.append_child(pugi::node_pcdata).set_value(value.c_str());

				//
				//	set data type as attribute
				//
				node.append_attribute("type") << types::name(obj.code().first);

				//
				//	set effective size as attribute
				//
				std::size_t size = heap_size(obj);
				if (size != 0)
				{
					node.append_attribute("size") << size;
				}
			}
			return true;

			//	special handling for some data types
			case types::CYY_BUFFER:	//	internal buffer type
				return serialize_xml(node, value_cast<buffer_t>(obj));

			case types::CYY_PARAMETER: // std::pair<std::string, object>
				serialize_xml(node, value_cast<param_t>(obj), cb);
				return true;

			case types::CYY_PARAM_MAP: // std::map< std::string, object >
				return serialize_xml(node, value_cast<param_map_t>(obj), cb);

			case types::CYY_TUPLE:	//	std::list<object>
				return serialize_xml(node, value_cast<tuple_t>(obj), cb);
			case types::CYY_VECTOR:	//	std::vector<object>
				return serialize_xml(node, value_cast<vector_t>(obj), cb);
			case types::CYY_SET:	//	std::set<object>
				return serialize_xml(node, value_cast<set_t>(obj), cb);
				//	complex data types
//			case types::CYY_ARRAY:
			
//			case types::CYY_LABEL:	//	predefines for assembler
//			case types::CYY_CUSTOM_TYPES = 1024,
			default:
				return (!cb)
					? false
					: cb(node, obj)
					;
				break;
		}
		return false;
	}

	bool serialize_xml(pugi::xml_node node, cyy::buffer_t const& buffer)
	{
		const std::string str = buffer_format_hex(buffer);

		if (io::is_printable(buffer))
		{
			node.append_attribute("printable") << "true";

			BOOST_ASSERT(node.type() == pugi::node_element);
			const std::string ascii = buffer_format_ascii(buffer);
			node.append_attribute("ascii") << ascii;
		}
		else
		{
			node.append_attribute("printable") << "false";
		}

		node.append_child(pugi::node_pcdata).set_value(str.c_str());
		return true;;
	}

	pugi::xml_node serialize_xml(pugi::xml_node parent, param_t const& param, xml::custom_callback cb)
	{
		pugi::xml_node node = append_value(parent, param.first, param.second, cb);
		const auto code = get_code(param.second);

		switch (code)
		{
		case types::CYY_TUPLE:
			node.append_attribute("type") = types::type_name(type_traits<param_t>::code_::value);
			node.append_attribute("subtype") = types::type_name(type_traits<tuple_t>::code_::value);
			break;
		case types::CYY_VECTOR:
			node.append_attribute("type") = types::type_name(type_traits<param_t>::code_::value);
			node.append_attribute("subtype") = types::type_name(type_traits<vector_t>::code_::value);
			break;
		case types::CYY_SET:
			node.append_attribute("type") = types::type_name(type_traits<param_t>::code_::value);
			node.append_attribute("subtype") = types::type_name(type_traits<set_t>::code_::value);
			break;

		default:
			break;
		}
		return node;
	}

	bool serialize_xml(pugi::xml_node node, param_map_t const& params, xml::custom_callback cb)
	{
#if __GNUC__ > 5 || defined(_MSC_VER) 
		std::for_each(params.begin(), params.end(), [&node, cb](auto const& v) {
#else
		std::for_each(params.begin(), params.end(), [&node, cb](const_param_t const& v) {
#endif
			append_value(node, v.first, v.second, cb);
		});

		//
		//	set data type amd size as attribute
		//
		node.append_attribute("type") = types::type_name(type_traits<param_map_t>::code_::value);
		node.append_attribute("size") << params.size();
		return true;
	}

	bool serialize_xml(pugi::xml_node parent, tuple_t const& tpl, xml::custom_callback cb)
	{
		pugi::xml_node node = parent.append_child(types::type_name(type_traits<tuple_t>::code_::value));

#if __GNUC__ > 5 || defined(_MSC_VER) 
        std::for_each(tpl.begin(), tpl.end(), [&node, cb](auto const& v) {
#else
        std::for_each(tpl.begin(), tpl.end(), [&node, cb](object const& v) {
#endif
			serialize_xml(node, v, cb);
		});

		//
		//	set data type amd size as attribute
		//
		node.append_attribute("type") << types::name(type_traits<tuple_t>::code_::value);
		node.append_attribute("size") << tpl.size();

		std::stringstream ss;
		ss
			<< "0x"
			<< (void*)&tpl
			;
		const std::string id = ss.str();
		node.append_attribute("id") << id;
		return true;
	}

	bool serialize_xml(pugi::xml_node parent, cyy::vector_t const& vec, xml::custom_callback cb)
	{
		std::stringstream ss;
		ss
			<< types::name(type_traits<tuple_t>::code_::value)
			<< '_'
			<< (void*)&vec
			;
		const std::string tn = ss.str();
		pugi::xml_node node = parent.append_child(tn.c_str());

#if __GNUC__ > 5 || defined(_MSC_VER) 
        std::for_each(vec.begin(), vec.end(), [&node, cb](auto const& v) {
#else
        std::for_each(vec.begin(), vec.end(), [&node, cb](object const& v) {
#endif
			serialize_xml(node, v, cb);
		});

		//
		//	set data type amd size as attribute
		//
		node.append_attribute("type") << types::name(type_traits<tuple_t>::code_::value);
		node.append_attribute("size") << vec.size();
		return true;
	}

	bool serialize_xml(pugi::xml_node parent, cyy::set_t const& set, xml::custom_callback cb)
	{
		std::stringstream ss;
		ss
			<< types::name(type_traits<tuple_t>::code_::value)
			<< "0x"
			<< (void*)&set
			;
		const std::string tn = ss.str();
		pugi::xml_node node = parent.append_child(tn.c_str());

#if __GNUC__ > 5 || defined(_MSC_VER) 
        std::for_each(set.begin(), set.end(), [&node, cb](auto const& v) {
#else
        std::for_each(set.begin(), set.end(), [&node, cb](object const& v) {
#endif
			serialize_xml(node, v, cb);
		});

		//
		//	set data type amd size as attribute
		//
		node.append_attribute("type") << types::name(type_traits<tuple_t>::code_::value);
		node.append_attribute("size") << set.size();

		return true;
	}

	pugi::xml_node append_value(pugi::xml_node parent
		, std::string const& name
		, cyy::object const& obj
		, xml::custom_callback cb)
	{
		pugi::xml_node node = parent.append_child(name.c_str());
		serialize_xml(node, obj, cb);
		return node;
	}

	//
	//	helper function
	//
	const char* node_types[] =
	{
		"null", "document", "element", "pcdata", "cdata", "comment", "pi", "declaration"
	};

	param_t read_pcdata(pugi::xml_node const& parent, types::code type, unsigned int size)
	{
		const std::string name = parent.name();

		pugi::xml_node_iterator it = parent.begin();
		const bool valid = (it != parent.end() && it->type() == pugi::node_pcdata);

#ifdef _DEBUG
		//if (valid)
		//{
		//	std::cout << node_types[it->type()]
		//		<< ", name='"
		//		<< name
		//		<< "', value= "
		//		<< it->value()
		//		<< ", type: "
		//		<< types::type_name(type)
		//		<< ", size: "
		//		<< size
		//		<< "\n";
		//}
#endif

		switch (type)
		{
		case types::CYY_BOOL:
			return std::make_pair(name, valid ? bool_from_str_factory(it->value()) : false_factory());
		case types::CYY_INT8:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::int8_t>(it->value()) : numeric_factory<std::int8_t>(0));
		case types::CYY_UINT8:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::uint8_t>(it->value()) : numeric_factory<std::uint8_t>(0));
		case types::CYY_INT16:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::int16_t>(it->value()) : numeric_factory<std::int16_t>(0));
		case types::CYY_UINT16:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::uint16_t>(it->value()) : numeric_factory<std::uint16_t>(0));
		case types::CYY_INT32:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::int32_t>(it->value()) : numeric_factory<std::int32_t>(0));
		case types::CYY_UINT32:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::uint32_t>(it->value()) : numeric_factory<std::uint32_t>(0));
		case types::CYY_INT64:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::int64_t>(it->value()) : numeric_factory<std::int64_t>(0));
		case types::CYY_UINT64:
			return std::make_pair(name, valid ? numeric_from_str_factory<std::uint64_t>(it->value()) : numeric_factory<std::uint64_t>(0));

		case types::CYY_FLOAT32:
			return std::make_pair(name, valid ? numeric_from_str_factory<float>(it->value()) : numeric_factory<float>(0));
		case types::CYY_FLOAT64:
			return std::make_pair(name, valid ? numeric_from_str_factory<double>(it->value()) : numeric_factory<double>(0));
		case types::CYY_FLOAT80:
			return std::make_pair(name, valid ? numeric_from_str_factory<long double>(it->value()) : numeric_factory<long double>(0));

		case types::CYY_COMPLEX32:
			return std::make_pair(name, string_factory("CYY_COMPLEX32 not implemented yet"));
		case types::CYY_COMPLEX64:
			return std::make_pair(name, string_factory("CYY_COMPLEX64 not implemented yet"));
		case types::CYY_COMPLEX80:
			return std::make_pair(name, string_factory("CYY_COMPLEX80 not implemented yet"));

		case types::CYY_STRING:
			return std::make_pair(name, valid ? string_factory(it->value()) : string_factory());

		case types::CYY_TIME_STAMP:
			BOOST_ASSERT_MSG(size == 8u, "CYY_TIME_STAMP with wrong size");
			return std::make_pair(name, valid ? io::parse_basic_timestamp(it->value()).first : time_point_factory());
		case types::CYY_NANOSECONDS:
			return std::make_pair(name, string_factory("CYY_NANOSECONDS not implemented yet"));
		case types::CYY_MICROSECONDS:
			return std::make_pair(name, string_factory("CYY_MICROSECONDS not implemented yet"));
		case types::CYY_MILLISECONDS:
			return std::make_pair(name, string_factory("CYY_MILLISECONDS not implemented yet"));
		case types::CYY_SECONDS:
			return std::make_pair(name, string_factory("CYY_SECONDS not implemented yet"));
		case types::CYY_MINUTES:
			return std::make_pair(name, string_factory("CYY_MINUTES not implemented yet"));
		case types::CYY_HOURS:
			return std::make_pair(name, string_factory("CYY_HOURS not implemented yet"));
		case types::CYY_DAYS:
			return std::make_pair(name, string_factory("CYY_DAYS not implemented yet"));
		case types::CYY_FILE_STREAM:
			return std::make_pair(name, string_factory("CYY_FILE_STREAM not implemented yet"));
		case types::CYY_STRING_STREAM:
			return std::make_pair(name, string_factory("CYY_STRING_STREAM not implemented yet"));

									//	data types defined in cyy library
		case types::CYY_VERSION:
			return std::make_pair(name, string_factory("CYY_VERSION not implemented yet"));
		case types::CYY_REVISION:
			return std::make_pair(name, string_factory("CYY_REVISION not implemented yet"));
		case types::CYY_OP:
			return std::make_pair(name, string_factory("CYY_OP not implemented yet"));
		case types::CYY_BUFFER:
			return std::make_pair(name, string_factory("CYY_BUFFER not implemented yet"));
		case types::CYY_INDEX:
			return std::make_pair(name, string_factory("CYY_INDEX not implemented yet"));
		case types::CYY_DIFF:
			return std::make_pair(name, string_factory("CYY_DIFF not implemented yet"));
		case types::CYY_BYTES:
			return std::make_pair(name, string_factory("CYY_BYTES not implemented yet"));

		case types::CYY_BOOST_UUID:
			BOOST_ASSERT_MSG(size == 16u, "CYY_BOOST_UUID with wrong size");
			return std::make_pair(name, valid ? io::parse_basic_uuid(it->value()).first : uuid_factory());
		case types::CYY_BOOST_PATH:
			return std::make_pair(name, string_factory("CYY_BOOST_PATH not implemented yet"));
		case types::CYY_BOOST_ERROR:
			return std::make_pair(name, string_factory("CYY_BOOST_ERROR not implemented yet"));
		case types::CYY_BOOST_IP_ADDRESS:
			return std::make_pair(name, string_factory("CYY_BOOST_IP_ADDRESS not implemented yet"));
		case types::CYY_BOOST_TCP_ENDPOINT:
			return std::make_pair(name, string_factory("CYY_BOOST_TCP_ENDPOINT not implemented yet"));
		case types::CYY_BOOST_TCP_SOCKET:
			return std::make_pair(name, string_factory("CYY_BOOST_TCP_SOCKET not implemented yet"));
		case types::CYY_BOOST_TRIBOOL:
			return std::make_pair(name, string_factory("CYY_BOOST_TRIBOOL not implemented yet"));

		default:
			break;
		}
		return std::make_pair(name, factory());
	}

	object traverse(pugi::xml_node const& parent, std::size_t depth)
	{
		//
		//	parser for data type name
		//
		const std::string ts = parent.attribute("type").as_string("null");
		const std::string node_name = parent.name();
		std::pair<types::code, bool> r = cyy::io::get_type_by_name(ts);

		//for (int i = 0; i < depth; ++i) std::cout << "  "; // indentation

		const auto size = parent.attribute("size").as_uint();

#ifdef _DEBUG
		//std::cout 
		//	<< node_types[parent.type()]
		//	<< ", name='"
		//	<< node_name
		//	<< "', value= "
		//	<< parent.value()
		//	<< ", type: "
		//	<< cyy::types::name(r.first)
		//	<< ", size: "
		//	<< size
		//	<< "\n";
#endif

		switch (r.first)
		{
		case types::CYY_TUPLE:
		{
			tuple_t tpl;
			for (pugi::xml_node_iterator it = parent.begin(); it != parent.end(); ++it)
			{
				tpl.push_back(traverse(*it, depth + 1));
			}
			return factory(tpl);
		}
		break;
		case types::CYY_VECTOR:
		{
			vector_t vec;
			for (pugi::xml_node_iterator it = parent.begin(); it != parent.end(); ++it)
			{
				vec.push_back(traverse(*it, depth + 1));
			}
			return factory(vec);
		}
		break;
		case types::CYY_SET:
		{
			set_t set;
			for (pugi::xml_node_iterator it = parent.begin(); it != parent.end(); ++it)
			{
				set.insert(traverse(*it, depth + 1));
			}
			return factory(set);
		}
		break;
		case types::CYY_PARAM_MAP:
		{
			param_map_t pmap;
			for (pugi::xml_node_iterator it = parent.begin(); it != parent.end(); ++it)
			{
				//	parameters expected
				auto obj = traverse(*it, depth + 1);
				//std::cout << cyy::to_literal(obj) << std::endl;
				if (primary_type_code_test<types::CYY_PARAMETER>(obj))
				{
					param_t const& param = value_cast<param_t>(obj);
					pmap[param.first] = param.second;
				}
			}
			return set_factory(node_name, pmap);
		}
		break;
		case types::CYY_PARAMETER:
		{
			pugi::xml_node_iterator it = parent.begin();
			if (it != parent.end())
			{
				param_t param(node_name, traverse(*it, depth + 1));
				return factory(param);
			}
		}
		//	error: child node expected
		return factory(node_name, factory());

		case types::CYY_BOOL:
		case types::CYY_INT8:
		case types::CYY_UINT8:
		case types::CYY_INT16:
		case types::CYY_UINT16:
		case types::CYY_INT32:
		case types::CYY_UINT32:
		case types::CYY_INT64:
		case types::CYY_UINT64:

		case types::CYY_FLOAT32:
		case types::CYY_FLOAT64:
		case types::CYY_FLOAT80:

		case types::CYY_COMPLEX32:
		case types::CYY_COMPLEX64:
		case types::CYY_COMPLEX80:
		case types::CYY_STRING:
		case types::CYY_TIME_STAMP:
		case types::CYY_NANOSECONDS:
		case types::CYY_MICROSECONDS:
		case types::CYY_MILLISECONDS:
		case types::CYY_SECONDS:
		case types::CYY_MINUTES:
		case types::CYY_HOURS:
		case types::CYY_DAYS:
		case types::CYY_FILE_STREAM:
		case types::CYY_STRING_STREAM:
		case types::CYY_VERSION:
		case types::CYY_REVISION:
		case types::CYY_OP:
		case types::CYY_BUFFER:
		case types::CYY_INDEX:
		case types::CYY_DIFF:
		case types::CYY_BYTES:
		case types::CYY_BOOST_UUID:
		case types::CYY_BOOST_PATH:
		case types::CYY_BOOST_ERROR:
		case types::CYY_BOOST_IP_ADDRESS:
		case types::CYY_BOOST_TCP_ENDPOINT:
		case types::CYY_BOOST_TCP_SOCKET:
		case types::CYY_BOOST_TRIBOOL:
			if (!parent.empty())
			{
				//for (int i = 0; i < (depth + 1); ++i) std::cout << "  "; // indentation
				return factory(read_pcdata(parent, r.first, size));
			}
			else
#ifdef _DEBUG
			std::cerr
				<< "EMPTY: "
				<< node_types[parent.type()]
				<< ", name='"
				<< node_name
				<< "', value= "
				<< parent.value()
				<< ", type: "
				<< cyy::types::name(r.first)
				<< ", size: "
				<< size
				<< "\n";
#endif
			break;
		default:
#ifdef _DEBUG
			std::cerr
				<< "UNDEFINED: "
				<< node_types[parent.type()]
				<< ", name='"
				<< node_name
				<< "', value= "
				<< parent.value()
				<< ", type: "
				<< cyy::types::name(r.first)
				<< ", size: "
				<< size
				<< "\n";
#endif
			break;
		}


		return factory();
	}

	object traverse(pugi::xml_node const& node)
	{
#ifdef _DEBUG
		//std::cout
		//	<< "Start with node "
		//	<< node.name()
		//	<< "', value='"
		//	<< node.value()
		//	<< "\n";
#endif
		return traverse(node, 0);
	}

	object load(pugi::xml_node doc, std::string const& query)
	{
		pugi::xpath_node_set data = doc.select_nodes(query.c_str());
		pugi::xpath_node_set::const_iterator it = data.begin();
		if (it != data.end())
		{
// 			pugi::xml_node node = (*it).node();
			return traverse((*it).node().first_child());
		}

		//	
		//	no data found. return empty data set.
		//
		return factory();
	}

}

