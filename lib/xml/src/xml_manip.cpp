/*
 * Copyright Sylko Olzscher 2016
 *
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyy/xml/xml_manip.h>

namespace cyy
{
	pugi::xml_attribute operator<<(pugi::xml_attribute attr, std::string const& value)
	{
		attr = value.c_str();
		return attr;
	}
		
	pugi::xml_attribute operator<<(pugi::xml_attribute attr, std::size_t value)
	{
#ifdef PUGIXML_HAS_LONG_LONG		
		attr = static_cast<unsigned int>(value);
#else
		attr = static_cast<unsigned long long>(value);
#endif
		return attr;
	}

	pugi::xml_attribute operator<<(pugi::xml_attribute attr, index idx)
	{
		attr << *idx;
		return attr;
	}

	pugi::xml_attribute operator >> (pugi::xml_attribute attr, std::string& value)
	{
		value = std::string(attr.value());
		return attr;
	}

	pugi::xml_attribute operator >> (pugi::xml_attribute attr, index& idx)
	{
		idx = attr.as_ullong();
		return attr;
	}

}

