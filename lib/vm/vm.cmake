# top level files
set (vm_lib)

set (vm_cpp

    lib/vm/src/vm.cpp
    lib/vm/src/memory.cpp
    lib/vm/src/stack.cpp
    lib/vm/src/manip.cpp
    lib/vm/src/parser.cpp
    lib/vm/src/vm_dispatcher.cpp
    lib/vm/src/context.cpp
    lib/vm/src/procedure.cpp
    lib/vm/src/librarian.cpp
    lib/vm/src/code_generator.cpp
)

set (vm_h

    src/main/include/cyy/vm/vm.h
	src/main/include/cyy/vm/op_codes.h
	src/main/include/cyy/vm/memory.h
	src/main/include/cyy/vm/stack.h
	src/main/include/cyy/vm/manip.h
	src/main/include/cyy/vm/parser.h
	src/main/include/cyy/vm/vm_dispatcher.h
	src/main/include/cyy/vm/context.h
	src/main/include/cyy/vm/procedure.h
	src/main/include/cyy/vm/librarian.h
	src/main/include/cyy/vm/code_generator.h
	src/main/include/cyy/vm/log_processor.h
)

set (vm_intrinsics
	lib/vm/src/intrinsics/vm_factory.cpp
	lib/vm/src/intrinsics/compare_policy_vm.cpp    
	
	src/main/include/cyy/vm/intrinsics/type_traits_vm.hpp
	src/main/include/cyy/vm/intrinsics/compare_policy_vm.h
	src/main/include/cyy/vm/intrinsics/vm_factory.h	
)

set (vm_domains
	lib/vm/src/domains/file_proc.cpp
	src/main/include/cyy/vm/domains/file_proc.h	
)

source_group("intrinsics" FILES ${vm_intrinsics})
source_group("domains" FILES ${vm_domains})

# define the core lib
set (vm_lib
  ${vm_cpp}
  ${vm_h}
  ${vm_intrinsics}
  ${vm_domains}
)