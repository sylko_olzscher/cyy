/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#include <cyy/vm/librarian.h>
#include <cyy/io/io_set.h>
#include <cyy/value_cast.hpp>
#include <cyy/vm/intrinsics/vm_factory.h>
#include <iostream>
#include <boost/assert.hpp>

namespace cyy 
{
	
	librarian::librarian()
	: db_()
	{
		//
		//	register the librarian functions
		//

		//
		//	insert a function
		//
		insert("lib.insert", 3, [this](cyy::context& ctx){
			
			const cyy::vector_t frame = ctx.stack_frame(false);
			
			const std::string name = cyy::string_cast(frame[0]);
			const std::size_t arity = cyy::index_cast(frame[1]);
			const vm_call fun = cyy::value_cast<vm_call>(frame[2]);
			
 			this->insert(name, arity, fun);
		});
		
		//
		//	clear library
		//
		insert("lib.clear", 0, std::bind(&librarian::clear, this));
		
		//
		//	delete a specific function
		//
		insert("lib.erase", 1, [this](cyy::context& ctx){
			
			const cyy::vector_t frame = ctx.stack_frame(false);						
			const std::string name = cyy::string_cast(frame[0]);
 			this->erase(name);

		});

		//
		//	Even an invoke is possible (but with the same
		//	context - and therefore complete experimental)
		//
		insert("lib.invoke", 1, [this](cyy::context& ctx){
			
			const cyy::vector_t frame = ctx.stack_frame(false);
			
			const std::string name = cyy::string_cast(frame[0]);

			//	standard function call (no out of band)
 			this->invoke(name, ctx);
			
		});
	
		//
		//	swap a function.
		//	The previous found function is poped onto stack
		//
		insert("lib.swap", 2, [this](cyy::context& ctx){
			
			const cyy::vector_t frame = ctx.stack_frame(false);
			
			const std::string name = cyy::string_cast(frame[0]);
			const vm_call fun = cyy::value_cast<vm_call>(frame[1]);
			
			vm_call f = this->swap(name, fun);
 			ctx.push(factory(f));
			
		});		
	}
	
	bool librarian::insert(std::string const& name, procedure&& p)
	{
		auto pos = db_.find(name);
		if (pos == db_.end())
		{	
			return db_.emplace(std::piecewise_construct
				, std::forward_as_tuple(name)
				, std::forward_as_tuple(std::move(p))).second;
		}
		return false;
	}
	
	bool librarian::insert(std::string const& name
	, std::size_t arity
	, vm_call f)
	{
		return insert(name, procedure(f, arity));
	}
	
	bool librarian::erase(std::string const& name)
	{
		return db_.erase(name) != 0;
	}

	vm_call librarian::swap(std::string const& name, vm_call f)
	{
		auto pos = db_.find(name);
		if (pos != db_.end())
		{
			return (*pos).second.swap(f);
		}
		return vm_call();
	}

	void librarian::clear()
	{
		db_.clear();
	}
	
	bool librarian::invoke(std::string const& name, context& ctx) const
	{
		auto pos = db_.find(name);
		if (pos != db_.end())
		{
			//	test stack size 
			const std::size_t arity = (*pos).second.arity();
			if (ctx.frame_size() >= arity)
			{
				
				//	call procedure
				//	increase activation_counter_
				//activate a(*this);

				//
				//	test recursion depth
				//
				//BOOST_ASSERT_MSG(get_activation_count() == 1, "recursion");

				(*pos).second(ctx);
				return true;
			}
			else 
			{
				std::cerr 
					<< "\n***Warning: insufficient frame size: "
					<< ctx.frame_size()
					<< '/'
					<< arity 
					<< " ["
					<< name
					<< "]\n"
					<< std::endl 
					;
			}
		}
		return false;
	}		
}

