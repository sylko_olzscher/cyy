/*
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 * 
 * Copyright Sylko Olzscher 2016
 */

#include <cyy/io/text_io.h>
#include <iomanip>
#include <cyy/io/format/version_format.h>
#include <cyy/io/format/digest_format.h>
#include <cyy/io/format/time_format.h>
#include <cyy/io/format/bytes_format.h>
#include <cyy/io/format/buffer_format.h>
#include <cyy/io/format/mac_format.h>
#include <cyy/io/format/color_format.h>
#include <cyy/io/format/boost_format.h>

#include <cyy/io/classification.h>

#include <cyy/io/stream_io.h>	//!
#include <cyy/io/io_set.h>
#include <cyy/util/brute_cast.hpp>
#include <boost/io/ios_state.hpp>

namespace cyy 
{
	namespace io	
	{
		std::ostream& write_value(std::ostream& os, bool value)
		{
			os << ((value) ? "true" : "false");
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, std::int8_t value)
		{
			os << promote_to_printable_integer_type(value);
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, std::uint8_t value)
		{
			os << static_cast<unsigned>( value );
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, version const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, revision const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, buffer_t const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, op value)
		{
			os << cyy::codes::name(value.get_code());
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, index value)
		{
			os << *value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, diff value)
		{
			os << *value;
			return os;
		}

		std::ostream& write_value(std::ostream& os, bytes value)
		{
			os << *value;
			return os;
		}

		std::ostream& write_value(std::ostream& os, color_8 const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, color_16 const& value)
		{
			os << value;
			return os;
		}

		std::ostream& write_value(std::ostream& os, mac48 const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, mac64 const& value)
		{
			os << value;
			return os;
		}

		std::ostream& write_value(std::ostream& os, cyy::crypto::digest_md5 const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, cyy::crypto::digest_sha1 const& value)
		{
			os << value;
			return os;
		}

		std::ostream& write_value(std::ostream& os, cyy::crypto::digest_sha512 const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, attr_map_t const& value, custom_callback cb)
		{
			bool flag = false;
			std::for_each(value.begin()
				, value.end()
				, [&flag, &os, cb](attr_map_t::value_type const& attr){
					
				if (flag)
				{
					os << ',';
				}
				else 
				{
					flag = true;
				}
				
				os << '(' << attr.first << ':';
				serialize_stream(os, attr.second, cb);
				os << ')';
			});
			
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, param_map_t const& value, custom_callback cb)
		{
			bool flag = false;
			std::for_each(value.begin()
				, value.end()
				, [&flag, &os, cb](param_map_t::value_type const& param){
					
				if (flag)
				{
					os << ',';
				}
				else 
				{
					flag = true;
				}
				
				os << '(' << '"' << param.first << '"' << ':';
				serialize_stream(os, param.second, cb);
				os << ')';
			});
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, attr_t const& value, custom_callback cb)
		{
			return to_stream(os, value, cb);
		}
		
		std::ostream& write_value(std::ostream& os, param_t const& value, custom_callback cb)
		{
			return to_stream(os, value, cb);
		}
		
		std::ostream& write_value(std::ostream& os, tuple_t const& value, custom_callback cb)
		{
			return to_stream(os, value, cb);
		}

		std::ostream& write_value(std::ostream& os, vector_t const& value, custom_callback cb)
		{
			return to_stream(os, value, cb);
		}

		std::ostream& write_value(std::ostream& os, set_t const& value, custom_callback cb)
		{
			//	ToDo: implement
			//return to_stream(os, value, cb);
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, boost::uuids::uuid value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, boost::filesystem::path const& value)
		{
			os << value;
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, boost::asio::ip::address const& value)
		{
			os << value.to_string();
			return os;
		}
		
		std::ostream& write_value(std::ostream& os, std::chrono::system_clock::time_point const& value)
		{
			os << value;
			return os;
		}

		std::ostream& write_value(std::ostream& os, boost::logic::tribool value)
		{
			//	write true, false or indeterminate
			if (value) 
			{
				os << "T:3";
			}
			else if (!value) 
			{
				os << "F:3";
			}
			else 
			{
				os << "I:3";
			}
			return os;
		}
		
		template <>
		std::ostream& write_value_type<std::string>(std::ostream& os, std::string const& value)
		{
			os << '"';
			std::for_each(value.begin(), value.end(), [&os](char c){
				switch (c )
				{
				case '\a':
					os << '\\' << 'a';
					break;
				case '\b':
					os << '\\' << 'b';
					break;
				case '\f':
					os << '\\' << 'f';
					break;
				case '\n':
					os << '\\' << 'n';
					break;
				case '\r':
					os << '\\' << 'r';
					break;
				case '\t':
					os << '\\' << 't';
					break;
				case '\v':
					os << '\\' << 'v';
					break;
				case '\\':
					os << '\\' << '\\';
					break;
				case '\'':
					os << '\\' << '\'';
					break;
				case '\"':
					os << '\\' << '\"';
					break;
				default:
					os << c;
					//	ToDo: escape other non-printable characters
					break;
				}
			});
			os << '"';
			
			return os;
		}

		template <>
		std::ostream& write_value_type<std::chrono::system_clock::time_point>(std::ostream& os, std::chrono::system_clock::time_point const& value)
		{
 			os << '"' << value << '"';
 			return write_type<std::chrono::system_clock::time_point>(os);
		}
	
		template <>
		std::ostream& write_value_type<version>(std::ostream& os, version const& value)
		{
			write_type<version>(os);
			os << value;
			return os;
		}

		template <>
		std::ostream& write_value_type<revision>(std::ostream& os, revision const& value)
		{
			write_type<revision>(os);
			os << value;
			return os;
		}
	
		template <>
		std::ostream& write_value_type(std::ostream& os, buffer_t const& value)
		{
			os << '\'';
			write_value(os, value);
			os << '\'';
			return os;
		}
	
		std::ostream& write_value_type(std::ostream& os, bytes value)
		{
			os << value;
			return os;
		}
		
		template <>
		std::ostream& write_value_type(std::ostream& os, color_8 const& value)
		{
			os << '#';
			return write_value(os, value);
		}

		template <>
		std::ostream& write_value_type(std::ostream& os, color_16 const& value)
		{
			os << '#' << '#';
			return write_value(os, value);
		}
		
		template <>
		std::ostream& write_value_type<mac48>(std::ostream& os, mac48 const& value)
		{
			write_value(os, value);
			os << ':';
			write_type<mac48>(os);
			return os;
		}

		template <>
		std::ostream& write_value_type<mac64>(std::ostream& os, mac64 const& value)
		{
			write_value(os, value);
			os << ':';
			write_type<mac64>(os);
			return os;
		}

		template <>
		std::ostream& write_value_type<crypto::digest_md5>(std::ostream& os, crypto::digest_md5 const& value)
		{
			write_value(os, value);
			os << ':';
			write_type<crypto::digest_md5>(os);
			return os;
		}

		template <>
		std::ostream& write_value_type<crypto::digest_sha1>(std::ostream& os, crypto::digest_sha1 const& value)
		{
			write_value(os, value);
			os << ':';
			write_type<crypto::digest_sha1>(os);
			return os;
		}

		template <>
		std::ostream& write_value_type<crypto::digest_sha512>(std::ostream& os, crypto::digest_sha512 const& value)
		{
			write_value(os, value);
			os << ':';
			write_type<crypto::digest_sha512>(os);
			return os;
		}

		
		std::ostream& write_value_type(std::ostream& os, tuple_t const& value, custom_callback cb)
		{
			os << '{';

			//	serialize each element from the tuple
			bool init = false;
			std::for_each(value.begin(), value.end(), [&os, &init, cb](object const& obj){
				if (!init)	{
					init = true;
				}
				else {
					os << ",";
				}
				serialize_stream(os, obj, cb);
			});

			os << '}';
			return os;
		}
		
		std::ostream& write_value_type(std::ostream& os, vector_t const& value, custom_callback cb)
		{
			os << '[';

			//	serialize each element from the vector
			bool init = false;
			std::for_each(value.begin(), value.end(), [&os, &init, cb](object const& obj){
				if (!init)	{
					init = true;
				}
				else {
					os << ",";
				}
				serialize_stream(os, obj, cb);
			});

			os << ']';
			return os;
		}
	
		std::ostream& write_value_type(std::ostream& os, set_t const& value, custom_callback cb)
		{
			os << '<';

			//	serialize each element from the vector
			bool init = false;
			std::for_each(value.begin(), value.end(), [&os, &init, cb](object const& obj){
				if (!init)	{
					init = true;
				}
				else {
					os << ",";
				}
				serialize_stream(os, obj, cb);
			});

			return os << '>';
		}

		std::ostream& write_value_type(std::ostream& os, attr_map_t const& value, custom_callback cb)
		{
			os << '[';
			return write_value(os, value, cb) << ']';
		}

		std::ostream& write_value_type(std::ostream& os, param_map_t const& value, custom_callback cb)
		{
			os << '(';
			return write_value(os, value, cb) << ')';
		}
	
		std::ostream& write_value_type(std::ostream& os, attr_t const& value, custom_callback cb)
		{
			return to_literal(os, value, cb);
		}

		std::ostream& write_value_type(std::ostream& os, param_t const& value, custom_callback cb)
		{
			return to_literal(os, value, cb);
		}

		std::ostream& write_value_type(std::ostream& os, boost::uuids::uuid value)
		{
			os << '"';
			write_value(os, value);
			os << '"';
			return write_type<boost::uuids::uuid>(os);
		}
	
		template <>
		std::ostream& write_value_type<boost::filesystem::path>(std::ostream& os, boost::filesystem::path const& value)
		{
			//	already wrapped by "'s
			write_value(os, value);
			return write_type<boost::filesystem::path>(os);
		}
	
		template <>
		std::ostream& write_value_type(std::ostream& os, boost::asio::ip::address const& value)
		{
			os << '"';
			write_value(os, value);
			os << '"';
			return write_type<boost::asio::ip::address>(os);
		}
	}
}	//	cyy

