/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015-2016 Sylko Olzscher.
 */
#include <cyy/io/format/digest_format.h>
#include <cyy/util/brute_cast.hpp>
#include <iomanip>
#include <sstream>
#include <boost/io/ios_state.hpp>

namespace cyy 
{
	namespace crypto 
	{
		std::ostream& digest_format(std::ostream& os, crypto::digest_md5::value_type const& v)
		{
			//	store and reset stream state
			boost::io::ios_flags_saver  ifs(os);

			os
			<< std::setfill('0')
			<< std::hex
			;
			
			//	write buffer
			for ( const char c : v)
			{
				os
				<< std::setw(2)
				<< promote_to_printable_integer_type(c)
				;
			}
			return os;
		}
		
		std::ostream& digest_format(std::ostream& os, crypto::digest_sha1::value_type const& v)
		{
			//	store and reset stream state
			boost::io::ios_flags_saver  ifs(os);

			os
			<< std::setfill('0')
			<< std::hex
			;
			
			//	write buffer
			for ( const char c : v)
			{
				os
				<< std::setw(2)
				<< promote_to_printable_integer_type(c)
				;
			}
			return os;
			
		}
		
		std::ostream& digest_format(std::ostream& os, crypto::digest_sha256::value_type const& v)
		{
			//	store and reset stream state
			boost::io::ios_flags_saver  ifs(os);

			os
				<< std::setfill('0')
				<< std::hex
				;

			//	write buffer
			for (const char c : v)
			{
				os
					<< std::setw(2)
					<< promote_to_printable_integer_type(c)
					;
			}
			return os;
		}
	
		std::ostream& digest_format(std::ostream& os, crypto::digest_sha512::value_type const& v)
		{
			//	store and reset stream state
			boost::io::ios_flags_saver  ifs(os);

			os
			<< std::setfill('0')
			<< std::hex
			;
			
			//	write buffer
			for ( const char c : v)
			{
				os
				<< std::setw(2)
				<< promote_to_printable_integer_type(c)
				;
			}
			return os;
		}
	}
	
	std::ostream& operator<<(std::ostream& os, crypto::digest_md5 const& v)
	{
		return crypto::digest_format(os, v.data_);
	}
	
	std::ostream& operator<<(std::ostream& os, crypto::digest_sha1 const& v)
	{
		return crypto::digest_format(os, v.data_);
	}
	
	std::ostream& operator<<(std::ostream& os, crypto::digest_sha256 const& v)
	{
		return crypto::digest_format(os, v.data_);
	}

	std::ostream& operator<<(std::ostream& os, crypto::digest_sha512 const& v)
	{
		return crypto::digest_format(os, v.data_);
	}
	
	std::string to_string(crypto::digest_md5::value_type const& v)
	{
		std::stringstream ss;
		ss << '"';
		crypto::digest_format(ss, v);
		ss << '"';
		return ss.str();
	}

	std::string to_string(crypto::digest_sha1::value_type const& v)
	{
		std::stringstream ss;
		ss << '"';
		crypto::digest_format(ss, v);
		ss << '"';
		return ss.str();
	}

	std::string to_string(crypto::digest_sha256::value_type const& v)
	{
		std::stringstream ss;
		ss << '"';
		crypto::digest_format(ss, v);
		ss << '"';
		return ss.str();
	}

	std::string to_string(crypto::digest_sha512::value_type const& v)
	{
		std::stringstream ss;
		ss << '"';
		crypto::digest_format(ss, v);
		ss << '"';
		return ss.str();
	}


}	//	cyy


