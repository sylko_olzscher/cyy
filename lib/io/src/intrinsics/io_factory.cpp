/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2016 Sylko Olzscher.
 *
 */

#include <cyy/io/intrinsics/io_factory.h>
#include <cyy/io/intrinsics/type_traits_io.hpp>
#include <cyy/io/intrinsics/compare_policy_io.h>
#include <cyy/detail/make_value.hpp>

namespace cyy {
	
#if __GNUC__ > 4 || defined(_MSC_VER) 
    object factory(std::fstream&& stream)
	{
 		return object(core::make_value<std::fstream>(std::move(stream)));
	}
	
	object filestream_factory(std::string const& name, std::ios_base::openmode mode )
	{
 		return object(core::make_value<std::fstream>(name, mode));
	}
	
	object factory(std::stringstream&& stream)
	{
 		return object(core::make_value<std::stringstream>(std::move(stream)));
	}
	
	object stringstream_factory()
	{
 		return object(core::make_value<std::stringstream>());
	}
#endif
}	//	cyy

