# top level files
set (io_lib)

set (io_cpp

    lib/io/src/native_io.cpp
    lib/io/src/stream_io.cpp
    lib/io/src/binary_io.cpp
    lib/io/src/text_io.cpp
    lib/io/src/cpp_io.cpp
    lib/io/src/classification.cpp
    lib/io/src/io.cpp
    lib/io/src/io_set.cpp
    lib/io/src/match.cpp

)

set (io_h

	src/main/include/cyy/io.h
	src/main/include/cyy/io/io_callback.h
	src/main/include/cyy/io/native_io.h
	src/main/include/cyy/io/stream_io.h
	src/main/include/cyy/io/cpp_io.h
	src/main/include/cyy/io/binary_io.h
	src/main/include/cyy/io/text_io.h
	src/main/include/cyy/io/classification.h
	src/main/include/cyy/io/hex_dump.hpp
	src/main/include/cyy/io/match.h

	src/main/include/cyy/io/write_binary.hpp
	src/main/include/cyy/io/read_binary.hpp
	src/main/include/cyy/io/io_set.h
	
)

# parser
set (io_parser

	src/main/include/cyy/io/native_parser.h
    lib/io/src/native_parser.cpp

	#
	#	non spirit based parser
	#
    lib/io/src/parser/bom_parser.cpp
    

	#
	#	non spirit based parser
	#
	src/main/include/cyy/io/parser/bom_parser.h
)

# format
set (io_formatter

	lib/io/src/format/time_format.cpp
	lib/io/src/format/version_format.cpp
	lib/io/src/format/digest_format.cpp
	lib/io/src/format/mac_format.cpp
	lib/io/src/format/bytes_format.cpp
	lib/io/src/format/buffer_format.cpp
	lib/io/src/format/color_format.cpp
	lib/io/src/format/boost_format.cpp

	src/main/include/cyy/io/format/time_format.h
	src/main/include/cyy/io/format/version_format.h
	src/main/include/cyy/io/format/digest_format.h
	src/main/include/cyy/io/format/mac_format.h
	src/main/include/cyy/io/format/bytes_format.h
	src/main/include/cyy/io/format/buffer_format.h
	src/main/include/cyy/io/format/color_format.h
	src/main/include/cyy/io/format/boost_format.h
)

# intrinsics
set (io_intrinsics

	lib/io/src/intrinsics/io_factory.cpp
 
	src/main/include/cyy/io/intrinsics/type_traits_io.hpp
	src/main/include/cyy/io/intrinsics/io_factory.h
	src/main/include/cyy/io/intrinsics/compare_policy_io.h
)

source_group("parser" FILES ${io_parser})
source_group("formatter" FILES ${io_formatter})
source_group("intrinsics" FILES ${io_intrinsics})

# define the core lib
set (io_lib
  ${io_cpp}
  ${io_h}
  ${io_parser}
  ${io_formatter}
  ${io_intrinsics}
)
