# top level files
set (json_lib)

set (json_cpp


    lib/json/src/json_parser.cpp
	lib/json/src/json_parser.hpp
    lib/json/src/json_io.cpp
    lib/json/src/convert.cpp
    
)

set (json_h

	src/main/include/cyy/json/json_parser.h
	src/main/include/cyy/json/json_io.h    
	src/main/include/cyy/json/convert.h
	
)

# define the JSON lib
set (json_lib
  ${json_cpp}
  ${json_h}
)
