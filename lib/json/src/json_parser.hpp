/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 *
 */
#ifndef CYY_IO_PARSER_JSON_HPP
#define CYY_IO_PARSER_JSON_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
  #pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/json/json_parser.h>
#include <cyy/intrinsics/factory/set_factory.h>
#include <cyy/intrinsics/factory/boolean_factory.h>
#include <cyy/intrinsics/factory/null_factory.h>
#include <cyy/intrinsics/factory/set_factory.h>
#include <cyy/intrinsics/factory/chrono_factory.h>
#include <cyy/intrinsics/factory/boost_factory.h>
#ifdef _DEBUG
#include <cyy/parser/spirit_support.hpp>
#endif

#include <boost/spirit/home/support/attributes.hpp>	//	transform_attribute
#include <boost/spirit/include/phoenix.hpp>	//	enable assignment of values like cyy::object

namespace boost		
{ 
	namespace spirit	
	{ 
		namespace traits	
		{ 
	
			template <> 
			struct transform_attribute< cyy::object, cyy::tuple_t, boost::spirit::qi::domain > 
			{ 
				typedef cyy::tuple_t type; 
				static cyy::tuple_t pre(cyy::object& v) 
				{ 
					return cyy::tuple_t(); 
				} 
				static void post(cyy::object& val, cyy::tuple_t const& attr) 
				{
					val = cyy::factory(attr);
				} 
				static void fail(cyy::object&) {}
			}; 

			template <> 
			struct transform_attribute< cyy::object, cyy::vector_t, boost::spirit::qi::domain > 
			{ 
				typedef cyy::vector_t type; 
				static cyy::vector_t pre(cyy::object& v) 
				{ 
					return cyy::vector_t(); 
					
				} 
				static void post(cyy::object& val, cyy::vector_t const& attr) 
				{
					val = cyy::factory(attr);
				} 
				static void fail(cyy::object&) {}
			}; 

		}	//	traits
	}	//	spirit
}	//	boost
	
namespace cyy	
{
	namespace io	
	{
		namespace	
		{

			//	Transform an Attribute to a Different Type 
			//	boost sprits transform_attribute<> template would be another
			//	solution.
			struct cast_from_member_impl
			{
				template < typename A >
				struct result { typedef object type; };

				object operator()(boost::fusion::vector < std::string, object > arg) const
				{
					const std::string key = boost::fusion::front(arg);
					const object value = boost::fusion::back(arg);
					return factory(key, value);
				}
			};
		}	//	namespace *anonymous*
	
		template <typename Iterator, typename Skipper>
		json_parser< Iterator, Skipper > :: json_parser()
			: json_parser::base_type( r_json )
		{
			boost::phoenix::function<cast_from_member_impl>	cast_from_member;
			
			r_json
				= r_obj[boost::spirit::_val = boost::spirit::_1]
				;
				
			r_obj
				%= r_uuid_obj	//	auto detect UUIDs
				| r_numeric		//	int, uint, double
				| r_string		//	already object
				| (boost::spirit::qi::lit('@') > r_dt)[boost::spirit::_val = boost::spirit::_1]
				| boost::spirit::qi::lit("true")[boost::spirit::_val = true_factory()]
				| boost::spirit::qi::lit("false")[boost::spirit::_val = false_factory()]
				| boost::spirit::qi::lit("null")[boost::spirit::_val = factory()]
				| ('[' > (r_vector | boost::spirit::eps[boost::spirit::_val = vector_factory()])> ']')	//	automatic conversion 
				| ('{' > (r_members | boost::spirit::eps[boost::spirit::_val = tuple_factory()]) > '}')	//	automatic conversion 
				;
			
			r_members
				%= p_ % ','
				;
			
			//	needed as conversion step from "r_pair" to "object"
 			p_
 				= r_pair[boost::spirit::_val = cast_from_member(boost::spirit::_1)]
 				;

  			r_pair
  				= (r_str | r_ident ) >> boost::spirit::qi::lit(':') >> r_obj.alias()
				//= (r_str | r_ident) >> boost::spirit::qi::lit(':') >> r_value
				;

			r_value
				= r_obj[boost::spirit::_val = boost::spirit::_1]
				;

			//
			//	no type check. it's possible to populate
			//	the vector with values of different data types.
			//
			r_vector
				= r_value % ','
				;
			
			r_uuid_obj
				= r_uuid[boost::spirit::_val = boost::phoenix::bind(&uuid_from_buffer_factory, boost::spirit::_1)]
				;

			r_uuid
				= '"'
				>> boost::spirit::qi::repeat(4)[r_hex2] >> '-'
				>> boost::spirit::qi::repeat(2)[r_hex2] >> '-'
				>> boost::spirit::qi::repeat(2)[r_hex2] >> '-'
				>> boost::spirit::qi::repeat(2)[r_hex2] >> '-'
				>> boost::spirit::qi::repeat(6)[r_hex2]
				>> '"'
				;

			r_json.name("json_parser");
			
		}
	}	//	io
} 	//	cyy

#endif	// CYY_IO_PARSER_JSON_HPP


