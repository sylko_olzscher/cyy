# top level files
set (demo_lib)

set (demo_cpp

    lib/dload/src/example/demo.cpp

)

set (demo_h

    src/main/include/cyy/sys/dload/example/demo.h
)


# define the demo lib
set (demo_lib
  ${demo_cpp}
  ${demo_h}
)