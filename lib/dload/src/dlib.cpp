/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */


#include <cyy/sys/dload/dlib.h>

#include <cstdint>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/algorithm/string/predicate.hpp>

namespace cyy {
	namespace sys
	{
		dlib::dlib()
		: h_(nullptr)
		, references_(0)
		{
			//	 returned handle is for the main program
			load(nullptr);
		}
				
		dlib::dlib(std::string const& name)
		: h_(nullptr)
		, references_(0)
		{
			const std::string p = prepare_lib_path(name);
			load(p.c_str());
		}
		
		dlib::dlib(dlib&& other)
		: h_(std::move(other.h_))
		, references_(other.references_.load())
		{
			other.h_ = nullptr;
		}

		dlib::~dlib()
		{
			close();
			BOOST_ASSERT_MSG(references_ == 0, "open references");
		}
		
		dlib& dlib::operator=(dlib&& other)
		{
			std::swap(h_, other.h_);
			references_.exchange(other.references_);
			other.references_ = 0;
			return *this;
		}

		
		bool dlib::is_loaded() const
		{
			return (h_ != nullptr);
		}


		void dlib::close()
		{
#if BOOST_OS_WINDOWS
			::FreeLibrary(h_);
#else
			if (::dlclose(h_) != 0)
			{
				std::cerr
				<< ::dlerror()
				<< std::endl
				;
			}
#endif			
		}
		
		void dlib::load(const char* name)
		{
			//close();
			
			
#if BOOST_OS_WINDOWS
			h_ = ::LoadLibrary(name);
#else
			//	clear previous errors
			::dlerror();
//			h_ = ::dlopen(name, RTLD_NOW | RTLD_LOCAL);
			h_ = ::dlopen(name, RTLD_NOW | RTLD_GLOBAL);
#endif
		}
		
		void* dlib::get_address(std::string const& sym) const
		{
			BOOST_ASSERT_MSG(!sym.empty(), "symbol name required" );
			const char* s = sym.c_str();
#if BOOST_OS_WINDOWS
			//	returns int (WINAPI *FARPROC)();
			return (void*)::GetProcAddress(h_, s);
#else
			return ::dlsym(h_, s);
#endif
			
		}

		//	++x
		dlib& dlib::operator++()
		{
			++references_;
			return *this;
		}
		
		//	x++
		std::size_t dlib::operator++(int)
		{
			return references_++;
		}
		
		dlib& dlib::operator--()
		{
			--references_;
			return *this;
		}
		
		std::size_t dlib::operator--(int)
		{
			return references_--;
		}
			
		std::size_t dlib::ref_count() const
		{
			return references_;
		}

		std::string prepare_lib_path(std::string const& name)
		{
			boost::filesystem::path p(name);
			if (!p.has_extension())	{
				p.replace_extension( lib_extension );
			}
			
#if BOOST_OS_WINDOWS
			if (!boost::algorithm::starts_with(p.filename().string(), lib_prefix))
			{
				p = p.parent_path() / (lib_prefix + p.filename().string());
			}
#endif
			return p.string();

		}
		

		bool operator==(dlib const& lhs, dlib const& rhs)
		{
			return (lhs.h_ == rhs.h_);
		}
		
		bool operator<(dlib const& lhs, dlib const& rhs)
		{
			return (std::uintptr_t)lhs.h_ < (std::uintptr_t)rhs.h_;
		}

	}	//	sys
}	//	cyy 

