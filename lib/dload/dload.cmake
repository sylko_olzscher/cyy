# top level files
set (dload_lib)

set (dload_cpp

    lib/dload/src/dlib.cpp

)

set (dload_h

    src/main/include/cyy/sys/dload/dload.h
    src/main/include/cyy/sys/dload/dlib.h
	src/main/include/cyy/sys/dload/class_loader.hpp
)


# define the dload lib
set (dload_lib
  ${dload_cpp}
  ${dload_h}
)