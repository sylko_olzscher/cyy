/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 *
 */

#include "chrono_parser.hpp"
#include <cyy/intrinsics/buffer.hpp>
#include <cyy/intrinsics/factory/null_factory.h>

namespace cyy	
{
	namespace io	
	{

		template struct CYY_PARSER_LIBRARY_API chrono_parser <std::string::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API chrono_parser <buffer_t::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API db_timestamp_parser <std::string::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API timespan_parser <std::string::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API rfc3339_timestamp_parser <std::string::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API rfc3339_timestamp_parser <buffer_t::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API rfc3339_obj_parser <std::string::const_iterator>;
		template struct CYY_PARSER_LIBRARY_API rfc3339_obj_parser <buffer_t::const_iterator>;

		std::pair<object, bool > parse_basic_timestamp(std::string const& inp)
		{
			//std::chrono::system_clock::time_point result = std::chrono::system_clock::now();
			object result = time_point_factory();
			timepoint_basic_parser< std::string::const_iterator >	g;
			std::string::const_iterator iter = inp.begin();
			std::string::const_iterator end = inp.end();
			const bool r = boost::spirit::qi::parse(iter, end, g, result);
			return (r)
				? std::make_pair(result, r)
				: std::make_pair(factory(), r)
				;
		}

		std::pair<std::chrono::system_clock::time_point, bool > parse_db_timestamp(std::string const& inp)
		{
			std::chrono::system_clock::time_point result = std::chrono::system_clock::now();
			db_timestamp_parser< std::string::const_iterator >	g;
			std::string::const_iterator iter = inp.begin();
			std::string::const_iterator end = inp.end();
			const bool r = boost::spirit::qi::parse(iter, end, g, result);
			return std::make_pair(result, r);
		}
		
		std::pair<std::chrono::system_clock::time_point, bool > parse_rfc3339_timestamp(std::string const& inp)
		{
			std::chrono::system_clock::time_point result = std::chrono::system_clock::now();
			rfc3339_timestamp_parser< std::string::const_iterator >	g;
			std::string::const_iterator iter = inp.begin();
			std::string::const_iterator end = inp.end();
			const bool r = boost::spirit::qi::parse(iter, end, g, result);
			return std::make_pair(result, r);
		}

		std::pair<std::chrono::microseconds, bool > parse_timespan(std::string const& inp)
		{
			std::chrono::microseconds result(0);
			timespan_parser< std::string::const_iterator >	g;
			std::string::const_iterator iter = inp.begin();
			std::string::const_iterator end = inp.end();
			const bool r = boost::spirit::qi::parse(iter, end, g, result);
			return std::make_pair(result, r);
		}
		
		object parse_rfc3339_obj(std::string const& inp)
		{
			object obj;
			rfc3339_obj_parser< std::string::const_iterator >	g;
			std::string::const_iterator iter = inp.begin();
			std::string::const_iterator end = inp.end();
			const bool r = boost::spirit::qi::parse(iter, end, g, obj);
			return (r)
				? obj
				: factory()
				;

		}

	}	//	namespace io
}	//	namespace cyy

 
