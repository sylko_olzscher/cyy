/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */
#ifndef CYY_IO_PARSER_OBJECT_HPP
#define CYY_IO_PARSER_OBJECT_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
  #pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/parser/object_parser.h>
#include <cyy/parser/spirit_support.hpp>
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <cyy/value_cast.hpp>
#include <cyy/io.h>
#include <cyy/io/io_set.h>

#include <boost/spirit/home/support/attributes.hpp>	//	transform_attribute
#include <boost/spirit/include/phoenix.hpp>	//	enable assignment of values like cyy::object

namespace boost 
{
	namespace spirit	
	{
		namespace traits 
		{

			/**
			 * Define a customization point with transform_attribute<>.
			 * 
			 * @see http://boost-spirit.com/home/2010/02/08/how-to-adapt-templates-as-a-fusion-sequence/
			 */
			template <>
			struct transform_attribute< cyy::object, boost::fusion::vector < std::string, cyy::object >, boost::spirit::qi::domain > 
			{ 
				typedef boost::fusion::vector < std::string, cyy::object > type; 
				static type pre(cyy::object& v) { return type("", cyy::object()); } 
				static void post(cyy::object& obj, type const& attr) {
					
					const std::string key = boost::fusion::front( attr );
					const cyy::object value = boost::fusion::back( attr );
					obj = cyy::factory(key, value);
				} 
				static void fail(cyy::object&) 
				{}
			}; 
			
			template <>
			struct transform_attribute< cyy::object, boost::fusion::vector < std::size_t, cyy::object >, boost::spirit::qi::domain > 
			{ 
				typedef boost::fusion::vector < std::size_t, cyy::object > type; 
				static type pre(cyy::object& v) { return type(0u, cyy::object()); } 
				static void post(cyy::object& obj, type const& attr) {
					
					const std::size_t index = boost::fusion::front( attr );
					const cyy::object value = boost::fusion::back( attr );
					obj = cyy::factory(index, value);
				} 
				static void fail(cyy::object&) 
				{}
			}; 
			
			template <> 
			struct transform_attribute< cyy::object, cyy::tuple_t, boost::spirit::qi::domain > 
			{ 
				typedef cyy::tuple_t type; 
				static cyy::tuple_t pre(cyy::object& v) 
				{ 
					return cyy::tuple_t(); 
				} 
				static void post(cyy::object& val, cyy::tuple_t const& attr) 
				{
					val = cyy::factory(attr);
				} 
				static void fail(cyy::object&) 
				{}
			}; 

		}
	}
}

namespace cyy	
{
	namespace io	
	{	
		namespace
		{

			//	Convert a vector of objects into a attribute map
			struct cast_to_amap_impl
			{
				template < typename A >
				struct result { typedef object type; };

				object operator()(cyy::vector_t vec) const
				{
					attr_map_t amap;
					attr_t def;
					for (auto obj : vec)
					{
						attr_t const& attr = value_cast(obj, def);
//						std::cout << attr.first << " := " << cyy::to_literal(attr.second) << std::endl;
						amap[attr.first] = attr.second;
					}
					return factory(amap);
				}
			};

			struct cast_to_pmap_impl
			{
				template < typename A >
				struct result { typedef object type; };

				object operator()(cyy::vector_t vec) const
				{
					param_map_t pmap;
					param_t def;
					for (auto obj : vec)
					{
						param_t const& param = value_cast(obj, def);
//						std::cout << param.first << " := " << cyy::to_literal(param.second) << std::endl;
						pmap[param.first] = param.second;
					}
					return factory(pmap);
				}
			};
		}	//	namespace *anonymous*
	
	template <typename Iterator>
		object_parser< Iterator > :: object_parser()
			: object_parser::base_type( r_object )
		{
			boost::phoenix::function<cast_to_amap_impl>	cast_to_amap;
			boost::phoenix::function<cast_to_pmap_impl>	cast_to_pmap;

			r_start
			= r_object[boost::spirit::_val = boost::spirit::_1]
			;
			
			r_object 
			%=  boost::spirit::qi::lit("null")[ boost::spirit::_val = factory() ]
			| r_op
			| r_std
			| r_chrono
			| r_numeric //	must be placed after r_chrono because of int_ default
			| r_version
			| r_uuid
			| r_path
			| r_ip
			| r_ep
			| r_string
			| r_buffer
			| r_digest
			| r_color	//	starts with #
			| r_set
			| ('{' > (r_tuple | boost::spirit::eps[boost::spirit::_val = tuple_factory()]) > '}')	//	automatic conversion 
			| ('%' > r_map)
			;
			
			//
			//	customization point required
			//
			r_set
			= r_param
			| r_attr
			;

			//
			//	r_param synthesizes an attribute 
			//	of type boost::fusion::vector< std::string, object>
			//
			r_param
			= '(' 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> r_literal 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> ':' 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> r_object 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> ')'
			;
			
			//
			//	r_attr synthesizes an attribute 
			//	of type boost::fusion::vector< std::size_t, object>
			//
			r_attr
			= '(' 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> r_size 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> ':' 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> r_object 
			>> boost::spirit::omit[*boost::spirit::ascii::blank]
			>> ')'
			;
			
			r_tuple
			%= r_object % (',' >> boost::spirit::omit[*boost::spirit::ascii::blank])
			;

			r_map
			= ('(' >> (r_pmap)[boost::spirit::_val = cast_to_pmap(boost::spirit::_1)] >> ')')
			| ('[' >> (r_amap)[boost::spirit::_val = cast_to_amap(boost::spirit::_1)] >> ']')
			| boost::spirit::qi::lit("[]")[boost::spirit::_val = attr_map_factory()]
			| boost::spirit::qi::lit("()")[boost::spirit::_val = param_map_factory()]
			;

			r_amap
			%= r_attr % (',' >> boost::spirit::omit[*boost::spirit::ascii::blank])
			;
			
			r_pmap
			%= r_param % (',' >> boost::spirit::omit[*boost::spirit::ascii::blank])
			;

			r_object.name("object_parser");
			r_numeric.name("numeric");
			r_string.name("string");
			//debug(r_start);			
		}
	}	//	io
} 	//	cyy

#endif	// CYY_IO_PARSER_OBJECT_HPP


