/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 *
 */
#ifndef CYY_IO_PARSER_TOKEN_HPP
#define CYY_IO_PARSER_TOKEN_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
  #pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/parser/token_parser.h>
#include <cyy/intrinsics/factory/string_factory.h>

#include <boost/spirit/home/support/attributes.hpp>	//	transform_attribute
#include <boost/spirit/include/phoenix.hpp>	//	enable assignment of values like cyy::object
	
namespace boost		
{ 
	namespace spirit	
	{ 
		namespace traits	
		{ 
	
			template <> 
			struct transform_attribute< cyy::object, std::string, boost::spirit::qi::domain > 
			{ 
				typedef std::string type; 
				static type pre(cyy::object& v) 
				{ 
					return type(); 
				} 
				static void post(cyy::object& val, type const& attr) 
				{
					val = cyy::factory(attr);
				} 
				static void fail(cyy::object&) 
				{}
			}; 
			
		}	//	traits
	}	//	spirit
}	//	boost

namespace cyy	
{
	namespace io	
	{
		template <typename Iterator>
		literal_parser< Iterator >::literal_parser()
			: literal_parser::base_type(r_start)
		{
			r_start
				%= *(boost::spirit::qi::alnum | boost::spirit::qi::ascii::char_("_."))
				;
		}
		
		template <typename Iterator>
		lit_obj_parser< Iterator >::lit_obj_parser()
			: lit_obj_parser::base_type(r_start)
		{
			r_start
				= r_literal	//	implicit conversion
				;
		}
		
		template <typename Iterator>
		identifier_parser< Iterator >::identifier_parser()
			: identifier_parser::base_type(r_start)
		{
            //	lexeme directive ist required not to match
            //	the tokens after the whitespace
            //
            r_start
                %= boost::spirit::qi::lexeme[+r_first >> *r_other]
                ;
            
            r_first
                %= boost::spirit::qi::alpha
                | boost::spirit::qi::ascii::char_("_")
                ;

            r_other
                %= boost::spirit::qi::alnum
                | boost::spirit::qi::ascii::char_("_.")
                ;
        }

		template <typename Iterator>
		directive_parser< Iterator >::directive_parser()
			: directive_parser::base_type(r_start)
		{
			//	lexeme directive ist required not to match
			//	the tokens after the whitespace
			//
			r_start
				%= boost::spirit::qi::lexeme[+r_first >> *r_other]
				;

			r_first
				%= boost::spirit::qi::alpha
				| boost::spirit::qi::ascii::char_("_")
				;

			r_other
				%= boost::spirit::qi::alnum
				| boost::spirit::qi::ascii::char_("_.")
				| boost::spirit::qi::ascii::char_('-')
				;
		}

		template <typename Iterator>
		token_parser< Iterator >::token_parser()
			: token_parser::base_type(r_start)
		{

			//
			//	r_literal will be converted from string to object automatically
			//
			r_start
				%= (r_obj | r_literal) % boost::spirit::omit[+boost::spirit::ascii::blank]
				;
				
			r_literal
				%= *(boost::spirit::qi::alnum | boost::spirit::qi::ascii::char_("_."))
				;

		}
		
	}	//	io
} 	//	cyy

#endif	// CYY_IO_PARSER_TOKEN_HPP


