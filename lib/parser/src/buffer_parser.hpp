/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 *
 */
#ifndef CYY_IO_PARSER_BUFFER_HPP
#define CYY_IO_PARSER_BUFFER_HPP

#if defined(_MSC_VER) && (_MSC_VER >= 1200)
  #pragma once
#endif // defined(_MSC_VER) && (_MSC_VER >= 1200)

#include <cyy/parser/buffer_parser.h>
#include <cyy/intrinsics/factory/buffer_factory.h>

#include <boost/spirit/home/support/attributes.hpp>	//	transform_attribute
#include <boost/spirit/include/phoenix.hpp>	//	enable assignment of values like cyy::object
	
namespace cyy	{
	namespace io	{

		template <typename Iterator>
		hex_string< Iterator >::hex_string()
			: hex_string::base_type(r_start)
		{
			r_start
				%= (*r_hex2)
				;
		};

		template <typename Iterator>
		buffer< Iterator > :: buffer()
			: buffer::base_type( r_start )
		{
			r_start	
			%= *(boost::spirit::qi::alnum | boost::spirit::qi::space | "\\x" >> r_hex2)
			;
		};
		
		template <typename Iterator>
		object buffer_parser< Iterator > :: helper(cyy::buffer_t const& buffer)
		{
			return cyy::factory(buffer);
		}
		
		template <typename Iterator>
		buffer_parser< Iterator > :: buffer_parser()
			: buffer_parser::base_type( r_start )
		{
			/**
			* buffers are surrounded by single quotation marks and contain any symbol
			* or an escape sequence
			*/
			r_start
			= ('\'' >> r_buffer  >> '\'')[boost::spirit::_val = boost::phoenix::bind(&buffer_parser<Iterator>::helper, this, boost::spirit::_1)]
//			= ('\'' >> r_buffer  >> '\'')[boost::spirit::_val = boost::phoenix::bind(&factory, boost::spirit::_1)]
			;
		}

		template <typename Iterator>
		log_parser< Iterator > ::log_parser()
			: log_parser::base_type(r_buffer)
		{
			//
			//	boost::spirit::qi::hex => unsigned int
			//
			r_buffer
				%= r_hex2 % (+boost::spirit::qi::space)
//				%= boost::spirit::qi::hex % (+boost::spirit::qi::space)
				;
		}

	}	//	io
} 	//	cyy

#endif	// CYY_IO_PARSER_BUFFER_HPP


