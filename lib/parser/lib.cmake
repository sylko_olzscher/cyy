# top level files
set (parser_lib)

set (parser_cpp


    lib/parser/src/numeric_parser.cpp
    lib/parser/src/object_parser.cpp
    lib/parser/src/string_parser.cpp
    lib/parser/src/chrono_parser.cpp
    lib/parser/src/version_parser.cpp
    lib/parser/src/buffer_parser.cpp
    lib/parser/src/op_parser.cpp
    lib/parser/src/cyystd_parser.cpp
    lib/parser/src/boost_parser.cpp
    lib/parser/src/color_parser.cpp
    lib/parser/src/mac_parser.cpp
    lib/parser/src/digest_parser.cpp
    lib/parser/src/token_parser.cpp
    lib/parser/src/type_parser.cpp
    lib/parser/src/level_parser.cpp

	lib/parser/src/numeric_parser.hpp
	lib/parser/src/object_parser.hpp
	lib/parser/src/string_parser.hpp
	lib/parser/src/chrono_parser.hpp
	lib/parser/src/version_parser.hpp
	lib/parser/src/buffer_parser.hpp
	lib/parser/src/cyystd_parser.hpp
	lib/parser/src/boost_parser.hpp
	lib/parser/src/color_parser.hpp
	lib/parser/src/op_parser.hpp
	lib/parser/src/mac_parser.hpp
	lib/parser/src/digest_parser.hpp
	lib/parser/src/token_parser.hpp
	lib/parser/src/type_parser.hpp
	lib/parser/src/level_parser.hpp
    
)

set (parser_h

	src/main/include/cyy/parser/numeric_parser.h
	src/main/include/cyy/parser/object_parser.h
	src/main/include/cyy/parser/string_parser.h
	src/main/include/cyy/parser/chrono_parser.h
	src/main/include/cyy/parser/version_parser.h
	src/main/include/cyy/parser/buffer_parser.h
	src/main/include/cyy/parser/op_parser.h
	src/main/include/cyy/parser/cyystd_parser.h
	src/main/include/cyy/parser/boost_parser.h
	src/main/include/cyy/parser/color_parser.h
	src/main/include/cyy/parser/mac_parser.h
	src/main/include/cyy/parser/digest_parser.h
	src/main/include/cyy/parser/token_parser.h
	src/main/include/cyy/parser/type_parser.h
	src/main/include/cyy/parser/level_parser.h
	
    #
    #	support for the boost spirit parser library
    #
	src/main/include/cyy/parser/spirit_support.hpp
	
)

# set (json_parser
#     lib/parser/src/json_parser.cpp
# 	lib/parser/src/json_parser.hpp
# 	src/main/include/cyy/parser/json_parser.h
# 	
#     lib/parser/src/json/json_io.cpp
#     lib/parser/src/json/json_io.ipp
# 	src/main/include/cyy/parser/json/json_io.h    
# 	
# )

# source_group("JSON" FILES ${json_parser})

# define the core lib
set (parser_lib
  ${parser_cpp}
  ${parser_h}
#   ${json_parser}
)
