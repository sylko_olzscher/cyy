# top level files
set (tool_lib)

set (tool_cpp

	tools/shared/src/console.cpp
	
)

set (tool_h

    src/main/include/cyy/util/shared/console.h
)

# define the tool lib
set (tool_lib
  ${tool_cpp}
  ${tool_h}
)