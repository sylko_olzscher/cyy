/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <boost/config.hpp>
#include <cyy/util/demangle.h>

#if !defined(BOOST_WINDOWS)
#include <cxxabi.h>
#endif
#include <memory>
#include <cstddef>

namespace cyy 
{	
	std::string demangle( const char* mangled_name )
	{
#if defined(BOOST_WINDOWS)	
		//	use the undname tool installed with visual studio
		return mangled_name;
#else		
		std::size_t len = 0 ;
		int status = 0 ;
		std::unique_ptr< char, decltype(&std::free) > ptr(__cxxabiv1::__cxa_demangle( mangled_name, nullptr, &len, &status ), &std::free ) ;
		return ptr.get() ;		
#endif
	}
}