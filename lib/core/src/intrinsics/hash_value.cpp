/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#include <cyy/intrinsics/hash_value.h>
#include <cyy/intrinsics/hash/std_hash.h>
#include <cyy/intrinsics/type_codes.h>
#include <cyy/object_cast.hpp>

namespace cyy 
{
	namespace details 
	{
		template <types::code C>
		std::size_t hash_value(object const& obj)
		{
			typedef typename reverse_type < C >::type	type;
			const type* ptr = object_cast<type>(obj);
			
			//	create hash object
			std::hash< type >	ho;
			
			return (ptr != nullptr)
			? ho(*ptr)
			: 0
			;
		}	
		
	}
	
	std::size_t hash_value(object const& obj)
	{
		const auto code = obj.code();
		switch (code.first)
		{
// 			CYY_NULL = 1,	//!<	null, void
// 			CYY_OBJECT, 	//!<	nested object - try to avoid this
// 			
// 			//	native C++ types
			case types::CYY_BOOL: 
				return details::hash_value< types::CYY_BOOL >(obj);
			case types::CYY_INT8:
				return details::hash_value< types::CYY_INT8 >(obj);
			case types::CYY_UINT8:
				return details::hash_value< types::CYY_UINT8 >(obj);
			case types::CYY_INT16:
				return details::hash_value< types::CYY_INT16 >(obj);
			case types::CYY_UINT16:
				return details::hash_value< types::CYY_UINT16 >(obj);
			case types::CYY_INT32:
				return details::hash_value< types::CYY_INT32 >(obj);
			case types::CYY_UINT32:
				return details::hash_value< types::CYY_UINT32 >(obj);
			case types::CYY_INT64:
				return details::hash_value< types::CYY_INT64 >(obj);
			case types::CYY_UINT64:
				return details::hash_value< types::CYY_UINT64 >(obj);
// 			
			case types::CYY_FLOAT32:
				return details::hash_value< types::CYY_FLOAT32 >(obj);
			case types::CYY_FLOAT64:
				return details::hash_value< types::CYY_FLOAT64 >(obj);
			case types::CYY_FLOAT80:
				return details::hash_value< types::CYY_FLOAT80 >(obj);
// 			
			case types::CYY_COMPLEX32:
				return details::hash_value< types::CYY_COMPLEX32 >(obj);
			case types::CYY_COMPLEX64:
				return details::hash_value< types::CYY_COMPLEX64 >(obj);
			case types::CYY_COMPLEX80:
				return details::hash_value< types::CYY_COMPLEX80 >(obj);
// 
// 			//	standard library
			case types::CYY_STRING:
				return details::hash_value< types::CYY_STRING >(obj);
// 			//	note ther is no support for std::wstring anymore
			case types::CYY_TIME_STAMP:
				return details::hash_value< types::CYY_TIME_STAMP >(obj);
			case types::CYY_NANOSECONDS:
				return details::hash_value< types::CYY_NANOSECONDS >(obj);
			case types::CYY_MICROSECONDS:
				return details::hash_value< types::CYY_MICROSECONDS >(obj);
			case types::CYY_MILLISECONDS:
				return details::hash_value< types::CYY_MILLISECONDS >(obj);
			case types::CYY_SECONDS:
				return details::hash_value< types::CYY_SECONDS >(obj);
			case types::CYY_MINUTES:
				return details::hash_value< types::CYY_MINUTES >(obj);
			case types::CYY_HOURS:
				return details::hash_value< types::CYY_HOURS >(obj);
			case types::CYY_DAYS:
				return details::hash_value< types::CYY_DAYS >(obj);
// 			
// 			//	data types defined in cyy library
			case types::CYY_VERSION:
				return details::hash_value< types::CYY_VERSION >(obj);
			case types::CYY_REVISION:
				return details::hash_value< types::CYY_REVISION >(obj);
// 			CYY_OP,			//!<	VM operation
			case types::CYY_BUFFER:
				return details::hash_value< types::CYY_BUFFER >(obj);
			case types::CYY_INDEX:
				return details::hash_value< types::CYY_INDEX >(obj);
			case types::CYY_DIFF:
				return details::hash_value< types::CYY_DIFF >(obj);
			case types::CYY_RGB_COLOR8:
				return details::hash_value< types::CYY_RGB_COLOR8 >(obj);
			case types::CYY_RGB_COLOR16:
				return details::hash_value< types::CYY_RGB_COLOR16 >(obj);
			case types::CYY_MAC48:
				return details::hash_value< types::CYY_MAC48 >(obj);
// 			
// 			CYY_ATTR_MAP,	//!<	std::map< std::size_t, object >
// 			CYY_PARAM_MAP,	//!<	std::map< std::string, object >
// 			CYY_ATTRIBUTE,	//!<	std::pair<std::size_t, object>
// 			CYY_PARAMETER,	//!<	std::pair<std::string, object>
			case types::CYY_TUPLE:
				return details::hash_value< types::CYY_TUPLE >(obj);
			case types::CYY_VECTOR:
				return details::hash_value< types::CYY_VECTOR >(obj);
// 			CYY_SET,		//!<	std::set<object>
// 			
// 			//	datatypes from boost library
			case types::CYY_BOOST_UUID:
				return details::hash_value< types::CYY_BOOST_UUID >(obj);
			case types::CYY_BOOST_PATH:
				return details::hash_value< types::CYY_BOOST_PATH >(obj);
// 			CYY_BOOST_ERROR,	//!<	boost::system::error_code
// 			CYY_BOOST_IP_ADDRESS,	//!<	boost::asio::ip::address
// 			CYY_BOOST_TCP_ENDPOINT,	//!<	boost::asio::ip::tcp::endpoint
			case types::CYY_BOOST_TRIBOOL:
				return details::hash_value< types::CYY_BOOST_TRIBOOL >(obj);
// 			
// 			//	complex data types
// 			CYY_ARRAY,
			
			default:
				break;
		}
		return 0;
	}
	
	std::size_t hash_value(tuple_t const& c)
	{
		return 0;
	}

	std::size_t hash_value(vector_t const& c)
	{
		return 0;
	}
	
	std::size_t hash_value(set_t const& c)
	{
		return 0;
	}
	
}


