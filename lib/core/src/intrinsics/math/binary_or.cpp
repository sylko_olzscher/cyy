/*
* Copyright Sylko Olzscher 2016
*
* Use, modification, and distribution is subject to the Boost Software
* License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
* http://www.boost.org/LICENSE_1_0.txt)
*/

#include <cyy/intrinsics/math/add.h>
#include <cyy/intrinsics/factory.h>	
#include <cyy/intrinsics/type_traits.h>
#include <cyy/object_cast.hpp>
#include <cyy/intrinsics/compare_types.h>

namespace cyy 
{
	namespace details 
	{
		template <types::code CLHS, types::code CRHS = CLHS>
		object binary_or(object const& lhs, object const& rhs)
		{
			typedef typename reverse_type < CLHS >::type	lhs_type;
			typedef typename reverse_type < CRHS >::type	rhs_type;
			const lhs_type* lh_ptr = object_cast<lhs_type>(lhs);
			const rhs_type* rh_ptr = object_cast<rhs_type>(rhs);
			return (lh_ptr != nullptr && rh_ptr != nullptr)
			? factory(*lh_ptr | *rh_ptr)
			: object()
			;
		}
		
	}
	
	object binary_or(object const& lhs, object const& rhs)
	{
		if (is_same_type(lhs, rhs))
		{
			//	same type
			const auto code = cyy::get_code(lhs);
			switch (code)
			{
				case types::CYY_BOOL:
					break;
				case types::CYY_INT8:
					return details::binary_or< types::CYY_INT8 >(lhs, rhs);
				case types::CYY_UINT8:
					return details::binary_or< types::CYY_UINT8 >(lhs, rhs);
				case types::CYY_INT16:
					return details::binary_or< types::CYY_INT16 >(lhs, rhs);
				case types::CYY_UINT16:
					return details::binary_or< types::CYY_UINT16 >(lhs, rhs);
				case types::CYY_INT32:
					return details::binary_or< types::CYY_INT32 >(lhs, rhs);
				case types::CYY_UINT32:
					return details::binary_or< types::CYY_UINT32 >(lhs, rhs);
				case types::CYY_INT64:
					return details::binary_or< types::CYY_INT64 >(lhs, rhs);
				case types::CYY_UINT64:
					return details::binary_or< types::CYY_UINT64 >(lhs, rhs);
					
				default:
					break;
			}
		}
		else
		{
			//	different types
		}
		return factory();
	}
	
	object operator|(object const& lhs, object const& rhs)
	{
		return binary_or(lhs, rhs);
	}
	
}


