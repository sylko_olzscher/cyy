/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#include <cyy/intrinsics/compare.h>
#include <cyy/intrinsics/policy.h>
#include <cyy/intrinsics/type_traits.h>
#include <cyy/object_cast.hpp>
#include <cyy/intrinsics/compare_types.h>

namespace cyy 
{
	namespace details 
	{

		
		template <types::code C>
		bool cmp(object const& lhs, object const& rhs)
		{
			typedef typename reverse_type < C >::type	type;
			const type* lh_ptr = object_cast<type>(lhs);
			const type* rh_ptr = object_cast<type>(rhs);
			
			//	create function object
			return equality_operator<type>()(*lh_ptr, *rh_ptr);
		}	
		
		template <types::code C>
		bool less(object const& lhs, object const& rhs)
		{
			typedef typename reverse_type < C >::type	type;
			const type* lh_ptr = object_cast<type>(lhs);
			const type* rh_ptr = object_cast<type>(rhs);
			
			//	create function object
			return less_operator<type>()(*lh_ptr, *rh_ptr);
		}			
	}	//	details
	
	
	
	bool is_equal(object const& lhs, object const& rhs, proto_compare cb)
	{
		//	return false if type is not comparable
		if (!is_comparable(lhs) || !is_comparable(rhs))	return false;

		if (is_same_type(lhs, rhs))
		{
			//	switch case to get the bridge from
			//	runtime data to compiled meta data
			//BOOST_ASSERT(lhs.code() == rhs.code());
			const auto code = lhs.code();

			switch (code.first)
			{
			case types::CYY_NULL:
				//	always false
				break;
			case types::CYY_EOD:
				//	always true
				return true;

				//	native C++ types
			case types::CYY_BOOL:
				return details::cmp< types::CYY_BOOL >(lhs, rhs);
			case types::CYY_INT8:
				return details::cmp< types::CYY_INT8 >(lhs, rhs);
			case types::CYY_UINT8:
				return details::cmp< types::CYY_UINT8 >(lhs, rhs);
			case types::CYY_INT16:
				return details::cmp< types::CYY_INT16 >(lhs, rhs);
			case types::CYY_UINT16:
				return details::cmp< types::CYY_UINT16 >(lhs, rhs);
			case types::CYY_INT32:
				return details::cmp< types::CYY_INT32 >(lhs, rhs);
			case types::CYY_UINT32:
				return details::cmp< types::CYY_UINT32 >(lhs, rhs);
			case types::CYY_INT64:
				return details::cmp< types::CYY_INT64 >(lhs, rhs);
			case types::CYY_UINT64:
				return details::cmp< types::CYY_UINT64 >(lhs, rhs);
//				
			case types::CYY_FLOAT32:
				return details::cmp< types::CYY_FLOAT32 >(lhs, rhs);
			case types::CYY_FLOAT64:
				return details::cmp< types::CYY_FLOAT64 >(lhs, rhs);
			case types::CYY_FLOAT80:
				return details::cmp< types::CYY_FLOAT80 >(lhs, rhs);
//				
			case types::CYY_COMPLEX32:
				return details::cmp< types::CYY_COMPLEX32 >(lhs, rhs);
			case types::CYY_COMPLEX64:
				return details::cmp< types::CYY_COMPLEX64 >(lhs, rhs);
			case types::CYY_COMPLEX80:
				return details::cmp< types::CYY_COMPLEX80 >(lhs, rhs);
// 
//				//	standard library
			case types::CYY_STRING:
				return details::cmp< types::CYY_STRING >(lhs, rhs);
				
			case types::CYY_TIME_STAMP:
				return details::cmp< types::CYY_TIME_STAMP >(lhs, rhs);
			case types::CYY_NANOSECONDS:
				return details::cmp< types::CYY_NANOSECONDS >(lhs, rhs);
			case types::CYY_MICROSECONDS:
				return details::cmp< types::CYY_MICROSECONDS >(lhs, rhs);
			case types::CYY_MILLISECONDS:
				return details::cmp< types::CYY_MILLISECONDS >(lhs, rhs);
			case types::CYY_SECONDS:
				return details::cmp< types::CYY_SECONDS >(lhs, rhs);
			case types::CYY_MINUTES:
				return details::cmp< types::CYY_MINUTES >(lhs, rhs);
			case types::CYY_HOURS:
				return details::cmp< types::CYY_HOURS >(lhs, rhs);
			case types::CYY_DAYS:
				return details::cmp< types::CYY_DAYS >(lhs, rhs);
//			
//			//	data types defined in cyy library
			case types::CYY_VERSION:
				return details::cmp< types::CYY_VERSION >(lhs, rhs);
			case types::CYY_REVISION:
				return details::cmp< types::CYY_REVISION >(lhs, rhs);
			case types::CYY_OP:
				return details::cmp< types::CYY_OP >(lhs, rhs);
			case types::CYY_BUFFER:
				break;
			case types::CYY_INDEX:
				return details::cmp< types::CYY_INDEX >(lhs, rhs);
			case types::CYY_DIFF:
				return details::cmp< types::CYY_DIFF >(lhs, rhs);
			case types::CYY_RGB_COLOR8:
				return details::cmp< types::CYY_RGB_COLOR8 >(lhs, rhs);
			case types::CYY_RGB_COLOR16:
				return details::cmp< types::CYY_RGB_COLOR16 >(lhs, rhs);
			case types::CYY_MAC48:
				return details::cmp< types::CYY_MAC48 >(lhs, rhs);
//			
//			CYY_ATTR_MAP,	//!<	std::map< std::size_t, object >
//			CYY_PARAM_MAP,	//!<	std::map< std::string, object >
			case types::CYY_ATTRIBUTE:
				return details::cmp< types::CYY_ATTRIBUTE >(lhs, rhs);
			case types::CYY_PARAMETER:
				return details::cmp< types::CYY_PARAMETER >(lhs, rhs);
//			CYY_TUPLE,		//!<	std::list<object>
//			CYY_VECTOR,		//!<	std::vector<object>
//			CYY_SET,		//!<	std::set<object>
			case types::CYY_LOCKABLE:
				return details::cmp< types::CYY_LOCKABLE >(lhs, rhs);
//			
//			//	datatypes from boost library
			case types::CYY_BOOST_UUID:
				return details::cmp< types::CYY_BOOST_UUID >(lhs, rhs);
			case types::CYY_BOOST_PATH:
				return details::cmp< types::CYY_BOOST_PATH >(lhs, rhs);
			case types::CYY_BOOST_ERROR:
				break;
			case types::CYY_BOOST_IP_ADDRESS:
				return details::cmp< types::CYY_BOOST_IP_ADDRESS >(lhs, rhs);
			case types::CYY_BOOST_TCP_ENDPOINT:
				return details::cmp< types::CYY_BOOST_TCP_ENDPOINT >(lhs, rhs);
			case types::CYY_BOOST_TRIBOOL:
				return details::cmp< types::CYY_BOOST_TRIBOOL >(lhs, rhs);
				
			default:
				break;
			}
		}
		return (cb)
		? cb(lhs, rhs)
		: is_same_type(lhs, rhs)
		;
	}
	
	bool is_less(object const& lhs, object const& rhs, proto_compare cb)
	{
		//	return false if type is not sortable
		if (!is_comparable(lhs) || !is_comparable(rhs))	return false;
		
		if (is_same_type(lhs, rhs))
		{
//			BOOST_ASSERT(lhs.code() == rhs.code());
			const auto code = lhs.code();		

			switch (code.first)
			{
			case types::CYY_NULL:
				//	always false
				break;

				//	native C++ types
			case types::CYY_BOOL:
				return details::less< types::CYY_BOOL >(lhs, rhs);
			case types::CYY_INT8:
				return details::less< types::CYY_INT8 >(lhs, rhs);
			case types::CYY_UINT8:
				return details::less< types::CYY_UINT8 >(lhs, rhs);
			case types::CYY_INT16:
				return details::less< types::CYY_INT16 >(lhs, rhs);
			case types::CYY_UINT16:
				return details::less< types::CYY_UINT16 >(lhs, rhs);
			case types::CYY_INT32:
				return details::less< types::CYY_INT32 >(lhs, rhs);
			case types::CYY_UINT32:
				return details::less< types::CYY_UINT32 >(lhs, rhs);
			case types::CYY_INT64:
				return details::less< types::CYY_INT64 >(lhs, rhs);
			case types::CYY_UINT64:
				return details::less< types::CYY_UINT64 >(lhs, rhs);
//				
			case types::CYY_FLOAT32:
				return details::less< types::CYY_FLOAT32 >(lhs, rhs);
			case types::CYY_FLOAT64:
				return details::less< types::CYY_FLOAT64 >(lhs, rhs);
			case types::CYY_FLOAT80:
				return details::less< types::CYY_FLOAT80 >(lhs, rhs);
//				
			case types::CYY_COMPLEX32:
				return details::less< types::CYY_COMPLEX32 >(lhs, rhs);
			case types::CYY_COMPLEX64:
				return details::less< types::CYY_COMPLEX64 >(lhs, rhs);
			case types::CYY_COMPLEX80:
				return details::less< types::CYY_COMPLEX80 >(lhs, rhs);
// 
//				//	standard library
			case types::CYY_STRING:
				return details::less< types::CYY_STRING >(lhs, rhs);
				
			case types::CYY_TIME_STAMP:
				return details::less< types::CYY_TIME_STAMP >(lhs, rhs);
			case types::CYY_NANOSECONDS:
				return details::less< types::CYY_NANOSECONDS >(lhs, rhs);
			case types::CYY_MICROSECONDS:
				return details::less< types::CYY_MICROSECONDS >(lhs, rhs);
			case types::CYY_MILLISECONDS:
				return details::less< types::CYY_MILLISECONDS >(lhs, rhs);
			case types::CYY_SECONDS:
				return details::less< types::CYY_SECONDS >(lhs, rhs);
			case types::CYY_MINUTES:
				return details::less< types::CYY_MINUTES >(lhs, rhs);
			case types::CYY_HOURS:
				return details::less< types::CYY_HOURS >(lhs, rhs);
			case types::CYY_DAYS:
				return details::less< types::CYY_DAYS >(lhs, rhs);
//			
//			//	data types defined in cyy library
			case types::CYY_VERSION:
				return details::less< types::CYY_VERSION >(lhs, rhs);
			case types::CYY_REVISION:
				return details::less< types::CYY_REVISION >(lhs, rhs);
			case types::CYY_OP:	break;
			case types::CYY_BUFFER:	break;
			case types::CYY_INDEX:
				return details::less< types::CYY_INDEX >(lhs, rhs);
			case types::CYY_DIFF:
				return details::less< types::CYY_DIFF >(lhs, rhs);
			case types::CYY_RGB_COLOR8:
				return details::less< types::CYY_RGB_COLOR8 >(lhs, rhs);
			case types::CYY_RGB_COLOR16:
				return details::less< types::CYY_RGB_COLOR16 >(lhs, rhs);
			case types::CYY_MAC48:
				return details::less< types::CYY_MAC48 >(lhs, rhs);
//			
//			CYY_ATTR_MAP,	//!<	std::map< std::size_t, object >
//			CYY_PARAM_MAP,	//!<	std::map< std::string, object >
			case types::CYY_ATTRIBUTE:
				return details::less< types::CYY_ATTRIBUTE >(lhs, rhs);
			case types::CYY_PARAMETER:
				return details::less< types::CYY_PARAMETER >(lhs, rhs);
//			CYY_TUPLE,		//!<	std::list<object>
//			CYY_VECTOR,		//!<	std::vector<object>
//			CYY_SET,		//!<	std::set<object>
			case types::CYY_LOCKABLE:
				return details::less< types::CYY_LOCKABLE >(lhs, rhs);
//			
//			//	datatypes from boost library
			case types::CYY_BOOST_UUID:
				return details::less< types::CYY_BOOST_UUID >(lhs, rhs);
			case types::CYY_BOOST_PATH:
				return details::less< types::CYY_BOOST_PATH >(lhs, rhs);
			case types::CYY_BOOST_ERROR:	break;
			case types::CYY_BOOST_IP_ADDRESS:
				return details::less< types::CYY_BOOST_IP_ADDRESS >(lhs, rhs);
			case types::CYY_BOOST_TCP_ENDPOINT:
				return details::less< types::CYY_BOOST_TCP_ENDPOINT >(lhs, rhs);
			case types::CYY_BOOST_TRIBOOL:	break;
				
			default:
				break;
			}
			
		}
		return (cb)
		? cb(lhs, rhs)
		: is_less_type(lhs, rhs)
		;
	}
	
	
	bool operator==(object const& lhs, object const& rhs)
	{
		return is_equal(lhs, rhs);
	}
	
	bool operator< (object const& lhs, object const& rhs)
	{
		return is_less(lhs, rhs);
	}
	
	bool operator!= (object const& lhs, object const& rhs)
	{
		return !(is_equal( lhs, rhs ));
	}
	
	bool operator> (object const& lhs, object const& rhs)
	{
		return is_less(rhs, lhs);
	}
	
	bool operator<= (object const& lhs, object const& rhs)
	{
		return !(is_less( rhs, lhs ));
	}
	
	bool operator>= (object const& lhs, object const& rhs)
	{
		return !(is_less( lhs, rhs ));
	}
	
	bool is_comparable(object const& obj)
	{
		BOOST_ASSERT_MSG(obj.value_, "object not initialized");
		return obj.value_->is_comparable();
	}

	bool is_sortable(object const& obj)
	{
		BOOST_ASSERT_MSG(obj.value_, "object not initialized");
		return obj.value_->is_sortable();
	}
	
}	//	cyy

namespace std 
{
	bool equal_to<cyy::object>::operator()(cyy::object const& lhs, cyy::object const& rhs) const
	{
		return cyy::is_equal(lhs, rhs, cyy::adjust::proto_compare_eq);
	}
	bool less<cyy::object>::operator()(cyy::object const& lhs, cyy::object const& rhs) const
	{
		return cyy::is_less(lhs, rhs, cyy::adjust::proto_compare_lt);
	}
}