/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include <cyy/intrinsics/factory/generic_factory.hpp>

namespace cyy 
{	
	template <>
	object set_factory<const char*>(std::initializer_list<const char*> il)
	{
		set_t s;
		std::for_each(il.begin(), il.end(), [&s](std::string v){
			s.insert(factory(v));
		});
		return factory(s);
	}
	
	template <>
	object set_factory<object>(std::initializer_list<object> obj_list)
	{
		set_t s;
		std::for_each(obj_list.begin(), obj_list.end(), [&s](object const& obj){
			s.insert(obj);
		});
		return factory(s);
	}
	
	object param_map_factory()
	{
		return factory(param_map_t());
	}
	
	object attr_map_factory()
	{
		return factory(attr_map_t());
	}
	
	
}