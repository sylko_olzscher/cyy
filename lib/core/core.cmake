# top level files
set (core_lib)

set (core_cpp

    lib/core/src/object.cpp
    lib/core/src/object_tracker.cpp
    lib/core/src/object_exceptions.cpp
    
# intrinsics
# intrinsics factory
# chrono
	lib/core/src/chrono/chrono.cpp
	
# math
	
# intrinsics generals
	lib/core/src/intrinsics/type_traits.cpp
	lib/core/src/intrinsics/algorithm.cpp
	lib/core/src/intrinsics/compare.cpp
	lib/core/src/intrinsics/compare_types.cpp
	lib/core/src/intrinsics/size.cpp
	lib/core/src/intrinsics/hash_value.cpp
	
# intrinsics hash
	lib/core/src/intrinsics/hash/std_hash.cpp
	
# intrinsics policy	
# util
)

set (core_h

    src/main/include/cyy/cyy_export.h
    src/main/include/cyy/object.h
    src/main/include/cyy/object_interface_fwd.h
    src/main/include/cyy/object_cast.hpp
    src/main/include/cyy/object_tracker.h
    src/main/include/cyy/object_exceptions.h
    src/main/include/cyy/object_type.h
    src/main/include/cyy/value_cast.hpp
    src/main/include/cyy/numeric_cast.hpp
    
    src/main/include/cyy/detail/object_interface.h
    src/main/include/cyy/detail/value.hpp
    src/main/include/cyy/detail/make_value.hpp
    src/main/include/cyy/detail/clone_helper.hpp
    
# intrinsics
# intrinsics factory
# intrinsics traits
# intrinsics policy
# intrinsics generals
	src/main/include/cyy/intrinsics/algorithm.h
	src/main/include/cyy/intrinsics/compare.h
	src/main/include/cyy/intrinsics/compare_types.h
	src/main/include/cyy/intrinsics/size.h
# 	src/main/include/cyy/intrinsics/hash_value.h

# math

# intrinsics hash
 	src/main/include/cyy/intrinsics/hash/std_hash.h
# 	src/main/include/cyy/intrinsics/hash/hash.hpp
# 	src/main/include/cyy/intrinsics/hash/hash_append.hpp
# 	src/main/include/cyy/intrinsics/hash/fnv1a.h

# chrono
	src/main/include/cyy/chrono/chrono.h
	
# container
#	src/main/include/cyy/container/trie.hpp
	src/main/include/cyy/container/step.hpp
	src/main/include/cyy/container/fifo.hpp
	
# util
)

set (core_factory

	src/main/include/cyy/intrinsics/factory.h
	src/main/include/cyy/intrinsics/factory/null_factory.h
	src/main/include/cyy/intrinsics/factory/eod_factory.h
	src/main/include/cyy/intrinsics/factory/numeric_factory.h
	src/main/include/cyy/intrinsics/factory/boolean_factory.h
	src/main/include/cyy/intrinsics/factory/string_factory.h
	src/main/include/cyy/intrinsics/factory/chrono_factory.h
	src/main/include/cyy/intrinsics/factory/version_factory.h
	src/main/include/cyy/intrinsics/factory/set_factory.h
	src/main/include/cyy/intrinsics/factory/buffer_factory.h
	src/main/include/cyy/intrinsics/factory/op_factory.h
	src/main/include/cyy/intrinsics/factory/std_factory.h
	src/main/include/cyy/intrinsics/factory/boost_factory.h
	src/main/include/cyy/intrinsics/factory/complex_factory.h
	src/main/include/cyy/intrinsics/factory/constants_factory.hpp
	src/main/include/cyy/intrinsics/factory/color_factory.h
	src/main/include/cyy/intrinsics/factory/mac_factory.h
	src/main/include/cyy/intrinsics/factory/digest_factory.h
	src/main/include/cyy/intrinsics/factory/lockable_factory.hpp
	src/main/include/cyy/intrinsics/factory/generic_factory.hpp
	src/main/include/cyy/intrinsics/factory/bytes_factory.h

	lib/core/src/intrinsics/factory/null_factory.cpp
	lib/core/src/intrinsics/factory/eod_factory.cpp
	lib/core/src/intrinsics/factory/numeric_factory.cpp
	lib/core/src/intrinsics/factory/boolean_factory.cpp
	lib/core/src/intrinsics/factory/string_factory.cpp
	lib/core/src/intrinsics/factory/chrono_factory.cpp
	lib/core/src/intrinsics/factory/version_factory.cpp
	lib/core/src/intrinsics/factory/set_factory.cpp
	lib/core/src/intrinsics/factory/buffer_factory.cpp
	lib/core/src/intrinsics/factory/op_factory.cpp
	lib/core/src/intrinsics/factory/std_factory.cpp
	lib/core/src/intrinsics/factory/boost_factory.cpp
	lib/core/src/intrinsics/factory/complex_factory.cpp
	lib/core/src/intrinsics/factory/color_factory.cpp
	lib/core/src/intrinsics/factory/mac_factory.cpp
	lib/core/src/intrinsics/factory/digest_factory.cpp
	lib/core/src/intrinsics/factory/lockable_factory.cpp
	lib/core/src/intrinsics/factory/bytes_factory.cpp

	lib/core/src/intrinsics/factory/generic_factory.cpp
)

set (core_intrinsics
	src/main/include/cyy/intrinsics/null.h
	src/main/include/cyy/intrinsics/eod.h
	src/main/include/cyy/intrinsics/version.h
	src/main/include/cyy/intrinsics/op.h
	src/main/include/cyy/intrinsics/sets_fwd.h
	src/main/include/cyy/intrinsics/sets.hpp
	src/main/include/cyy/intrinsics/buffer.hpp
	src/main/include/cyy/intrinsics/cyystddef.h
	src/main/include/cyy/intrinsics/color.hpp
	src/main/include/cyy/intrinsics/color_codes.h
	src/main/include/cyy/intrinsics/mac.h
	src/main/include/cyy/intrinsics/lockable.h
	src/main/include/cyy/intrinsics/bytes.h
	src/main/include/cyy/intrinsics/digest.h
	src/main/include/cyy/intrinsics/assign.h

	lib/core/src/intrinsics/version.cpp
	lib/core/src/intrinsics/op.cpp
	lib/core/src/intrinsics/cyystddef.cpp
	lib/core/src/intrinsics/mac.cpp
	lib/core/src/intrinsics/assign.cpp
	lib/core/src/intrinsics/lockable.cpp
	lib/core/src/intrinsics/bytes.cpp
)

set (core_traits 
	src/main/include/cyy/intrinsics/type_codes.h
	src/main/include/cyy/intrinsics/type_traits.h
	src/main/include/cyy/intrinsics/traits/type_traits.hpp
	src/main/include/cyy/intrinsics/traits/type_traits_boost.hpp
	src/main/include/cyy/intrinsics/traits/type_traits_object.hpp
	src/main/include/cyy/intrinsics/traits/type_traits_sets.hpp
	src/main/include/cyy/intrinsics/traits/type_traits_lockable.hpp
)

set (core_policy 
	src/main/include/cyy/intrinsics/policy.h
	src/main/include/cyy/intrinsics/policy/compare_policy.hpp
	src/main/include/cyy/intrinsics/policy/compare_policy_set.h
	src/main/include/cyy/intrinsics/policy/compare_policy_lockable.h
	src/main/include/cyy/intrinsics/policy/compare_policy_boost.h
	
	src/main/include/cyy/intrinsics/sizeop.hpp
	src/main/include/cyy/intrinsics/policy/size_policy.hpp
	src/main/include/cyy/intrinsics/policy/size_policy_set.h
	src/main/include/cyy/intrinsics/policy/size_policy_boost.h

	lib/core/src/intrinsics/policy/compare_policy_lockable.cpp
	lib/core/src/intrinsics/policy/compare_policy_boost.cpp
	lib/core/src/intrinsics/policy/size_policy.cpp
	lib/core/src/intrinsics/policy/size_policy_boost.cpp
	lib/core/src/intrinsics/policy/size_policy_set.cpp

)

set (core_math
	lib/core/src/intrinsics/math/add.cpp
	lib/core/src/intrinsics/math/sub.cpp
	lib/core/src/intrinsics/math/mul.cpp
	lib/core/src/intrinsics/math/div.cpp
	lib/core/src/intrinsics/math/binary_or.cpp

	src/main/include/cyy/intrinsics/math.h
	src/main/include/cyy/intrinsics/math/add.h
	src/main/include/cyy/intrinsics/math/sub.h
	src/main/include/cyy/intrinsics/math/mul.h
	src/main/include/cyy/intrinsics/math/div.h
	src/main/include/cyy/intrinsics/math/binary_or.h
)

set (core_util

	src/main/include/cyy/util/slice.hpp
	src/main/include/cyy/util/brute_cast.hpp
	src/main/include/cyy/util/read_sets.h
	src/main/include/cyy/util/verify_file_extension.h
	src/main/include/cyy/util/tree_walker.h
	src/main/include/cyy/util/circular_counter.hpp
	src/main/include/cyy/util/byte_swap.h
	src/main/include/cyy/util/bit_fiddling.hpp
	src/main/include/cyy/util/alloc_stack.hpp
	src/main/include/cyy/util/meta.hpp
	src/main/include/cyy/util/demangle.h
	src/main/include/cyy/util/macro.h
	src/main/include/cyy/util/dom_manip.h
	src/main/include/cyy/util/activator.hpp

	lib/core/src/util/read_sets.cpp
	lib/core/src/util/verify_file_extension.cpp
	lib/core/src/util/demangle.cpp
	lib/core/src/util/tree_walker.cpp
	lib/core/src/util/dom_manip.cpp
)

source_group("factory" FILES ${core_factory})
source_group("intrinsics" FILES ${core_intrinsics})
source_group("traits" FILES ${core_traits})
source_group("policy" FILES ${core_policy})
source_group("math" FILES ${core_math})
source_group("util" FILES ${core_util})

# define the core lib
set (core_lib
  ${core_cpp}
  ${core_h}
  ${core_factory}
  ${core_intrinsics}
  ${core_traits}
  ${core_policy}
  ${core_math}
  ${core_util}
)