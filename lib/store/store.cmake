# top level files
set (store_lib)

set (store_cpp

	lib/store/src/store.cpp
	lib/store/src/meta.cpp
	lib/store/src/table.cpp
	lib/store/src/record.cpp
	lib/store/src/publisher.cpp
)

set (store_h

    src/main/include/cyy/store/access.hpp
    src/main/include/cyy/store/store.h
    src/main/include/cyy/store/key.hpp
    src/main/include/cyy/store/body.hpp
    src/main/include/cyy/store/meta.h
    src/main/include/cyy/store/table.h
    src/main/include/cyy/store/record.h
    src/main/include/cyy/store/publisher.h
)

set (store_intrinsics
	
    src/main/include/cyy/store/intrinsics/type_traits_store.hpp
    src/main/include/cyy/store/intrinsics/compare_policy_store.h
    src/main/include/cyy/store/intrinsics/store_factory.h

    lib/store/src/intrinsics/store_factory.cpp
	lib/store/src/intrinsics/compare_policy_store.cpp
)


source_group("intrinsics" FILES ${store_intrinsics})

# define the store lib
set (store_lib
  ${store_cpp}
  ${store_h}
  ${store_intrinsics}
)