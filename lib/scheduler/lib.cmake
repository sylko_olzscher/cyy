# top level files
set (scheduler_lib)

set (scheduler_cpp

	lib/scheduler/src/thread/pool.cpp
	lib/scheduler/src/thread/scheduler.cpp
	lib/scheduler/src/thread/registry.cpp
    
	lib/scheduler/src/broker.cpp
	lib/scheduler/src/ruler.cpp
	lib/scheduler/src/signal_handler.cpp
	
	lib/scheduler/src/logging/severity.cpp
)

set (scheduler_h

	src/main/include/cyy/scheduler/thread/pool.h
	src/main/include/cyy/scheduler/thread/scheduler.h
	src/main/include/cyy/scheduler/thread/sync.hpp
	
	src/main/include/cyy/scheduler/task_fwd.h
	src/main/include/cyy/scheduler/broker.h
	src/main/include/cyy/scheduler/ruler.h
	
	src/main/include/cyy/scheduler/task.hpp
	src/main/include/cyy/scheduler/factory.hpp
	src/main/include/cyy/scheduler/registry.h
	
	src/main/include/cyy/scheduler/signal_handler.h	
)

set (scheduler_tasks

#	lib/scheduler/src/tasks/shutdown.h
#	lib/scheduler/src/tasks/shutdown.cpp
	lib/scheduler/src/tasks/gc.h
	lib/scheduler/src/tasks/gc.cpp
)

set (scheduler_log
	src/main/include/cyy/scheduler/logging/log.h
	src/main/include/cyy/scheduler/logging/logger.hpp
	src/main/include/cyy/scheduler/logging/severity.h
	src/main/include/cyy/scheduler/logging/entry.hpp

)

source_group("tasks" FILES ${scheduler_tasks})
source_group("log" FILES ${scheduler_log})

# define the core lib
set (scheduler_lib
  ${scheduler_cpp}
  ${scheduler_h}
  ${scheduler_tasks}
  ${scheduler_log}
)
