/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */

#include <cyy/scheduler/factory.hpp>
#include <cyy/intrinsics/factory/set_factory.h>
#include "tasks/gc.h"
#include <stdexcept>


namespace cyy 
{

	ruler::ruler()
	: scheduler_()
		, next_tag_(NO_TASK)
		, tasks_()
		, mutex_()
		, shutdown_(false)
		, gc_(scheduler::factory(*this).start<gc>().second)
	{
		BOOST_ASSERT_MSG(scheduler_.is_running(), "scheduler not running");
	}
	
	ruler::ruler(std::size_t count)
	: scheduler_(count)
		, next_tag_(NO_TASK)
		, tasks_()
		, mutex_()
		, shutdown_(false)
		, gc_(scheduler::factory(*this).start<gc>().second)
	{
		BOOST_ASSERT_MSG(scheduler_.is_running(), "scheduler not running");	
	}

	ruler::~ruler()
	{
		BOOST_ASSERT_MSG(tasks_.empty(), "there are still running tasks" );
	}

	std::size_t ruler::next_tag()
	{
		return ++next_tag_;
	}
	
	bool ruler::insert(shared_task ptr)
	{
		return (scheduler_.is_running() && !shutdown_)
			? insert(ptr, thread::unique_lock<thread::shared_mutex>(mutex_))
			: false
			;
	}

	bool ruler::insert(shared_task ptr, thread::unique_lock<thread::shared_mutex>&& lk)
	{
		return tasks_.emplace(ptr->get_tag(), ptr).second;
	}
	
	thread::scheduler& ruler::get_scheduler()
	{
		return scheduler_;
	}

	bool ruler::stop()
	{
		BOOST_ASSERT_MSG(scheduler_.is_running(), "scheduler is stopped" );

		if (!shutdown_.load())
		{
			//	send termination signal
			return deliver(gc_, 1);
		}

		//
		//	shutdown already done or in progress
		//
		return false;
	}

	bool ruler::is_running(std::size_t id) const
	{
		if (!shutdown_)
		{
			//	lock task list
			thread::shared_lock<thread::shared_mutex> guard(mutex_);

			return tasks_.find(id) != tasks_.end();
		}
		return false;
	}


	bool ruler::stop(std::size_t id) const
	{
		BOOST_ASSERT_MSG(scheduler_.is_running(), "scheduler is stopped");
		return send(gc_, 0, cyy::index(id));
	}

	std::size_t ruler::size() const
	{
		//	lock task list
 		thread::shared_lock<thread::shared_mutex> guard(mutex_);
		return tasks_.size();
	}
	
	bool ruler::send(std::size_t id, std::size_t slot, tuple_t&& msg) const
	{
		if (!shutdown_)
		{
			//	lock task list
			thread::shared_lock<thread::shared_mutex> guard(mutex_);

			auto pos = tasks_.find(id);
// 			const bool b = pos != tasks_.end();
			if (pos != tasks_.end())
			{
				//	forward message
				auto ptr = pos->second;
				scheduler_.invoke([ptr, slot, msg]() {
					ptr->receive(slot, msg);
				});

				return true;
			}
#ifdef _DEBUG
			std::cerr
				<< "***warning: receiver "
				<< id
				<< " not found"
				<< std::endl;
#endif
		}
		return false;
	}
	
	bool ruler::send(std::size_t id, std::size_t slot) const
	{
		return send(id, slot, tuple_t());
	}

	bool ruler::deliver(std::size_t id, std::size_t slot, tuple_t&& msg) const
	{
		if (!shutdown_)
		{
			//
			//	This promise signals that the operation 
			//	is complete
			//
			std::promise<bool> result;
			auto f = result.get_future();

			{
				//	lock task list
				thread::shared_lock<thread::shared_mutex> guard(mutex_);

				auto pos = tasks_.find(id);
				const bool b = pos != tasks_.end();
				if (b)
				{
					//	forward message
					auto ptr = pos->second;
					scheduler_.invoke([ptr, slot, msg, &result]() {
						ptr->receive(slot, msg);
						result.set_value(true);
					});
				}
				else
				{
#ifdef _DEBUG
					std::cerr
						<< "***warning: receiver "
						<< id
						<< " not found"
						<< std::endl;
#endif
				}
			}

			//
			//	Wait for delivering message during task list 
			//	is not longer locked.
			//
			return f.get();
		}

		//
		//	shutdown already done or in progress
		//
		return false;
	}

	bool ruler::deliver(std::size_t id, std::size_t slot) const
	{
		return deliver(id, slot, tuple_t());
	}


}	//	cyy


