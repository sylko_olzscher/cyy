/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_SCHEDULER_SHUTDOWN_H
#define CYY_SCHEDULER_SHUTDOWN_H

#include <cyy/cyy_export.h>
#include <cyy/scheduler/task_fwd.h>

namespace cyy 
{
	class shutdown 
	{
	public:
		typedef std::tuple<int>	msg_1;
		typedef std::tuple<msg_1>	msg_type;
		
	public:
		shutdown(broker&, std::size_t);
		
		//	task interface
		void run(int workload);
		void stop();
		inline void process(int const&)
		{}

	private:
		broker&	broker_;
		//	gc task id
		const std::size_t gc_;
	};
}

#endif	//	CYY_SCHEDULER_SHUTDOWN_H