/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_SCHEDULER_GC_H
#define CYY_SCHEDULER_GC_H

#include <cyy/cyy_export.h>
#include <cyy/scheduler/task_fwd.h>
#include <cyy/intrinsics/cyystddef.h>
#include <list>

#include <cyy/util/meta.hpp>

namespace cyy 
{
	/*
	 * Removes the specified task from task list and itself.
	 */
	class gc 
	{
	public:
		using msg_0 = std::tuple<index>;	//	stop a single task
		using msg_1 = empty_msg_t;	//	stop all tasks
		using msg_type = std::tuple<msg_0, msg_1>;

	public:
		gc(broker&);
		
		//	task interface
		void run(int workload);

		/*
		 * locks task map
		 */
		void stop();

		/*
		 * Extract task id from mesage, call the stop() method
		 * of this task and removes it from task list.
		 * Locks task map.
		 *
		 * @tsk task to stop
		 */
		void process(index const& tsk);

		/*
		 * Stop all tasks at once.
		 */
		void process();
        
	private:
		/**
		 * Find the specified task, removes it from the task list
		 * and return a shared pointer containing the task object.
		 *
		 * @param id task id
		 * @return shared pointer containing the task object. Could be empty.
		 */
		shared_task find(std::size_t id);

		/**
		 * Collect a list of all tasks except this one.
		 */
		std::list<shared_task>	get_task_list();

	private:
		broker&	broker_;
	};
}

#endif	//	CYY_SCHEDULER_GC_H
