/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "shutdown.h"
#include <cyy/scheduler/broker.h>
#include <cyy/scheduler/ruler.h>
#include <iostream>

namespace cyy 
{
	shutdown::shutdown(broker& b, std::size_t id)
	: broker_(b)
	, gc_(id)
	{}
	
	void shutdown::run(int workload)
	{
		//std::cerr 
		//	<< "***Info: shutdown #" 
		//	<< broker_.get_tag() 
		//	<< " is RUNNING" 
		//	<< std::endl;

		if (!broker_.ruler_.shutdown_.exchange(true))
		{

			{
				thread::unique_lock<thread::shared_mutex> test(broker_.ruler_.mutex_, thread::try_to_lock);
				//	if we cannot grab the lock, then anyone other has it. This is critical.
				//BOOST_ASSERT_MSG(test.owns_lock(), "deadlock");
				if (!test.owns_lock())	std::cerr << "***warning: possible deadlock in shutdown task: " << broker_.get_tag() << std::endl;
			}

			//	lock task map
			thread::unique_lock<thread::shared_mutex> guard(broker_.ruler_.mutex_);

			//	stop all tasks - in reverse order
			auto pos = broker_.ruler_.tasks_.end();
			while (pos != broker_.ruler_.tasks_.begin())
			{
				--pos;

				//	don't remove this running task from
				//	task list
				if (pos->first != broker_.get_tag() && pos->first != gc_)
				{
					//
					//	hold a reference
					//
					auto tmp = pos->second->get_shared();

					//
					//	After removing from the list the task is no longer
					//	available
					//
					pos = broker_.ruler_.tasks_.erase(pos);

					//
					//	Now task could be stopped in a safely manner
					//
					tmp->stop();

				}
			}

			BOOST_ASSERT_MSG(broker_.ruler_.tasks_.size() == 2, "wrong task count (2)");

			//	remove gc task at least, because stop() method
			//	cannot be called in loop above because the iterator would be screwed. 
			pos = broker_.ruler_.tasks_.find(gc_);
			if (pos != broker_.ruler_.tasks_.end())
			{
				auto tmp = pos->second->get_shared();
				//	call stop - iterator screwed
				if (tmp)	tmp->stop();
			}

			BOOST_ASSERT_MSG(broker_.ruler_.tasks_.size() == 1, "wrong task count (1)");

			//	close this task
			broker_.push_result(boost::system::errc::make_error_code(boost::system::errc::success));
		}
		else
		{
			//	already done
			broker_.push_result(boost::system::errc::make_error_code(boost::system::errc::operation_in_progress));
		}

	}
	
	void shutdown::stop()
	{}

}