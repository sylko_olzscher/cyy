/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#include "gc.h"
#include <cyy/scheduler/broker.h>
#include <cyy/scheduler/ruler.h>

namespace cyy 
{
	gc::gc(broker& b)
		: broker_(b)
	{}
	
	void gc::run(int workload)
	{}
	
	void gc::process(index const& msg)
	{
		//
		//	task  scheduler is in shutdown mode
		//
		if (broker_.ruler_.shutdown_.load())	return;

		const std::size_t id = *msg;
		//std::cout << "remove task " << id << std::endl;

		BOOST_ASSERT_MSG(id != broker_.get_tag(), "connot remove itself");
		if (id == broker_.get_tag())	return;

		if (auto st = find(id))
		{
			st->stop();
		}
	}

	void gc::process()
	{
		//
		//	set global shutdown flag. No new tasks should be started
		//	after this and no messages could be delivered as of now.
		//
		if (!broker_.ruler_.shutdown_.exchange(true))
		{
#ifdef _DEBUG
			//	This is a test
			//	If we cannot grab the lock, then anyone other has it. This is critical.
			{
				thread::unique_lock<thread::shared_mutex> test(broker_.ruler_.mutex_, thread::try_to_lock);
				if (!test.owns_lock())	std::cerr << "***warning: possible deadlock in shutdown task: " << broker_.get_tag() << std::endl;
			}
#endif

			//
			//	get list of all running tasks
			//
			const auto list = get_task_list();

			std::for_each(std::begin(list), std::end(list), [](shared_task ptr) {

				//
				//	no auto triggered stops are allowed
				//	at this stage.
				//
				ptr->stop();
			});

			//
			//	remove this task from fask list
			//
			stop();

			//
			//	close this task
			//
			broker_.push_result(boost::system::errc::make_error_code(boost::system::errc::success));

		}
		else
		{
			broker_.push_result(boost::system::errc::make_error_code(boost::system::errc::operation_in_progress));
		}

	}

	std::list<shared_task>	gc::get_task_list()
	{
		std::list<shared_task> r;

		//
		//	lock task map
		//
		thread::unique_lock<thread::shared_mutex> guard(broker_.ruler_.mutex_);

		//	stop all tasks - in reverse order
		for (auto pos = broker_.ruler_.tasks_.cbegin(); pos != broker_.ruler_.tasks_.cend(); /* no increment */)
		{
			if (pos->first != broker_.get_tag())
			{
				r.push_front(pos->second->get_shared());
				pos = broker_.ruler_.tasks_.erase(pos);				
			}
			else
			{
				++pos;
			}
		}

		BOOST_ASSERT_MSG(broker_.ruler_.tasks_.size() == 1, "wrong task count (1)");
		return r;
	}

	shared_task gc::find(std::size_t id)
	{
		//	lock task map
		thread::unique_lock<thread::shared_mutex> guard(broker_.ruler_.mutex_);

		//	remove specifed tasks
		auto pos = broker_.ruler_.tasks_.find(id);
		const bool b = pos != broker_.ruler_.tasks_.end();
		if (b)
		{
			//
			//	hold a reference
			//
			auto tmp = pos->second->get_shared();

			//
			//	After removing from the list the task is no longer
			//	available
			//
			broker_.ruler_.tasks_.erase(pos);

			//
			//	pos is now invalid
			//

			//
			//	Now task could be stopped in a safely manner
			//
			return tmp;
		}

		//
		//	This is a failure state
		//
		std::cerr
			<< "***warning: task "
			<< id
			<< " not found"
			<< std::endl;

		return shared_task();
	}

	void gc::stop()
	{
		//	task map is locked from shutdown task
		//BOOST_ASSERT_MSG(!broker_.ruler_.mutex_.try_lock(), "missing lock");
		//	lock task map
		thread::unique_lock<thread::shared_mutex> guard(broker_.ruler_.mutex_);

		//	remove this tasks from task list 
		auto count = broker_.ruler_.tasks_.erase(broker_.get_tag());
		BOOST_ASSERT_MSG(count == 1, "gc not found");
	}

}