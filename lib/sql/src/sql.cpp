/*
 * Boost Software License - Version 1.0 - August 17th, 2003
 *
 * Permission is hereby granted, free of charge, to any person or organization
 * obtaining a copy of the software and accompanying documentation covered by
 * this license (the "Software") to use, reproduce, display, distribute,
 * execute, and transmit the Software, and to prepare derivative works of the
 * Software, and to permit third-parties to whom the Software is furnished to
 * do so, all subject to the following:
 *
 * The copyright notices in the Software and this entire statement, including
 * the above license grant, this restriction and the following disclaimer,
 * must be included in all copies of the Software, in whole or in part, and
 * all derivative works of the Software, unless such copies or derivative
 * works are solely in the form of machine-executable object code generated by
 * a source language processor.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE, TITLE AND NON-INFRINGEMENT. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDERS OR ANYONE DISTRIBUTING THE SOFTWARE BE LIABLE
 * FOR ANY DAMAGES OR OTHER LIABILITY, WHETHER IN CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * Copyright 2015 Sylko Olzscher.
 */ 

#include <cyy/sql/sql.h>
#include <cyy/sql/field.h>

#include <algorithm>
#include <numeric>
#include <boost/assert.hpp>

namespace cyy 
{
	namespace sql 
	{
		command::command(table const& tbl, custom_callback cb)
		: std::enable_shared_from_this< command >()
			, table_(tbl)
			, cb_(cb)
			, stream_()
		{}
	
		std::shared_ptr< command > factory(table const& tbl, custom_callback cb)
		{
			return std::make_shared< command >(tbl, cb);
		}
		
		void command::clear()
		{
			stream_.str("");
		}
		
		void command::clear(std::string const& str)
		{
			clear();
			stream_
			<< str
			<< ' '
			;
		}
		
		select_ptr command::select(dialect dia)
		{
			std::vector<std::size_t> vec(table_.size());
			std::iota(vec.begin(), vec.end(), 1);
			return select(vec, dia);
		}

 		select_ptr command::select(std::string const& s, dialect dia)
 		{
			clear("SELECT");
 			return std::make_shared< sql_select >(stream_, &table_, dia, s);
 		}
		
		select_ptr command::select(std::initializer_list<std::size_t> list, dialect dia)
		{
			const std::vector<std::size_t> vec = list;
			return select(vec, dia);
		}
		
		select_ptr command::select(std::vector<std::size_t> const& vec, dialect dia)
		{
			clear("SELECT");
			bool init_flag = false;
			std::for_each(std::begin(vec), std::end(vec), [&init_flag, dia, this](std::size_t index) {

				if (!init_flag)
				{
					init_flag = true;
				}
				else
				{
					stream_ << ", ";
				}

				BOOST_ASSERT_MSG(index > 0, "index out of range");

				if (dia == cyy::sql::SQLITE && table_.is_of_type(index - 1, cyy::types::CYY_TIME_STAMP))
				{
					stream_
						<< "datetime("
						<< table_.get_column_name(index - 1)
						<< ')'
						;
				}
				else
				{
					stream_
						<< table_.get_column_name(index - 1)
						;
				}
			});

			if (init_flag)
			{
				stream_ << ' ';
			}

			return std::make_shared< sql_select >(stream_, &table_, dia);

		}

		select_count_ptr command::count(dialect dia)
		{
			clear("SELECT");
			return std::make_shared< sql_select_count >(stream_, &table_, dia);
		}

		update_ptr command::update(std::vector<std::size_t> const& vec, dialect dia)
		{
			clear("UPDATE");
			stream_
				<< table_.get_table_name()
				<< " SET "
				;

			bool init_flag = false;
			std::for_each(std::begin(vec), std::end(vec), [&init_flag, dia, this](std::size_t index) {

				if (!init_flag)
				{
					init_flag = true;
				}
				else
				{
					stream_ << ", ";
				}

				stream_ << table_.get_column_name(index);

				if (dia == cyy::sql::SQLITE && table_.is_of_type(index, cyy::types::CYY_TIME_STAMP))
				{
					stream_ << " = julianday(?)";
				}
				else
				{
					stream_ << " = ?";
				}
			});

			if (init_flag)
			{
				stream_ << ' ';
			}

			return std::make_shared< sql_update >(stream_, &table_, dia);
		}

		update_ptr command::update(std::initializer_list<std::size_t> list, dialect dia)
		{
			const std::vector<std::size_t> vec = list;
			return update(vec, dia);
		}
		
		update_ptr command::update(dialect dia)
		{
			std::vector<std::size_t> vec;	// (table_.size());
			auto meta = table_.get_meta_data();
			for (std::size_t idx = 0; idx < table_.size(); idx++)
			{
				if (meta.is_body(idx))
				{
					vec.push_back(idx);
				}
			}
			return update(vec, dia);
		}

		insert_ptr command::insert(dialect dia)
		{
			clear();
			return std::make_shared< sql_insert >(stream_, &table_, dia, table_.get_table_name());
		}
		
		create_ptr command::create(dialect dia)
		{
			clear();
			return std::make_shared< sql_create >(stream_, &table_, dia, cb_);
		}
		
		delete_ptr command::remove(dialect dia)
		{
			clear();
			return std::make_shared< sql_delete >(stream_, &table_, dia, cb_);
		}

		drop_ptr command::drop(dialect dia)
		{
			clear();
			return std::make_shared< sql_drop >(stream_, &table_, dia, cb_);
		}

		void command::alter()
		{
			clear();
		}
	
		std::string command::to_str() const
		{
			return stream_.str();
		}

		
		/**
		 * select constructor
		 */
		sql_select::sql_select(std::ostream& s, const table* tbl, dialect dia)
		: std::enable_shared_from_this< sql_select >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{}

		sql_select::sql_select(std::ostream& s, const table* tbl, dialect dia, std::string const& str)
		: std::enable_shared_from_this< sql_select >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			if (!str.empty())
			{
				stream_
					<< str
					<< ' '
					;
			}
		}
	
		select_count_ptr sql_select::count()
		{
			return std::make_shared< sql_select_count >(stream_, table_, dia_);
		}
	
		select_all_ptr sql_select::all()
		{
			return std::make_shared< sql_select_all >(stream_, table_, dia_);
		}

		select_all_ptr sql_select::all(std::string const& str)
		{
			return std::make_shared< sql_select_all >(stream_, table_, dia_, str);
		}

		select_distinct_ptr sql_select::distinct()
		{
			return std::make_shared< sql_select_distinct >(stream_, table_, dia_);
		}

		select_distinct_ptr sql_select::distinct(std::string const& str)
		{
			return std::make_shared< sql_select_distinct >(stream_, table_, dia_, str);
		}
		
		from_ptr sql_select::from(std::string const& str)
		{
			return std::make_shared< sql_from >(stream_, table_, dia_, str);			
		}

		from_ptr sql_select::from()
		{
			return std::make_shared< sql_from >(stream_, table_, dia_, table_->get_table_name());
		}

		sql_select_all::sql_select_all(std::ostream& s, const table* tbl, dialect dia)
		: std::enable_shared_from_this< sql_select_all >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			stream_ 
			<< "ALL * ";
		}

		sql_select_all::sql_select_all(std::ostream& s, const table* tbl, dialect dia, std::string const& str)
		: std::enable_shared_from_this< sql_select_all >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			stream_ 
			<< "ALL "
			<< str
			;
		}

		from_ptr sql_select_all::from(std::string const& str)
		{
			return std::make_shared< sql_from >(stream_, table_, dia_, str);			
		}

		from_ptr sql_select_all::from()
		{
			return std::make_shared< sql_from >(stream_, table_, dia_, table_->get_table_name());
		}

		/**
		 * select_distinct constructor
		 */
		sql_select_distinct::sql_select_distinct(std::ostream& s, const table* tbl, dialect dia)
		: std::enable_shared_from_this< sql_select_distinct >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			stream_ << "DISTINCT * ";
		}

		sql_select_distinct::sql_select_distinct(std::ostream& s, const table* tbl, dialect dia, std::string const& str)
		: std::enable_shared_from_this< sql_select_distinct >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			stream_ 
			<< "DISTINCT "
			<< str
			;
		}

		from_ptr sql_select_distinct::from(std::string const& str)
		{
			return std::make_shared< sql_from >(stream_, table_, dia_, str);			
		}
		
		/**
		 * select_count constructor
		 */
		sql_select_count::sql_select_count(std::ostream& s, const table* tbl, dialect dia)
		: std::enable_shared_from_this< sql_select_count >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			stream_ 
				<< "count(*) FROM "
				<< table_->get_table_name()
				//<< ' '
				;
		}

		where_ptr sql_select_count::where_pk()
		{
			stream_
				<< " WHERE "
				;

			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this](column_definition const& col) {

				if (col.pk_)
				{
					if (!init_flag)
					{
						init_flag = true;
					}
					else
					{
						stream_ << " AND ";
					}

					stream_
						<< col.name_
						<< " = ?"
						;
				}
			});
			return std::make_shared< sql_where >(stream_, table_);
		}


		/**
		 *	@code
		 DELETE FROM table_name WHERE some_column=some_value;
		 *	@endcode
		 */
		sql_delete::sql_delete(std::ostream& s, const table* tbl, dialect dia, custom_callback cb)
			: stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			BOOST_ASSERT_MSG(table_ != nullptr, "table not initialized");

			stream_
				<< "DELETE FROM "
				<< table_->get_table_name()
				<< ' '
				;
		}

		where_ptr sql_delete::where_pk()
		{
			stream_
				<< "WHERE "
				;

			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this](column_definition const& col) {

				if (col.pk_)
				{
					if (!init_flag)
					{
						init_flag = true;
					}
					else
					{
						stream_ << " AND ";
					}

					stream_
						<< col.name_
						<< " = ?"
						;
				}
			});
			return std::make_shared< sql_where >(stream_, table_);

		}

		/**
		 * from constructor
		 */
		sql_from::sql_from(std::ostream& s, const table* tbl, dialect dia, std::string const& str)
		: std::enable_shared_from_this< sql_from >()
			, stream_(s)
			, table_(tbl)
			, dia_(dia)
		{
			stream_ 
				<< "FROM "
				<< str
				<< ' '
				;
		}
		
		where_ptr sql_from::where(const char* s)
		{
			stream_
				<< "WHERE "
				<< s
				;
			return std::make_shared< sql_where >(stream_, table_);
		}

		group_ptr sql_from::group_by(std::string const& str)
		{
			return std::make_shared< sql_group >(stream_, table_, str);
		}

		/**
		 * where constructor
		 */
		sql_where::sql_where(std::ostream& s, const table* tbl)
		: std::enable_shared_from_this< sql_where >()
			, stream_(s)
			, table_(tbl)
		{
			stream_ 
			<< ' ';
		}
		
		group_ptr sql_where::group_by(std::string const& str)
		{
			return std::make_shared< sql_group >(stream_, table_, str);
		}

		order_ptr sql_where::order_by(std::string const& str, bool asc)
		{
			return std::make_shared< sql_order >(stream_, table_, str, asc);
		}
		
		order_ptr sql_where::order_by(std::size_t index, bool asc)
		{
			return std::make_shared< sql_order >(stream_, table_, table_->get_column_name(index - 1), asc);
		}

		/**
		 * group constructor
		 */
		sql_group::sql_group(std::ostream& s, const table* tbl, std::string const& str)
		: std::enable_shared_from_this< sql_group >()
		, stream_(s)
		{
			stream_ 
			<< "GROUP BY "
			<< str
			<< ' ';
		}		
		
		having_ptr sql_group::having(std::string const& str)
		{
			return std::make_shared< sql_having >(stream_, table_, str);
		}

		order_ptr sql_group::order_by(std::string const& str, bool asc)
		{
			return std::make_shared< sql_order >(stream_, table_, str, asc);
		}

		/**
		 * having constructor
		 */
		sql_having::sql_having(std::ostream& s, const table* tbl, std::string const& str)
		: std::enable_shared_from_this< sql_having >()
			, stream_(s)
			, table_(tbl)
		{
			stream_ 
			<< "HAVING "
			<< str
			<< ' ';
		}		

		order_ptr sql_having::order_by(std::string const& str, bool asc)
		{
			return std::make_shared< sql_order >(stream_, table_, str, asc);
		}

		/**
		 * order constructor
		 */
		sql_order::sql_order(std::ostream& s, const table* tbl, std::string const& str, bool asc)
		: std::enable_shared_from_this< sql_order >()
			, stream_(s)
			, table_(tbl)
		{
			stream_ 
			<< "ORDER BY "
			<< str
			<< ' '
			<< (asc ? "ASC" : "DESC")
			<< ' '
			;
		}		
		
		/**
		 * insert constructor
		 */
		sql_insert::sql_insert(std::ostream& s, const table* tbl, dialect dia, std::string const& str)
		: std::enable_shared_from_this< sql_insert >()
			, stream_(s)
			, table_(tbl)
		{
			stream_ 
			<< "INSERT INTO "
			<< str
			<< ' '
			<< '('
			;
			
			write_columns(dia);
			
			stream_ 
			<< ") VALUES ("
			;

			write_values(dia);

			stream_ 
			<< ')'
			;
		}

		void sql_insert::write_columns(dialect dia)
		{
			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this, dia](column_definition const& col){
				
				if (!init_flag)	
				{
					init_flag = true;
				}
				else	
				{
					stream_ << ", ";
				}
				
				stream_
				<< col.name_
				;
			});
		}
		
		void sql_insert::write_values(dialect dia)
		{
			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this, dia](column_definition const& col){
				
				if (!init_flag)	
				{
					init_flag = true;
				}
				else	
				{
					stream_ << ", ";
				}
				
				if (dia == cyy::sql::SQLITE && col.type_ == cyy::types::CYY_TIME_STAMP)
				{
					stream_	<< "julianday(?)";
				}
				else
				{
					stream_	<< '?';
				}
			});
		}
		
		/**
		 * update constructor
		 */
		sql_update::sql_update(std::ostream& s, const table* tbl, dialect dia)
		: stream_(s) 
		, table_(tbl)
		, dia_(dia)
		{}
		
		where_ptr sql_update::where_pk()
		{
			stream_
				<< "WHERE "
				;

			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this](column_definition const& col) {

				if (col.pk_)
				{
					if (!init_flag)
					{
						init_flag = true;
					}
					else
					{
						stream_ << " AND ";
					}

					stream_
						<< col.name_
						<< " = ?"
						;
				}
			});
			return std::make_shared< sql_where >(stream_, table_);
		}

		/**
		 * MSSQL Server doesn't support the "IF NOT EXSITS" syntax.
		 *	A workaround could be something like:
		 *	@code
		 IF(EXISTS(SELECT *
		 FROM INFORMATION_SCHEMA.TABLES
		 WHERE TABLE_SCHEMA = 'TheSchema'
		 AND  TABLE_NAME = 'TheTable'))
		 BEGIN
		 --Do Stuff
		 END
		 *	@endcode
		 */
		
		sql_create::sql_create(std::ostream& s, const table* tbl, dialect dia, custom_callback cb)
		: stream_(s)
		, table_(tbl)
		{
			BOOST_ASSERT_MSG(table_ != nullptr, "table not initialized");
			
			stream_ 
			<< "CREATE TABLE "
			;
			
						
			if (has_if_not_exist_support(dia))
			{
				stream_ 
				<< "IF NOT EXISTS "
				;
			}
			
			stream_ 
 			<< table_->get_table_name()
			<< ' '
			<< '('
			;
			
			//
			//	write column definitions
			//
			write_columns(dia, cb);
			
			//
			//	write primary key constraint
			//
			const bool has_pk = table_->has_primary_key();
			if (has_pk)
			{
				stream_
				<< ", PRIMARY KEY("
				;
			
				write_primary_key();
				
				stream_
				<< ")"
				;
			}
			
			//
			//	write (named) unique constraints
			//
			const bool has_uc = table_->has_unique_constraints();
			if (has_uc)
			{
				write_unique_constraints(has_pk);
			}
			
			stream_ 
			<< ')'
			;
		}
		
		void sql_create::write_columns(dialect dia, custom_callback cb)
		{
			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this, dia, cb](column_definition const& col){
				
				if (!init_flag)	
				{
					init_flag = true;
				}
				else	
				{
					stream_ << ", ";
				}
				
				stream_
				<< col.name_
				<< ' '
				<< col.get_field_type(dia, cb)
				;
			});
		}
		
		void sql_create::write_primary_key()
		{
			bool init_flag = false;
			std::for_each(std::begin(table_->columns_), std::end(table_->columns_), [&init_flag, this](column_definition const& col){
				
				if (col.pk_)
				{
					if (!init_flag)	
					{
						init_flag = true;
					}
					else	
					{
						stream_ << ", ";
					}
					
					stream_
					<< col.name_
					;
				}
			});
		}
		
		void sql_create::write_unique_constraints(bool has_pk)
		{
			bool init_flag = has_pk;
			std::for_each(std::begin(table_->unique_constraints_), std::end(table_->unique_constraints_), [&init_flag, this](table::unique_constraints_t::value_type const& cons){
				
				if (!init_flag)	
				{
					init_flag = true;
				}
				else	
				{
					stream_ << ", ";
				}
				
				stream_ 
				<< "CONSTRAINT "
				<< cons.first	//	name of constraint
				<< " UNIQUE ("
				;
				
				write_unique_constraint_members(cons.second);
				
				stream_ 
				<< ")"
				;
 			});
		}
		
		void sql_create::write_unique_constraint_members(unique_constraint const& uc)
		{
			bool init_flag = false;
 			std::for_each(std::begin(uc), std::end(uc), [&init_flag, this](std::size_t index){
 				
 				if (!init_flag)	
 				{
 					init_flag = true;
 				}
 				else	
 				{
 					stream_ << ", ";
 				}
 				
 				BOOST_ASSERT_MSG(index < table_->columns_.size(), "column index out of range");
				stream_ 
				<< table_->columns_[index].name_;
				;
				
			});
		}
		
		sql_drop::sql_drop(std::ostream& s, const table* tbl, dialect dia, custom_callback cb)
			: stream_(s)
			, table_(tbl)
		{
			BOOST_ASSERT_MSG(table_ != nullptr, "table not initialized");

			stream_
				<< "DROP TABLE "
				;

			if (has_if_not_exist_support(dia))
			{
				stream_
					<< "IF EXISTS "
					;
			}

			stream_
				<< table_->get_table_name()
				;
		}

	}
}
