# top level files
set (odbc_lib)

set (odbc_cpp

	lib/sql/src/intrinsics/factory/odbc_factory.cpp
)

set (odbc_h

	src/main/include/cyy/sql/intrinsics/factory/odbc_factory.h
)

source_group("ODBC" FILES ${odbc_cpp} ${odbc_h})

# define the core lib
set (odbc_lib
  ${odbc_cpp}
  ${odbc_h}
)