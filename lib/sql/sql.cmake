# top level files
set (sql_lib)

set (sql_cpp

	lib/sql/src/sql.cpp
	lib/sql/src/dialects.cpp
	lib/sql/src/field.cpp
	lib/sql/src/column_definition.cpp
	lib/sql/src/table.cpp
)

set (sql_h

	src/main/include/cyy/sql/sql.h
	src/main/include/cyy/sql/dialects.h
	src/main/include/cyy/sql/field.h
	src/main/include/cyy/sql/column_definition.h
	src/main/include/cyy/sql/table.h
	src/main/include/cyy/sql/sql_callback.h
)

set (sql_dsl

	src/main/include/cyy/sql/dsl/constant.hpp
	lib/sql/src/dsl/constant.cpp
	src/main/include/cyy/sql/dsl/variable.hpp
	src/main/include/cyy/sql/dsl/binary_expression.hpp
	src/main/include/cyy/sql/dsl/list_expression.hpp
	src/main/include/cyy/sql/dsl/operators.hpp
	src/main/include/cyy/sql/dsl/column.h
	lib/sql/src/dsl/column.cpp
	src/main/include/cyy/sql/dsl/time_point.h
	lib/sql/src/dsl/time_point.cpp
	src/main/include/cyy/sql/dsl/placeholder.h
	lib/sql/src/dsl/placeholder.cpp
	src/main/include/cyy/sql/dsl/aggregate.h
	lib/sql/src/dsl/aggregate.cpp
)

source_group("dsl" FILES ${sql_dsl})

# define the core lib
set (sql_lib
  ${sql_cpp}
  ${sql_h}
  ${sql_dsl}
)