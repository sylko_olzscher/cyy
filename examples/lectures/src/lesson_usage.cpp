#include "lesson_usage.h"
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <boost/assert.hpp>
#include <cyy/object_cast.hpp>
#include <cyy/value_cast.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace cyy 
{
	void example_usage()
	{
		auto obj = create<types::CYY_INT32>(99);
		BOOST_ASSERT(cyy::get_code(obj) == types::CYY_INT32);
		
		//	an integer should be comparable
		BOOST_ASSERT(cyy::is_comparable(obj));
		
		//	an integer should be sortable
		BOOST_ASSERT(cyy::is_sortable(obj));
		
		//	get const pointer to value
		const int* p = object_cast<int>(obj);
		BOOST_ASSERT(p != nullptr);
		//	should print 99
		std::cout << *p << ", ";
		
		//	asserts or returns nullptr
		//const long* pl = object_cast<long>(obj);
		
		//	cast according to selected type code
		const int* i = type_code_cast<types::CYY_INT32>(obj);
		BOOST_ASSERT(i != nullptr);
		//	should print 99
		std::cout << *i << ", ";

		//	value_cast requires a default value
		const int& n = value_cast(obj, 0);
		std::cout << n << ", ";
		
		auto str = string_factory("hello cyy");
		std::cout << string_cast(str)<< ", " << string_cast(obj, "not a string") << ", ";
		
		//	create a random UUID v4
		uuid_random_factory uuid_gen;
		auto tag = uuid_gen();
		std::cout << uuid_cast(tag) << std::endl;
		
	}
}