#include "lesson_reader.h"
#include <cyy/util/read_sets.h>
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <boost/uuid/uuid_io.hpp>
#include <boost/uuid/random_generator.hpp>

namespace cyy 
{
	void example_reader()
	{
		//	create a complex and deep nested data structure (again)
		auto tpl = tuple_factory(make_parameter("pi", 3.1415926535)
			, make_parameter("X", 10)
			, make_parameter("Y", 20)
			, make_parameter("Z", 30)
			, factory("Q", tuple_factory(make_parameter("A", 1), make_parameter("B", 2), make_parameter("C", 3)))
			);

		object_reader reader1(tpl);
		const cyy::object obj = reader1.get_obj({ "Q", "B" });
		std::cout << numeric_cast<int>(obj) << ", ";

		//	this is a move asignment
		tpl = tuple_factory(set_factory(1, "one")
			, make_attribute(2, "two")
			, factory(8, tuple_factory(make_attribute(1, "one"), factory(7, tuple_factory(make_parameter("A", 1), make_parameter("B", 2), make_parameter("C", 3)))))
			, make_parameter("seven", cyy::chrono::days(7))
 		);

		//	using the subscript operator
		object_reader reader2(tpl);
		std::cout << reader2[8][7].get_numeral<int>("B", 0) << std::endl;

		//	create a tuple with attributes
		tpl = tuple_factory(attr_map_factory(boost::uuids::random_generator()()
			, boost::asio::ip::address::from_string("127.0.0.5")
			, "acme"
			, false
			, std::chrono::minutes::max()
			, std::chrono::seconds(22)));

		//	create a typesafe writer 
		typedef writer_factory<
			boost::uuids::uuid, //	ident
			boost::asio::ip::address, //	address
			std::string, 	//	name
			bool, 			//	enabled
			std::chrono::minutes,	//	watchdog
			std::chrono::seconds	//	monitor
		> writer;

		//	get type code at runtime (should print "ip")
		const code_t code = get_type_code<typename writer::return_type>()(1);
		std::cout
			<< types::name(code)
			<< std::endl;

		//	write values into a tuple
		auto r = writer::write(object_ref_cast<tuple_t>(tpl));
		std::cout
			<< std::get<0>(r) << ", "
			<< std::get<1>(r) << ", "
			<< std::get<2>(r) << ", "
			<< std::boolalpha
			<< std::get<3>(r) << ", "
			<< std::get<4>(r).count() << "min, "
			<< std::get<5>(r).count() << "sec"
			<< std::endl;

	}
}