#include "lesson_composites.h"
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <cyy/intrinsics/algorithm.h>
#include <cyy/io/stream_io.h>
#include <cyy/io.h>
#include <boost/uuid/random_generator.hpp>

namespace cyy 
{
	void example_composites()
	{
		
		//	nested container
		auto tpl1 = tuple_factory(set_factory(1, "one")
			, set_factory(2, "two")
			, vector_factory({"AAA", "BBB", "C++"})
			, tuple_factory( set_factory(1, "one")
			, set_factory(2, "two")
			, position_t(), position_t()	//	current template expansion size
			, make_parameter("pi", 3.1415926535)	//	compare this to set_factory
			, set_factory("e", 2.718281828459045)	//	compare this to make_parameter
			, set_factory(8, "eight")
			, tuple_factory(1, 2, 3)
			, set_factory(23, std::chrono::seconds(23)))
			//	vector of vectors
			, vector_factory({vector_factory({"this", "is", "an", "early", "draft."})
			, vector_factory({"It is", "known", "to be", "incomplete", "and", "incorrect."})})
			, set_factory("seven", cyy::chrono::days(7))
		);
		
		//	print tree structure
		print(std::cout, tpl1, io::custom_callback()); 
		
		auto map1 = param_map_factory({ make_attribute(1, 1), make_attribute(2, 2)  });
		auto map2 = param_map_factory({ make_parameter("1", 1), make_parameter("2", 2), make_parameter("3", 3) });
		auto map3 = attr_map_factory(12, "hello", tpl1.clone(), boost::uuids::random_generator()());
		print(std::cout, map1, io::custom_callback()); 
		//std::cout << to_literal(map1) << std::endl;
		
		// read and search on nested containers
 		auto vec1 = vector_factory({make_attribute(1, "one")
			, make_attribute(2, "two")
			, make_attribute(8, "eight")
			, make_attribute(5, "five")});
		std::cout << to_literal(vec1) << std::endl;
		
		//	attributes are unsorted
 	 	assert(!is_sorted_attr(object_ref_cast<vector_t>(vec1)));
		
		// using a vector as key-value store
		std::cout << "8 => " <<  find_value(vec1, 8, std::string("not found")) << std::endl;
		
	}
}