#include "lesson_math.h"
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <boost/assert.hpp>
#include <cyy/value_cast.hpp>
#include <cyy/intrinsics/math.h>

namespace cyy 
{
	void example_math()
	{
		const auto two = factory(2);
		const auto three = factory(3);
		const auto four = factory(4);
		//	2 + 3 * 4
		const auto r = two + three * four;

		auto i = value_cast<int>(r, 0);
		std::cout << i << ", ";
		
		//	considering operator precedence the result
		//	should be equal to 14
		BOOST_ASSERT(i == 14);

		const auto str = string_factory("abc");
		
		//	a string could be multiplied only with 
		//	unsigned integers
		auto result = factory(10u) * str;
		
		//	abcabcabcabcabcabcabcabcabcabc
		std::cout << string_cast(result) << std::endl;
		
	}
}