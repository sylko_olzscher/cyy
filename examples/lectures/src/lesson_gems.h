/*
 * Copyright Sylko Olzscher 2016
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */

#ifndef CYY_LESSON_GEMS_H
#define CYY_LESSON_GEMS_H


namespace cyy 
{
	void example_gems();
}	//	cyy 

#endif	//	CYY_LESSON_GEMS_H

