/*
 * Copyright Sylko Olzscher 2016-2017
 * 
 * Use, modification, and distribution is subject to the Boost Software
 * License, Version 1.0. (See accompanying file LICENSE_1_0.txt or copy at
 * http://www.boost.org/LICENSE_1_0.txt)
 */


#include <boost/program_options.hpp>
//#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <fstream>
#include <iostream>
#include <cyy/version_info.h>

#include "lesson_factory.h"
#include "lesson_factory_generic.h"
#include "lesson_usage.h"
#include "lesson_math.h"
#include "lesson_composites.h"
#include "lesson_types.h"
#include "lesson_reader.h"
#include "lesson_gems.h"
#include "lesson_io.h"

/**
 * Examples according to the cyy library introduction article.
 */
int main(int argc, char* argv[])	{

	try
	{
		std::string config_file;
	
		//
		//	generic options
		//
		boost::program_options::options_description generic("Generic options");
		generic.add_options()
		
		("help,h", "print usage message")
		("version,v", "print version string")
		("build,b", "last built timestamp and platform")
		("config,C", boost::program_options::value<std::string>(&config_file)->default_value("lecture.cfg"), "optional configuration file")

		;

		//
		//	all lecture options
		//
		std::string test = "all";
		boost::program_options::options_description lecture("lecture");
		lecture.add_options()

			("verbose,V", boost::program_options::bool_switch()->default_value(false), "verbose mode")
			("run,r", boost::program_options::value<std::string>(&test), "Select an example")
//			("run,r", boost::program_options::value<std::string>(&test)->default_value("all"), "Select a test")
			;

		//
		//	all you can grab from the command line
		//
		boost::program_options::options_description cmdline_options;
        cmdline_options.add(generic).add(lecture);
		

		boost::program_options::variables_map vm;
		boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(cmdline_options).run(), vm);
		boost::program_options::notify(vm);
 
		if (vm.count("help")) 
		{  
			std::cout 
			<< cmdline_options 
			<< std::endl
			;
			return EXIT_SUCCESS;
		}
		
		if (vm.count("version")) 
		{  
			std::cout 
			<< "cyysam assembl[e]r v" 
			<< CYY_VERSION
			<< std::endl
			;
			return EXIT_SUCCESS;
		}
		
	if (vm.count("build")) 
	{  
		std::cout 
		<< "last built at : "
		<< __DATE__
		<< " "
		<< __TIME__
		<< std::endl
		<< "Platform      : "
		<< CYY_PLATFORM
		<< std::endl
		<< "Compiler      : "
		<< BOOST_COMPILER
		<< std::endl
		<< "StdLib        : "
		<< BOOST_STDLIB
		<< std::endl
		<< "BOOSTLib      : "
		<< BOOST_LIB_VERSION 
		<< " ("
		<< BOOST_VERSION
		<< ")"
		<< std::endl
		<< "build type    : "
		<< CYY_BUILD_TYPE
		<< std::endl
		<< std::endl
		;
		return EXIT_SUCCESS;
	}
		

		std::ifstream ifs(config_file);
		if (!ifs)
		{
// 			std::cout 
// 			<< "***info: config file: " 
// 			<< config_file 
// 			<< " not found"
// 			<< std::endl
// 			;
		}
		else
		{
			boost::program_options::store(boost::program_options::parse_config_file(ifs, lecture), vm);
			boost::program_options::notify(vm);
		}
		 
 		if (vm.count("run"))
 		{
			const std::string name = vm["run"].as< std::string >();
 			std::cout 
 			<< "run example: " 
 			<< name
 			<< std::endl
 			;
			
			if (boost::algorithm::equals(name, "factory"))
			{
				cyy::example_factory();
			}
			else if (boost::algorithm::equals(name, "generic"))
			{
				cyy::example_generic_factory();
			}
			else if (boost::algorithm::equals(name, "usage"))
			{
				cyy::example_usage();
			}
			else if (boost::algorithm::equals(name, "math"))
			{
				cyy::example_math();
			}
			else if (boost::algorithm::equals(name, "composites"))
			{
				cyy::example_composites();
			}
			else if (boost::algorithm::equals(name, "types"))
			{
				cyy::example_types();
			}
			else if (boost::algorithm::equals(name, "reader"))
			{
				cyy::example_reader();
			}
			else if (boost::algorithm::equals(name, "gems"))
			{
				cyy::example_gems();
			}
			else if (boost::algorithm::equals(name, "io"))
			{
				cyy::example_gems();
			}
			else
			{
				std::cout 
				<< "***warning: " 
				<< name
				<< " is an unknown example"
				<< std::endl
				;
			}
 		}
 		else 
		{
 			std::cout 
 			<< "run all examples" 
 			<< std::endl
 			;
			
 			std::cout << "run example: factory "  << std::endl;
			cyy::example_factory();

 			std::cout << "run example: generic "  << std::endl;
			cyy::example_generic_factory();
			
 			std::cout << "run example: usage "  << std::endl;
			cyy::example_usage();

 			std::cout << "run example: math "  << std::endl;
			cyy::example_math();

 			std::cout << "run example: composites "  << std::endl;
			cyy::example_composites();
			
 			std::cout << "run example: types "  << std::endl;
			cyy::example_types();
			
 			std::cout << "run example: reader "  << std::endl;
			cyy::example_reader();
			
 			std::cout << "run example: gems "  << std::endl;
			cyy::example_gems();
			
 			std::cout << "run example: io "  << std::endl;
			cyy::example_io();
		}


		 
	}
	catch(std::exception& e) 
	{
		std::cerr 
		<< e.what() 
		<< std::endl
		;
    }

	return EXIT_SUCCESS;
}

