#include "lesson_gems.h"
#include <cyy/util/brute_cast.hpp>
#include <iostream>
#include <iomanip>
#include <cstdint> 

namespace cyy 
{
	void example_gems()
	{
		//	cast a pointer to integer
		std::uint64_t a = 1;
		std::uint64_t *b = &a;
#ifdef _WIN64
		a = brute_cast<std::uint64_t>(b);
#endif
		//	interestingly int doesn't work on 64 bit linux machines using gcc
		//	sizeof(int) = 4
		//	sizeof(int*) = 8

		//	this output may vary
		std::cout << "0x" << std::hex << a << " == " << b << std::endl;

		//	cast array to integer
		const char c[4] = { 0x63, 0x79, 0x79, 0 };
		const std::uint32_t value = brute_cast<std::uint32_t>(c);
		//	on little endian machines the hex value is in reversed order
		std::cout << c << " " << std::hex << std::setw(8) << std::setfill('0') << value << std::endl;

		//	And vice versa: split a POT into pieces and fill
		//	an array. Since C++ doesn't allow arrays as return
		//	types, we use a reference parameter.
		char r[4] = { 0 };
		cyy::brute_cast(r, value);
		std::cout << r << std::endl;

		//	the other way round
		cyy::brute_reverse_cast(r, value);
		BOOST_ASSERT(r[0] == 0);
		std::cout << r[1] << r[2] << r[3] << std::endl;

		
	}
}