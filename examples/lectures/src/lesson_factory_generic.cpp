#include "lesson_factory_generic.h"
#include <cyy/intrinsics/factory/generic_factory.hpp>

namespace cyy 
{
	void example_generic_factory()
	{
		//	selects the factory method depending on the specified type code 
		auto obj = create<types::CYY_UINT8>(99);
		obj = create<types::CYY_INT16>(99);
		
		//	Create a tuple.
		//	Note the usage of the std::string object
		obj = tuple_factory(std::string("1"), 2, std::chrono::seconds(3));
		
		//	is the same as
		tuple_t tpl;
		tpl.push_back(string_factory("1"));
		tpl.push_back(factory(2));
		tpl.push_back(factory(std::chrono::seconds(3)));
		obj = factory(tpl);
		
		//	create vectors from a homogeneous initializer_list
		obj = vector_factory({ 1, 2, 3, 4 });
		
		//	doesn't compile
		//obj = vector_factory({ std::string("1"), 2});	//	error
		
		//	It's possible but not recommended 
		//	to embed objects into objects.
		auto dont_do_this = core::make_value(obj);
	}
}