#include "lesson_types.h"
//#define CYY_DISABLE_EXPLICT_FACTORY
#include <cyy/intrinsics/factory/generic_factory.hpp>
#include <cyy/value_cast.hpp>
#include <cyy/intrinsics/algorithm.h>
#include <cyy/intrinsics/type_traits.h>
#include <cyy/intrinsics/type_codes.h>
#include <boost/assert.hpp>

namespace cyy 
{
	void example_types()
	{
		//	create a vector of std::string(s)
		auto v1 = vector_factory({"CCC", "AAA", "BBB"});

		//	create a vector of attributes
		auto v2 = vector_factory({set_factory(3, "three")
			, set_factory(1, "one")
			, set_factory(2, "two")
			, set_factory(42, "forty-two")
			, set_factory(8, "eight")
		});
		
		//	lexicographically sorting
		sort(v1);
		
		try {
			auto ref = object_ref_cast<vector_t>(v1);
			for (auto obj : ref)
			{
				std::cout 
				<< string_cast(obj) 
				<< " "
				;
			}
			std::cout << std::endl;
		}
		catch (boost::exception const& ex)	{
		}

		//	attributes are sorted by index
		sort(v2);
		
		try {
			auto ref = object_ref_cast<vector_t>(v2);
			for (auto obj : ref)
			{
				std::cout 
				<< value_cast<attr_t>(obj).first 
				<< " "
				;
			}
			std::cout << std::endl;
		}
		catch (boost::exception const& ex)	{
		}
		
		
//		object obj = find(v2, 42);

		//	create a tuple of different types
		auto tpl1 = cyy::tuple_factory( set_factory(3, "three")
 			, set_factory(1, "one")
 			, set_factory(2, "two")
 			, set_factory(42, "forty-two")
 			, set_factory(8, "eight")	//	5 attributes with string
 			, set_factory(23, std::chrono::seconds(23))
			, "A", "B", "C"
			, 1, 2, 3	//	7 other data types
		);
		
//#if defined(TRUE)
//  		std::cout << "sort... " << std::endl;
//  		sort(tpl1);
//  		for(auto obj : value_cast<tuple_t>(tpl1))
//  		{
//  			std::cout << cyy::to_literal(obj) << std::endl;
//  		}
//  		std::cout << "...sort " << std::endl;
//#endif
		
		//
		//	find all attributes of type string
		std::size_t attributes{ 0 }, others{ 0 };
		for(auto obj : value_cast<tuple_t>(tpl1))
		{
			(type_code_test< types::CYY_ATTRIBUTE, types::CYY_STRING >(obj))
				? attributes++
				: others++
				;
		}
		std::cout 
		<< attributes
		<< " attributes with string, "
		<< others
		<< " others"
		<< std::endl;

	}
}