#include "lesson_factory.h"
#define CYY_DISABLE_EXPLICT_FACTORY
#include <cyy/intrinsics/factory.h>

namespace cyy 
{
	void example_factory()
	{
		//	generate an NULL object
		auto obj1 = factory();
		
		//	generate an object of type int
		//	note: int is platform dependend
		auto obj2 = factory(42);
		
		//	make sure to generate an object of type int
		auto obj3 = numeric_factory_cast<int>(42);

		//	generate the largest int value
		auto obj4 = numeric_factory_max<int>();
		
		//	same as factory(true)
		auto obj5 = true_factory();

		//	generate a string not a pointer
		auto obj6 = string_factory("hello world!");
		
		//	whoops, a pointer
		//	requires -DCYY_DISABLE_EXPLICT_FACTORY
		auto obj7 = factory("hello world!");
	}
}