# top level files
set (lectures)

set (lectures_cpp

    examples/lectures/src/main.cpp
    examples/lectures/src/lesson_factory.cpp
    examples/lectures/src/lesson_factory_generic.cpp
    examples/lectures/src/lesson_usage.cpp
    examples/lectures/src/lesson_math.cpp
    examples/lectures/src/lesson_composites.cpp
    examples/lectures/src/lesson_types.cpp
    examples/lectures/src/lesson_reader.cpp
    examples/lectures/src/lesson_gems.cpp
    examples/lectures/src/lesson_io.cpp
)

set (lectures_h
    examples/lectures/src/lesson_factory.h
    examples/lectures/src/lesson_factory_generic.h
    examples/lectures/src/lesson_usage.h
    examples/lectures/src/lesson_math.h
    examples/lectures/src/lesson_composites.h
    examples/lectures/src/lesson_types.h
    examples/lectures/src/lesson_reader.h
    examples/lectures/src/lesson_gems.h
    examples/lectures/src/lesson_io.h
)

# define the lectures program
set (lectures
  ${lectures_cpp}
  ${lectures_h}
)